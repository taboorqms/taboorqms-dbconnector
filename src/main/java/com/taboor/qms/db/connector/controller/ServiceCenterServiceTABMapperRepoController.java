package com.taboor.qms.db.connector.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.ServiceCenter;
import com.taboor.qms.core.model.ServiceCenterServiceTABMapper;
import com.taboor.qms.core.model.ServiceRequirement;
import com.taboor.qms.core.model.ServiceTAB;
import com.taboor.qms.db.connector.repository.ServiceCenterServiceTABMapperRepository;

@RestController
@RequestMapping("/tab/servicecenterservicetabmapper")
@CrossOrigin(origins = "*")
public class ServiceCenterServiceTABMapperRepoController {

	@Autowired
	ServiceCenterServiceTABMapperRepository serviceCenterServiceTABMapperRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveServiceCenterServiceTABMapper(
			@RequestBody @Valid final ServiceCenterServiceTABMapper serviceCenterServiceTABMapper)
			throws Exception, Throwable {
		return ResponseEntity.ok(serviceCenterServiceTABMapperRepository.save(serviceCenterServiceTABMapper));
	}

	@RequestMapping(value = "/saveAll", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveAll(
			@RequestBody @Valid final List<ServiceCenterServiceTABMapper> serviceCenterServiceTABMappers)
			throws Exception, Throwable {
		return ResponseEntity.ok(serviceCenterServiceTABMapperRepository.saveAll(serviceCenterServiceTABMappers));
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<ServiceCenterServiceTABMapper> entity = serviceCenterServiceTABMapperRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long serviceCenterServiceTABMapperId)
			throws Exception, Throwable {

		Optional<ServiceCenterServiceTABMapper> entity = serviceCenterServiceTABMapperRepository
				.findById(serviceCenterServiceTABMapperId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/getServices/serviceCenter", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getServicesByServiceCenter(Long serviceCenterId)
			throws Exception, Throwable {

		ServiceCenter center = new ServiceCenter();
		center.setServiceCenterId(serviceCenterId);
		List<ServiceCenterServiceTABMapper> entity = serviceCenterServiceTABMapperRepository
				.findByServiceCenter(center);
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/countServices/serviceCenter", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getCountServicesByServiceCenter(Long serviceCenterId)
			throws Exception, Throwable {

		int entity = serviceCenterServiceTABMapperRepository.countByServiceCenter(serviceCenterId);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/getRequirements/serviceCenterIdAndServiceId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getRequirementsByServiceCenterIdAndServiceId(Long serviceCenterId,
			Long serviceId) throws Exception, Throwable {

		Collection<ServiceRequirement> entity = serviceCenterServiceTABMapperRepository
				.findRequirementsByServiceCenterIdAndServiceId(serviceCenterId, serviceId);
		return ResponseEntity.ok(new ArrayList<>(entity));
	}

	@RequestMapping(value = "/get/serviceCenterIdAndServiceId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByServiceCenterIdAndServiceId(Long serviceCenterId, Long serviceId)
			throws Exception, Throwable {

		Optional<ServiceCenterServiceTABMapper> entity = serviceCenterServiceTABMapperRepository
				.findByServiceCenterIdAndServiceId(serviceCenterId, serviceId);
		return ResponseEntity.ok(entity);
	}

//	@RequestMapping(value = "/delete/id", method = RequestMethod.GET)
//	public @ResponseBody ResponseEntity<?> deleteCounterById(@RequestParam @Valid final Long serviceId,
//			@RequestParam @Valid final Long serviceCenterId) throws Exception, Throwable {
//		int response = serviceCenterServiceTABMapperRepository.deleteByServiceId(serviceId, serviceCenterId);
//		if (response == 1)
//			return ResponseEntity.ok(true);
//		return ResponseEntity.ok(false);
//	}

	@RequestMapping(value = "/delete/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteCounterById(@RequestParam @Valid final Long serviceCenterServiceId)
			throws Exception, Throwable {
		serviceCenterServiceTABMapperRepository.deleteById(serviceCenterServiceId);
		return ResponseEntity.ok(true);
	}

}
