package com.taboor.qms.db.connector.controller;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.UserPaymentTransaction;
import com.taboor.qms.db.connector.repository.UserPaymentTransactionRepository;

@RestController
@RequestMapping("/tab/userpaymenttransactions")
@CrossOrigin(origins = "*")
public class UserPaymentTransactionRepoController {

	@Autowired
	UserPaymentTransactionRepository userPaymentTransactionRepository;

	@PersistenceContext
	EntityManager em;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveUserPaymentTransaction(
			@RequestBody @Valid final UserPaymentTransaction userPaymentTransaction) throws Exception, Throwable {
		return ResponseEntity.ok(userPaymentTransactionRepository.save(userPaymentTransaction));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long userPaymentTransactionId)
			throws Exception, Throwable {

		Optional<UserPaymentTransaction> entity = userPaymentTransactionRepository.findById(userPaymentTransactionId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<UserPaymentTransaction> entity = userPaymentTransactionRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}
}
