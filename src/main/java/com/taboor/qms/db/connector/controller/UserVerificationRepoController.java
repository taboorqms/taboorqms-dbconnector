package com.taboor.qms.db.connector.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.User;
import com.taboor.qms.core.model.UserVerification;
import com.taboor.qms.core.payload.VerifyUserPayload;
import com.taboor.qms.db.connector.repository.UserVerificationRepository;

@RestController
@RequestMapping("/tab/userverifications")
@CrossOrigin(origins = "*")
public class UserVerificationRepoController {

	@Autowired
	UserVerificationRepository userVerificationRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveUserVerification(
			@RequestBody @Valid final UserVerification userVerification) throws Exception, Throwable {
		return ResponseEntity.ok(userVerificationRepository.save(userVerification));
	}

	@RequestMapping(value = "/getByUserEmailAndModeAndCode", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> getByUserEmail(@RequestBody @Valid final VerifyUserPayload verifyUserPayload)
			throws Exception, Throwable {

		Optional<UserVerification> entity = userVerificationRepository.findByUserEmailAndModeAndCode(
				verifyUserPayload.getEmail(), verifyUserPayload.getVerificationMode(),
				verifyUserPayload.getVerificationCode());

		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}
	@RequestMapping(value = "/getByUserPhoneAndModeAndCode", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByUserPhone(@RequestBody @Valid final VerifyUserPayload verifyUserPayload)
			throws Exception, Throwable {
		System.out.println(verifyUserPayload.getEmail());
		System.out.println(verifyUserPayload.getVerificationMode());
		System.out.println(verifyUserPayload.getVerificationCode());
		Optional<UserVerification> entity = userVerificationRepository.findByUserPhoneAndModeAndCode(
				verifyUserPayload.getEmail(), verifyUserPayload.getVerificationMode(),
				verifyUserPayload.getVerificationCode());

		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}
	@RequestMapping(value = "/check/user", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByUser(@RequestBody @Valid final User user) throws Exception, Throwable {

		Optional<UserVerification> entity = userVerificationRepository.findEmailVerificationByUser(user.getUserId(),
				user.getEmail());

		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

}
