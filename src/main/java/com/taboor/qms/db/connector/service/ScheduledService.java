package com.taboor.qms.db.connector.service;

import org.springframework.stereotype.Service;

import com.taboor.qms.core.exception.TaboorQMSServiceException;

@Service
public interface ScheduledService {

	void updateInvoiceStatus() throws TaboorQMSServiceException, Exception;

	void chargeInvoices() throws TaboorQMSServiceException, Exception;

	void updateBranchTicketsPerDay() throws TaboorQMSServiceException, Exception;

}
