package com.taboor.qms.db.connector.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.Ticket;

@Repository
@Transactional
public interface TicketRepository extends JpaRepository<Ticket, Long> {

	@Modifying
	@Query("DELETE from Ticket t where t.ticketId = :ticketId")
	int deleteByTicketId(@Param("ticketId") long ticketId);
        
        @Modifying
	@Query("DELETE from Ticket t where t.branch = :branch")
	int deleteByBranch(Branch branch);

	int countByBranch(Branch branch);

	@Query("SELECT count(t.ticketId) FROM Ticket t where t.branch =:branch group by DATE(t.createdTime)")
	Long countDayAverageByBranch(Branch branch);
	
	@Query("select tk from Ticket tk where branch_id = :branchId and tk.counterNumber = :counterNo and tk.ticketStatus = 3")
	Optional<Ticket> findTicketByCounterAndBranch(@Param("branchId") Long branchId,
			@Param("counterNo") String counterNo);
	
	@Query("select tk from Ticket tk where branch_id = :branchId and tk.ticketStatus = 3 and tk.ticketType != 4 and tk.ticketType != 5")
	List<Ticket> findTicketByBranch(@Param("branchId") Long branchId);
	
	Optional<Ticket> findByTicketNumber(String ticketNumber);

	@Query("SELECT count(t.ticketId) FROM Ticket t where branch_id IN :branchIdList and DATE(t.createdTime) between :startDate and :endDate")
	Long countByBranchList(@Param("branchIdList") List<Long> branchIdList, @Param("startDate") LocalDate startDate,
			@Param("endDate") LocalDate endDate);

	@Query("SELECT count(t.ticketId) FROM Ticket t where branch_id IN :branchIdList and t.ticketType =:ticketType and DATE(t.createdTime) between :startDate and :endDate")
	Long countFastpassByBranchList(@Param("branchIdList") List<Long> branchIdList, @Param("ticketType") int ticketType,
			@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

        @Query("select tk from Ticket tk where step_out =true")
	List<Ticket> findTicketByStepOut();
}
