package com.taboor.qms.db.connector.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.PaymentMethod;
import com.taboor.qms.db.connector.repository.PaymentMethodRepository;

@RestController
@RequestMapping("/tab/paymentmethods")
@CrossOrigin(origins = "*")
public class PaymentMethodRepoController {

	@Autowired
	PaymentMethodRepository paymentMethodRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> savePaymentMethod(@RequestBody @Valid final PaymentMethod paymentMethod)
			throws Exception, Throwable {
		return ResponseEntity.ok(paymentMethodRepository.save(paymentMethod));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long paymentMethodId)
			throws Exception, Throwable {

		Optional<PaymentMethod> entity = paymentMethodRepository.findById(paymentMethodId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/name", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByName(@RequestParam @Valid final String paymentMethod)
			throws Exception, Throwable {
		PaymentMethod entity = paymentMethodRepository.findByPaymentMethodName(paymentMethod);
		if (entity != null)
			return ResponseEntity.ok(entity);
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<PaymentMethod> entity = paymentMethodRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}
}
