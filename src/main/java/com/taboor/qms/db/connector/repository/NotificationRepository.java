package com.taboor.qms.db.connector.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.Notification;

@Repository
@Transactional
public interface NotificationRepository extends JpaRepository<Notification, Long> {

	@Modifying
	@Query("UPDATE Notification n set n.readStatus = true where n.notificationId IN :notifId")
	int updateUserNotificationReadStatus(@Param("notifId") List<Long> notifIdList);

	@Query("SELECT count(readStatus) FROM Notification WHERE readStatus = false and user_id = :userId")
	int findUnReadNotificationCountByUserId(@Param("userId") Long userId);

	@Query("SELECT n FROM Notification n WHERE notificationType = :type and user_id = :userId order by n.createdAt desc")
	List<Notification> findByUserAndType(@Param("userId") Long userId, @Param("type") int notificationType,
			Pageable pageable);
}
