package com.taboor.qms.db.connector.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.UserConfiguration;
import com.taboor.qms.db.connector.repository.UserConfigurationRepository;

@RestController
@RequestMapping("/tab/userconfigurations")
@CrossOrigin(origins = "*")
public class UserConfigurationRepoController {

	@Autowired
	UserConfigurationRepository userConfigurationRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveUserConfiguration(
			@RequestBody @Valid final UserConfiguration userConfiguration) throws Exception, Throwable {
		return ResponseEntity.ok(userConfigurationRepository.save(userConfiguration));
	}

	@RequestMapping(value = "/get/userId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByUserId(@RequestParam @Valid final Long userId)
			throws Exception, Throwable {

		Optional<UserConfiguration> entity = userConfigurationRepository.findByUserId(userId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}
}
