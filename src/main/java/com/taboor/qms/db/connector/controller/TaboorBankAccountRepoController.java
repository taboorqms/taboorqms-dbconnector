package com.taboor.qms.db.connector.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.TaboorBankAccount;
import com.taboor.qms.db.connector.repository.TaboorBankAccountRepository;

@RestController
@RequestMapping("/tab/bankaccounts")
@CrossOrigin(origins = "*")
public class TaboorBankAccountRepoController {

	@Autowired
	TaboorBankAccountRepository taboorBankAccountRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveTaboorBankAccount(
			@RequestBody @Valid final TaboorBankAccount taboorBankAccount) throws Exception, Throwable {

		return ResponseEntity.ok(taboorBankAccountRepository.save(taboorBankAccount));
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<TaboorBankAccount> entity = taboorBankAccountRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long bankAccountId)
			throws Exception, Throwable {

		Optional<TaboorBankAccount> entity = taboorBankAccountRepository.findById(bankAccountId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/getDefaultAccount", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getDefaultAccount() throws Exception, Throwable {

		List<TaboorBankAccount> entityList = taboorBankAccountRepository.findByDefaultAccount(true);
		return ResponseEntity.ok(entityList.get(0));
	}
}
