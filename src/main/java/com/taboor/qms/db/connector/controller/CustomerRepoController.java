package com.taboor.qms.db.connector.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.Customer;
import com.taboor.qms.db.connector.repository.CustomerRepository;
import com.taboor.qms.db.connector.repository.UserRepository;

@RestController
@RequestMapping("/tab/customers")
@CrossOrigin(origins = "*")
public class CustomerRepoController {

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	UserRepository userRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> saveCustomer(@RequestBody @Valid final Customer customer)
			throws Exception, Throwable {
		userRepository.save(customer.getUser());
		return ResponseEntity.ok(customerRepository.save(customer));
	}

	@RequestMapping(value = "/get/userId", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> getByUserId(@RequestParam @Valid final Long userId)
			throws Exception, Throwable {

		Optional<Customer> entity = customerRepository.findByUserId(userId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/userEmail", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> getByUserEmail(@RequestParam @Valid final String email)
			throws Exception, Throwable {

		Optional<Customer> entity = customerRepository.findByUserEmail(email);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}
}
