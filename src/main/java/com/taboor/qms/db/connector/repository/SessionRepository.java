package com.taboor.qms.db.connector.repository;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.Session;

@Repository
@Transactional
public interface SessionRepository extends JpaRepository<Session, Long> {

	Optional<Session> findBySessionTokenAndLogoutTimeIsNull(String sessionToken);

	Optional<Session> findByRefreshToken(String refreshToken);

	Optional<Session> findBySessionIdAndLogoutTimeIsNull(@Valid Long sessionId);
	
	@Query("Select s from Session s where user_id = :userId")
	Optional<Session> findByUser(@Valid Long userId);

	@Query("Select s from Session s where s.user.email = :userEmail and s.logoutTime IS NULL")
	List<Session> findByUserEmailAndLogoutTime(@Param("userEmail") String userEmail);
	
	@Query("Select s from Session s where user_id = :userId and s.logoutTime IS NULL")
	List<Session> findByUserIdAndLogoutTime(@Valid Long userId);

	@Query("Select s from Session s where user_id = :userId and s.logoutTime IS NULL Order By s.loginTime DESC")
	List<Session> findActiveSessionByUserIdAndLogoutTime(@Valid Long userId);

}
