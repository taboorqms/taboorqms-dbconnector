package com.taboor.qms.db.connector.controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.Agent;
import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.BranchCounter;
import com.taboor.qms.core.model.BranchCounterAgentMapper;
import com.taboor.qms.core.model.BranchCounterServiceTABMapper;
import com.taboor.qms.core.model.BranchServiceTABMapper;
import com.taboor.qms.core.model.BranchWorkingHours;
import com.taboor.qms.core.model.Customer;
import com.taboor.qms.core.model.PaymentMethod;
import com.taboor.qms.core.model.PaymentPlan;
import com.taboor.qms.core.model.PlanPriceObject;
import com.taboor.qms.core.model.Queue;
import com.taboor.qms.core.model.QueueTicketMapper;
import com.taboor.qms.core.model.ServiceCenter;
import com.taboor.qms.core.model.ServiceCenterConfiguration;
import com.taboor.qms.core.model.ServiceCenterEmpBranchMapper;
import com.taboor.qms.core.model.ServiceCenterEmployee;
import com.taboor.qms.core.model.ServiceCenterServiceTABMapper;
import com.taboor.qms.core.model.ServiceRequirement;
import com.taboor.qms.core.model.ServiceSector;
import com.taboor.qms.core.model.ServiceTAB;
import com.taboor.qms.core.model.TaboorBankAccount;
import com.taboor.qms.core.model.TaboorEmployee;
import com.taboor.qms.core.model.Ticket;
import com.taboor.qms.core.model.TicketFastpass;
import com.taboor.qms.core.model.TicketFeedback;
import com.taboor.qms.core.model.TicketUserMapper;
import com.taboor.qms.core.model.User;
import com.taboor.qms.core.model.UserBankCard;
import com.taboor.qms.core.model.UserConfiguration;
import com.taboor.qms.core.model.UserNotificationType;
import com.taboor.qms.core.model.UserPaymentTransaction;
import com.taboor.qms.core.model.UserPrivilege;
import com.taboor.qms.core.model.UserPrivilegeMapper;
import com.taboor.qms.core.model.UserRole;
import com.taboor.qms.core.model.UserRoleMapper;
import com.taboor.qms.core.response.GenStatusResponse;
import com.taboor.qms.core.utils.BranchStatus;
import com.taboor.qms.core.utils.Currency;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.RoleName;
import com.taboor.qms.core.utils.ServiceReviewStatus;
import com.taboor.qms.core.utils.Signature;
import com.taboor.qms.core.utils.TaboorQMSConfigurations;
import com.taboor.qms.core.utils.TicketFastpassStatus;
import com.taboor.qms.core.utils.TicketStatus;
import com.taboor.qms.core.utils.TicketType;
import com.taboor.qms.core.utils.UserVerificationStatus;
import com.taboor.qms.core.utils.WeekDays;
import com.taboor.qms.db.connector.repository.AgentRepository;
import com.taboor.qms.db.connector.repository.BranchCounterAgentMapperRepository;
import com.taboor.qms.db.connector.repository.BranchCounterRepository;
import com.taboor.qms.db.connector.repository.BranchCounterServiceTABMapperRepository;
import com.taboor.qms.db.connector.repository.BranchRepository;
import com.taboor.qms.db.connector.repository.BranchServiceTABMapperRepository;
import com.taboor.qms.db.connector.repository.BranchWorkingHoursRepository;
import com.taboor.qms.db.connector.repository.CustomerRepository;
import com.taboor.qms.db.connector.repository.PaymentMethodRepository;
import com.taboor.qms.db.connector.repository.PaymentPlanRepository;
import com.taboor.qms.db.connector.repository.QueueRepository;
import com.taboor.qms.db.connector.repository.QueueTicketMapperRepository;
import com.taboor.qms.db.connector.repository.ServiceCenterConfigurationRepository;
import com.taboor.qms.db.connector.repository.ServiceCenterEmpBranchMapperRepository;
import com.taboor.qms.db.connector.repository.ServiceCenterEmployeeRepository;
import com.taboor.qms.db.connector.repository.ServiceCenterRepository;
import com.taboor.qms.db.connector.repository.ServiceCenterServiceTABMapperRepository;
import com.taboor.qms.db.connector.repository.ServiceRequirementRepository;
import com.taboor.qms.db.connector.repository.ServiceSectorRepository;
import com.taboor.qms.db.connector.repository.ServiceTABRepository;
import com.taboor.qms.db.connector.repository.TaboorBankAccountRepository;
import com.taboor.qms.db.connector.repository.TaboorEmployeeRepository;
import com.taboor.qms.db.connector.repository.TicketFastpassRepository;
import com.taboor.qms.db.connector.repository.TicketFeedbackRepository;
import com.taboor.qms.db.connector.repository.TicketRepository;
import com.taboor.qms.db.connector.repository.TicketUserMapperRepository;
import com.taboor.qms.db.connector.repository.UserBankCardRepository;
import com.taboor.qms.db.connector.repository.UserConfigurationRepository;
import com.taboor.qms.db.connector.repository.UserNotificationTypeRepository;
import com.taboor.qms.db.connector.repository.UserPaymentTransactionRepository;
import com.taboor.qms.db.connector.repository.UserPrivilegeMapperRepository;
import com.taboor.qms.db.connector.repository.UserRepository;
import com.taboor.qms.db.connector.repository.UserRoleMapperRepository;
import com.taboor.qms.db.connector.repository.UserRoleRepository;

@RestController
@RequestMapping("/tab/data")
@CrossOrigin(origins = "*")
public class DataEntryController {

    private final String passEncryptionKey = TaboorQMSConfigurations.PASSWORD_ENCRYPTION_KEY.getValue();

//	@Value("${url.db.connector}")
//	private String dbConnectorMicroserviceURL;
    @Autowired
    ServiceSectorRepository serviceSectorRepository;

    @Autowired
    PaymentPlanRepository paymentPlanRepsitory;

    @Autowired
    ServiceTABRepository serviceTABRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ServiceCenterRepository serviceCenterRepository;

    @Autowired
    ServiceCenterServiceTABMapperRepository serviceCenterServiceTABMapperRepository;

    @Autowired
    ServiceCenterEmployeeRepository serviceCenterEmployeeRepository;

    @Autowired
    BranchRepository branchRepository;

    @Autowired
    BranchWorkingHoursRepository branchWorkingHoursRepository;

    @Autowired
    BranchServiceTABMapperRepository branchServiceTABMapperRepository;

    @Autowired
    ServiceRequirementRepository serviceRequirementRepository;

    @Autowired
    BranchCounterRepository branchCounterRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    PaymentMethodRepository paymentMethodRepository;

    @Autowired
    UserBankCardRepository userBankCardRepository;

    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    TicketUserMapperRepository ticketUserMapperRepository;

    @Autowired
    QueueRepository queueRepository;

    @Autowired
    QueueTicketMapperRepository queueTicketMapperRepository;

    @Autowired
    TicketFastpassRepository ticketFastpassRepository;

    @Autowired
    UserRoleRepository roleRepository;

    @Autowired
    UserRoleMapperRepository userRoleMapperRepository;

    @Autowired
    UserPaymentTransactionRepository userPaymentTransactionRepository;

    @Autowired
    TicketFeedbackRepository ticketFeedbackRepository;

    @Autowired
    ServiceCenterEmpBranchMapperRepository serviceCenterEmpBranchMapperRepository;

    @Autowired
    UserPrivilegeMapperRepository userPrivilegeMapperRepository;

    @Autowired
    TaboorBankAccountRepository taboorBankAccountRepository;

    @Autowired
    AgentRepository agentRepository;

    @Autowired
    BranchCounterAgentMapperRepository branchCounterAgentMapperRepository;

    @Autowired
    BranchCounterServiceTABMapperRepository branchCounterServiceTABMapperRepository;

    @Autowired
    UserRoleRepository userRoleRepository;

    @Autowired
    TaboorEmployeeRepository taboorEmployeeRepository;

    @Autowired
    UserConfigurationRepository UserConfigurationRepository;

    @Autowired
    UserNotificationTypeRepository userNotificationTypeRepository;

    @Autowired
    ServiceCenterConfigurationRepository serviceCenterConfigurationRepository;

//	@PersistenceContext
//	EntityManager em;
//	RestTemplate restTemplate = new RestTemplate();
    @RequestMapping(value = "/entry2", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    GenStatusResponse dataEntry2() throws Exception, Throwable {

        ServiceSector serviceSector = new ServiceSector();
        serviceSector.setServiceSectorName("Banking");
//		serviceSector.setServiceSectorId(Long.parseLong(
//				em.createNativeQuery("select nextval('service_sector_seq_pk')").getResultList().get(0).toString()));
        serviceSector = serviceSectorRepository.save(serviceSector);

        ServiceSector serviceSector2 = new ServiceSector();
        serviceSector2.setServiceSectorName("Financing");
//		serviceSector2.setServiceSectorId(Long.parseLong(
//				em.createNativeQuery("select nextval('service_sector_seq_pk')").getResultList().get(0).toString()));
        serviceSector2 = serviceSectorRepository.save(serviceSector2);

        ServiceSector serviceSector3 = new ServiceSector();
        serviceSector3.setServiceSectorName("Other");
//		serviceSector3.setServiceSectorId(Long.parseLong(
//				em.createNativeQuery("select nextval('service_sector_seq_pk')").getResultList().get(0).toString()));
        serviceSector3 = serviceSectorRepository.save(serviceSector3);

//		User superAdminUser = new User();
//		superAdminUser.setEmail("superadmin@taboor.com");
//		superAdminUser.setCreatedTime(OffsetDateTime.now());
//		superAdminUser.setName("Super Admin User");
//		superAdminUser.setPhoneNumber("+92-3008112990");
//		superAdminUser.setRoleName(RoleName.ROLE_SUPERADMIN.name());
//		superAdminUser.setVerified(701);
//		superAdminUser.setPassword(Signature.encrypt("P@kistan", passEncryptionKey));
//
//		superAdminUser = userRepository.save( superAdminUser, User.class);
//
//		UserRole superAdminRole = RestUtil.getRequest(
//				dbConnectorMicroserviceURL + "/tab/userroles/get/roleName?roleName=" + RoleName.ROLE_SUPERADMIN,
//				UserRole.class, xyzErrorMessage.USERROLE_NOT_EXISTS.getErrorCode(),
//				xyzErrorMessage.USERROLE_NOT_EXISTS.getErrorDetails());
//
//		List<UserPrivilegeMapper> userPrivilegeMappers = new ArrayList<UserPrivilegeMapper>();
//
//		for (UserPrivilege entity : superAdminRole.getUserPrivileges()) {
//			UserPrivilegeMapper userPrivilegeMapper = new UserPrivilegeMapper();
//			userPrivilegeMapper.setUser(superAdminUser);
//			userPrivilegeMapper.setUserPrivilege(entity);
//			userPrivilegeMappers.add(userPrivilegeMapper);
//		}
//		userPrivilegeMappers = userPrivilegeMapperRepository.saveAll(userPrivilegeMappers);
        TaboorBankAccount taboorBankAccount = new TaboorBankAccount();
        taboorBankAccount.setCardCvv("123");
        taboorBankAccount.setCardNumber("1283651263587981");
        taboorBankAccount.setCardTitle("Taboor Bank Account Official");
        taboorBankAccount.setCardType("Credit");
        taboorBankAccount.setExpiryMonth(12);
        taboorBankAccount.setExpiryYear(2027);
        taboorBankAccount.setDefaultAccount(false);
        taboorBankAccount.setPaymentMethod(null);

        taboorBankAccountRepository.save(taboorBankAccount);

        taboorBankAccount = new TaboorBankAccount();
        taboorBankAccount.setCardCvv("123");
        taboorBankAccount.setCardNumber("1364932176421987");
        taboorBankAccount.setCardTitle("Taboor Bank Account Official 2");
        taboorBankAccount.setCardType("Credit");
        taboorBankAccount.setExpiryMonth(12);
        taboorBankAccount.setExpiryYear(2027);
        taboorBankAccount.setDefaultAccount(true);
        taboorBankAccount.setPaymentMethod(null);

        taboorBankAccountRepository.save(taboorBankAccount);

        PaymentPlan paymentPlan = new PaymentPlan();
        paymentPlan.setCreatedTime(OffsetDateTime.now().withNano(0));
        paymentPlan.setAdvanceReportingEnabled(true);
        paymentPlan.setBranchLimit(10);
        paymentPlan.setDedicatedAccountManager(true);
        paymentPlan.setNoOfServices(10);
        paymentPlan.setNoOfSMS(100);
        paymentPlan.setNoOfUserPerBranch(10);
        paymentPlan.setPlanName("ABC");
        PlanPriceObject object = new PlanPriceObject();
        object.setCurrency(Currency.AED);
        object.setPerSMSPrice(10.0);
        object.setPerUserPrice(15.0);
        object.setPriceAnnually(1500.0);
        object.setPriceMonthly(150.0);
        paymentPlan.setPrices(Arrays.asList(object));
        paymentPlanRepsitory.save(paymentPlan);

        ServiceCenter serviceCenter = new ServiceCenter();
        serviceCenter.setCountry("Pakistan");
//		serviceCenter.setPaymentPlan(paymentPlan);
        serviceCenter.setServiceSector(serviceSector);
        serviceCenter.setPhoneNumber("+92-3008112990");
        serviceCenter.setServiceCenterName("Meezan Bank");
        serviceCenter.setServiceCenterArabicName("بنك  الميزان");
        serviceCenter = serviceCenterRepository.save(serviceCenter);

        Branch branch = new Branch();
        branch.setAddress("G-11 Markaz, Islamabad");
//		branch.setAverageServiceTime(LocalTime.of(0, 10));
        branch.setAverageTicketPerDay(100);
//		branch.setAverageWaitingTime(LocalTime.of(0, 18));
        branch.setBranchName("Meezan Bank G-11");
        branch.setBranchNameArabic("بنك الميزان إحدى عشر");
        branch.setBranchStatus(BranchStatus.OPEN.getStatus());
        branch.setCity("Islamabad");
        branch.setCreatedTime(OffsetDateTime.now().withNano(0));
        branch.setEmailAddress("g11@meezan.com");
        branch.setLatitude(33.671025);
        branch.setLongitude(72.995970);
        branch.setPhoneNumber("+92-3008112990");
        branch.setRating(3.5);
        branch.setServiceCenter(serviceCenter);
        branch.setWorkingStatus(true);

        branch = branchRepository.save(branch);

        User serviceCenterEmployeeUser2 = new User();
        serviceCenterEmployeeUser2.setEmail("zse" + "@meezan.com");
        serviceCenterEmployeeUser2.setName("zse");
        serviceCenterEmployeeUser2.setCreatedTime(OffsetDateTime.now().withNano(0));
        serviceCenterEmployeeUser2.setPassword(Signature.encrypt("P@kistan", passEncryptionKey));
        serviceCenterEmployeeUser2.setPhoneNumber("+92-3008112990");
        serviceCenterEmployeeUser2.setProfileImageUrl("pic.jpg");
        serviceCenterEmployeeUser2.setEmailVerified(true);
        serviceCenterEmployeeUser2.setPhoneVerified(true);
        serviceCenterEmployeeUser2.setRoleName(RoleName.Agent.name());
        serviceCenterEmployeeUser2 = userRepository.save(serviceCenterEmployeeUser2);

        ServiceCenterEmployee serviceCenterEmployee2 = new ServiceCenterEmployee();
        serviceCenterEmployee2.setEmployeeNumber("RSX123" + "Z");
        serviceCenterEmployee2.setServiceCenter(serviceCenter);
        serviceCenterEmployee2.setUser(serviceCenterEmployeeUser2);
        serviceCenterEmployee2 = serviceCenterEmployeeRepository.save(serviceCenterEmployee2);

        branch.setAddress("ABC");
        Agent agent = new Agent();
        agent.setServiceCenterEmployee(serviceCenterEmployee2);
        agent.setTotalTicketServed(0);
        agent.setBranch(branch);
//		agent.setTotalWorkHours(LocalTime.of(0, 0));
//		agent.setAverageServiceTime(LocalTime.of(0, 0));
        agent.setLoginStatus(false);
        agent = agentRepository.save(agent);

        return new GenStatusResponse(0, "Success");
    }

    @RequestMapping(value = "/entry", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    GenStatusResponse dataEntry() throws Exception, Throwable {

        User superAdminUser = new User();
        superAdminUser.setEmail("superadmin@taboor.com");
        superAdminUser.setCreatedTime(OffsetDateTime.now());
        superAdminUser.setName("Super Admin User");
        superAdminUser.setPhoneNumber("+92-3008112990");
        superAdminUser.setRoleName(RoleName.Taboor_Admin.name());
        superAdminUser.setEmailVerified(true);
        superAdminUser.setPhoneVerified(true);
        superAdminUser.setPassword(Signature.encrypt("P@kistan", passEncryptionKey));

        superAdminUser = userRepository.save(superAdminUser);

        TaboorEmployee taboorEmployee = new TaboorEmployee();
        taboorEmployee.setEmployeeNumber("TAB0001");
        taboorEmployee.setUser(superAdminUser);
        taboorEmployeeRepository.save(taboorEmployee);

        UserConfiguration configuration = new UserConfiguration();
        configuration.setEmailAllowed(true);
        configuration.setInAppNotificationAllowed(true);
        configuration.setSmsAllowed(true);
        configuration.setUser(superAdminUser);
        UserConfigurationRepository.save(configuration);

        UserNotificationType userNotificationType = new UserNotificationType();
        userNotificationType.setCustomerFeedback1Star(false);
        userNotificationType.setCustomerFeedback2Star(false);
        userNotificationType.setCustomerFeedback3Star(false);
        userNotificationType.setCustomerFeedback4Star(false);
        userNotificationType.setCustomerFeedback5Star(false);
        userNotificationType.setNewOrUpdateAgent(false);
        userNotificationType.setNewOrUpdateBranch(false);
        userNotificationType.setNewOrUpdateServiceCenter(true);
        userNotificationType.setNewOrUpdateUser(true);
        userNotificationType.setUser(superAdminUser);
        userNotificationTypeRepository.save(userNotificationType);

        UserRole superAdminRole = userRoleRepository.findByRoleName(RoleName.Taboor_Admin.name()).get();

        UserRoleMapper userRoleMapper = new UserRoleMapper();
        userRoleMapper.setUser(superAdminUser);
        userRoleMapper.setUserRole(superAdminRole);
        userRoleMapperRepository.save(userRoleMapper);

        List<UserPrivilegeMapper> userPrivilegeMappers = new ArrayList<UserPrivilegeMapper>();

        for (UserPrivilege entity : superAdminRole.getUserPrivileges()) {
            UserPrivilegeMapper userPrivilegeMapper = new UserPrivilegeMapper();
            userPrivilegeMapper.setUser(superAdminUser);
            userPrivilegeMapper.setUserPrivilege(entity);
            userPrivilegeMappers.add(userPrivilegeMapper);
        }
        userPrivilegeMappers = userPrivilegeMapperRepository.saveAll(userPrivilegeMappers);

        TaboorBankAccount taboorBankAccount = new TaboorBankAccount();
        taboorBankAccount.setCardCvv("123");
        taboorBankAccount.setCardNumber("1283651263587981");
        taboorBankAccount.setCardTitle("Taboor Bank Account Official");
        taboorBankAccount.setCardType("Credit");
        taboorBankAccount.setExpiryMonth(12);
        taboorBankAccount.setExpiryYear(2027);
        taboorBankAccount.setDefaultAccount(false);
        taboorBankAccount.setPaymentMethod(null);

        taboorBankAccountRepository.save(taboorBankAccount);

        taboorBankAccount = new TaboorBankAccount();
        taboorBankAccount.setCardCvv("123");
        taboorBankAccount.setCardNumber("1364932176421987");
        taboorBankAccount.setCardTitle("Taboor Bank Account Official 2");
        taboorBankAccount.setCardType("Credit");
        taboorBankAccount.setExpiryMonth(12);
        taboorBankAccount.setExpiryYear(2027);
        taboorBankAccount.setDefaultAccount(true);
        taboorBankAccount.setPaymentMethod(null);

        taboorBankAccountRepository.save(taboorBankAccount);

        ServiceSector serviceSector = new ServiceSector();
        serviceSector.setServiceSectorName("Banking");
        serviceSector = serviceSectorRepository.save(serviceSector);

        ServiceSector serviceSector2 = new ServiceSector();
        serviceSector2.setServiceSectorName("Financing");
        serviceSector2 = serviceSectorRepository.save(serviceSector2);

        ServiceSector serviceSector3 = new ServiceSector();
        serviceSector3.setServiceSectorName("Other");
        serviceSector3 = serviceSectorRepository.save(serviceSector3);

        PaymentPlan paymentPlan = new PaymentPlan();
        paymentPlan.setPlanName("Silver");
        paymentPlan.setPlanNameArabic("فضة");
        paymentPlan.setCreatedTime(OffsetDateTime.now().withNano(0));
        paymentPlan.setAdvanceReportingEnabled(true);
        paymentPlan.setBranchLimit(10);
        paymentPlan.setDedicatedAccountManager(true);
        paymentPlan.setNoOfServices(10);
        paymentPlan.setNoOfSMS(100);
        paymentPlan.setNoOfUserPerBranch(10);
        paymentPlan.setDiscountPercentage(0.0);
        paymentPlan.setEasyPaymentEnabled(true);
        paymentPlan.setFreeDemoDays(10);
        paymentPlan.setFullDaySupport(true);
        paymentPlan.setKioskEnabled(true);
        paymentPlan.setPrioritySupport(true);
        paymentPlan.setRemoteDedicatedSupport(true);
        paymentPlan.setResponseTimeInOffice(LocalTime.of(0, 20));
        paymentPlan.setResponseTimeOutOffice(LocalTime.of(0, 30));
        paymentPlan.setSmsLimit(1000);
        paymentPlan.setSupportType("Basic");
        paymentPlan.setUserPerBranchLimit(100);
        PlanPriceObject object = new PlanPriceObject();
        object.setCurrency(Currency.AED);
        object.setPerSMSPrice(10.0);
        object.setPerUserPrice(15.0);
        object.setPriceAnnually(1500.0);
        object.setPriceMonthly(150.0);
        paymentPlan.setPrices(Arrays.asList(object));
        paymentPlan.setPricesCollection(GenericMapper.objectToJSONMapper(paymentPlan.getPrices()));
        paymentPlanRepsitory.save(paymentPlan);

        paymentPlan = new PaymentPlan();
        paymentPlan.setPlanName("Bronze");
        paymentPlan.setPlanNameArabic("برونز");
        paymentPlan.setCreatedTime(OffsetDateTime.now().withNano(0));
        paymentPlan.setAdvanceReportingEnabled(true);
        paymentPlan.setBranchLimit(20);
        paymentPlan.setDedicatedAccountManager(true);
        paymentPlan.setNoOfServices(20);
        paymentPlan.setNoOfSMS(200);
        paymentPlan.setNoOfUserPerBranch(20);
        paymentPlan.setDiscountPercentage(0.0);
        paymentPlan.setEasyPaymentEnabled(true);
        paymentPlan.setFreeDemoDays(10);
        paymentPlan.setFullDaySupport(true);
        paymentPlan.setKioskEnabled(true);
        paymentPlan.setPrioritySupport(true);
        paymentPlan.setRemoteDedicatedSupport(true);
        paymentPlan.setResponseTimeInOffice(LocalTime.of(0, 10));
        paymentPlan.setResponseTimeOutOffice(LocalTime.of(0, 20));
        paymentPlan.setSmsLimit(1000);
        paymentPlan.setSupportType("Standard");
        paymentPlan.setUserPerBranchLimit(100);
        object = new PlanPriceObject();
        object.setCurrency(Currency.AED);
        object.setPerSMSPrice(10.0);
        object.setPerUserPrice(15.0);
        object.setPriceAnnually(2000.0);
        object.setPriceMonthly(200.0);
        paymentPlan.setPrices(Arrays.asList(object));
        paymentPlan.setPricesCollection(GenericMapper.objectToJSONMapper(paymentPlan.getPrices()));
        paymentPlanRepsitory.save(paymentPlan);

        paymentPlan = new PaymentPlan();
        paymentPlan.setPlanName("Gold");
        paymentPlan.setPlanNameArabic("ذهب");
        paymentPlan.setCreatedTime(OffsetDateTime.now().withNano(0));
        paymentPlan.setAdvanceReportingEnabled(true);
        paymentPlan.setBranchLimit(30);
        paymentPlan.setDedicatedAccountManager(true);
        paymentPlan.setNoOfServices(30);
        paymentPlan.setNoOfSMS(300);
        paymentPlan.setNoOfUserPerBranch(30);
        paymentPlan.setDiscountPercentage(0.0);
        paymentPlan.setEasyPaymentEnabled(true);
        paymentPlan.setFreeDemoDays(10);
        paymentPlan.setFullDaySupport(true);
        paymentPlan.setKioskEnabled(true);
        paymentPlan.setPrioritySupport(true);
        paymentPlan.setRemoteDedicatedSupport(true);
        paymentPlan.setResponseTimeInOffice(LocalTime.of(0, 5));
        paymentPlan.setResponseTimeOutOffice(LocalTime.of(0, 10));
        paymentPlan.setSmsLimit(1000);
        paymentPlan.setSupportType("Professional");
        paymentPlan.setUserPerBranchLimit(100);
        object = new PlanPriceObject();
        object.setCurrency(Currency.AED);
        object.setPerSMSPrice(10.0);
        object.setPerUserPrice(15.0);
        object.setPriceAnnually(2500.0);
        object.setPriceMonthly(250.0);
        paymentPlan.setPrices(Arrays.asList(object));
        paymentPlan.setPricesCollection(GenericMapper.objectToJSONMapper(paymentPlan.getPrices()));
        paymentPlanRepsitory.save(paymentPlan);

        List<ServiceTAB> serviceList = new ArrayList<ServiceTAB>();

        ServiceTAB serviceTAB1 = new ServiceTAB();
        serviceTAB1.setServiceName("Bill Payment");
        serviceTAB1.setServiceNameArabic("فاتورة  الدفع");
        serviceTAB1.setServiceDescription("Utility Bill Payments");
        serviceTAB1 = serviceTABRepository.save(serviceTAB1);
        serviceList.add(serviceTAB1);

        ServiceTAB serviceTAB2 = new ServiceTAB();
        serviceTAB2.setServiceName("Cheque Cash");
        serviceTAB2.setServiceNameArabic("الشيكات  النقدية");
        serviceTAB2.setServiceDescription("Cheque Cash");
        serviceTAB2 = serviceTABRepository.save(serviceTAB2);
        serviceList.add(serviceTAB2);

        ServiceTAB serviceTAB3 = new ServiceTAB();
        serviceTAB3.setServiceName("Insurance & Loans");
        serviceTAB3.setServiceNameArabic("التأمين والقروض");
        serviceTAB3.setServiceDescription("Insurance & Loans Repayments");
        serviceTAB3 = serviceTABRepository.save(serviceTAB3);
        serviceList.add(serviceTAB3);

        ServiceTAB serviceTAB4 = new ServiceTAB();
        serviceTAB4.setServiceName("Business financing");
        serviceTAB4.setServiceNameArabic("تمويل الأعمال");
        serviceTAB4.setServiceDescription("Business financing");
        serviceTAB4 = serviceTABRepository.save(serviceTAB4);
        serviceList.add(serviceTAB4);

        ServiceRequirement requiredDocument1 = new ServiceRequirement();
        requiredDocument1.setRequirementName("CNIC");
        requiredDocument1.setRequirementNameArabic("بطاقة الهوية الوطنية");
        requiredDocument1 = serviceRequirementRepository.save(requiredDocument1);

        ServiceRequirement requiredDocument2 = new ServiceRequirement();
        requiredDocument2.setRequirementName("Utility Bill");
        requiredDocument2.setRequirementNameArabic("فاتورة الخدمات");
        requiredDocument2 = serviceRequirementRepository.save(requiredDocument2);

        ServiceRequirement requiredDocument3 = new ServiceRequirement();
        requiredDocument3.setRequirementName("Original Cheque");
        requiredDocument3.setRequirementNameArabic("الشيك الأصلي");
        requiredDocument3 = serviceRequirementRepository.save(requiredDocument3);

        ServiceCenter serviceCenter = new ServiceCenter();
        serviceCenter.setCountry("Pakistan");
        serviceCenter.setServiceSector(serviceSector);
        serviceCenter.setPhoneNumber("+92-3008112990");
        serviceCenter.setServiceCenterName("Meezan Bank");
        serviceCenter.setServiceCenterArabicName("بنك  الميزان");
        serviceCenter = serviceCenterRepository.save(serviceCenter);

        Optional<ServiceCenterConfiguration> serviceCenterConfiguration = serviceCenterConfigurationRepository
                .findById((long) 0);

        if (serviceCenterConfiguration.isPresent()) {
            ServiceCenterConfiguration temp = new ServiceCenterConfiguration();
            temp.setCurrency(serviceCenterConfiguration.get().getCurrency());
            temp.setFastpassAmount(serviceCenterConfiguration.get().getFastpassAmount());
            temp.setFastpassStepoutAllowedCount(serviceCenterConfiguration.get().getFastpassStepoutAllowedCount());
            temp.setFastpassStepoutTime(serviceCenterConfiguration.get().getFastpassStepoutTime());
            temp.setNormalStepoutAllowedCount(serviceCenterConfiguration.get().getNormalStepoutAllowedCount());
            temp.setNormalStepoutTime(serviceCenterConfiguration.get().getNormalStepoutTime());
            temp.setTaboorShareAmount(serviceCenterConfiguration.get().getTaboorShareAmount());
            temp.setServiceCenter(serviceCenter);
            serviceCenterConfigurationRepository.save(temp);
        }

        ServiceCenterServiceTABMapper serviceCenterServiceTABMapper1 = new ServiceCenterServiceTABMapper();
        serviceCenterServiceTABMapper1.setServiceCenter(serviceCenter);
        serviceCenterServiceTABMapper1.setServiceTAB(serviceTAB1);
        serviceCenterServiceTABMapper1.setServiceRequirements(Arrays.asList(requiredDocument1, requiredDocument2));
        serviceCenterServiceTABMapper1 = serviceCenterServiceTABMapperRepository.save(serviceCenterServiceTABMapper1);

        ServiceCenterServiceTABMapper serviceCenterServiceTABMapper2 = new ServiceCenterServiceTABMapper();
        serviceCenterServiceTABMapper2.setServiceCenter(serviceCenter);
        serviceCenterServiceTABMapper2.setServiceTAB(serviceTAB2);
        serviceCenterServiceTABMapper2.setServiceRequirements(Arrays.asList(requiredDocument2, requiredDocument3));
        serviceCenterServiceTABMapper2 = serviceCenterServiceTABMapperRepository.save(serviceCenterServiceTABMapper2);

        ServiceCenterServiceTABMapper serviceCenterServiceTABMapper3 = new ServiceCenterServiceTABMapper();
        serviceCenterServiceTABMapper3.setServiceCenter(serviceCenter);
        serviceCenterServiceTABMapper3.setServiceTAB(serviceTAB3);
        serviceCenterServiceTABMapper3.setServiceRequirements(Arrays.asList(requiredDocument1, requiredDocument3));
        serviceCenterServiceTABMapper3 = serviceCenterServiceTABMapperRepository.save(serviceCenterServiceTABMapper3);

        ServiceCenter serviceCenter2 = new ServiceCenter();
        serviceCenter2.setCountry("Pakistan");
        serviceCenter2.setServiceSector(serviceSector);
        serviceCenter2.setPhoneNumber("+92-3008112990");
        serviceCenter2.setServiceCenterName("HBL Bank");
        serviceCenter2.setServiceCenterArabicName("سرقة  بنك");
        serviceCenter2 = serviceCenterRepository.save(serviceCenter2);

        if (serviceCenterConfiguration.isPresent()) {
            ServiceCenterConfiguration temp = new ServiceCenterConfiguration();
            temp.setCurrency(serviceCenterConfiguration.get().getCurrency());
            temp.setFastpassAmount(serviceCenterConfiguration.get().getFastpassAmount());
            temp.setFastpassStepoutAllowedCount(serviceCenterConfiguration.get().getFastpassStepoutAllowedCount());
            temp.setFastpassStepoutTime(serviceCenterConfiguration.get().getFastpassStepoutTime());
            temp.setNormalStepoutAllowedCount(serviceCenterConfiguration.get().getNormalStepoutAllowedCount());
            temp.setNormalStepoutTime(serviceCenterConfiguration.get().getNormalStepoutTime());
            temp.setTaboorShareAmount(serviceCenterConfiguration.get().getTaboorShareAmount());
            temp.setServiceCenter(serviceCenter2);
            serviceCenterConfigurationRepository.save(temp);
        }

        ServiceCenterServiceTABMapper serviceCenterServiceTABMapper4 = new ServiceCenterServiceTABMapper();
        serviceCenterServiceTABMapper4.setServiceCenter(serviceCenter2);
        serviceCenterServiceTABMapper4.setServiceTAB(serviceTAB1);
        serviceCenterServiceTABMapper4.setServiceRequirements(Arrays.asList(requiredDocument1, requiredDocument2));
        serviceCenterServiceTABMapperRepository.save(serviceCenterServiceTABMapper4);

        ServiceCenterServiceTABMapper serviceCenterServiceTABMapper5 = new ServiceCenterServiceTABMapper();
        serviceCenterServiceTABMapper5.setServiceCenter(serviceCenter2);
        serviceCenterServiceTABMapper5.setServiceTAB(serviceTAB2);
        serviceCenterServiceTABMapper5.setServiceRequirements(Arrays.asList(requiredDocument2, requiredDocument3));
        serviceCenterServiceTABMapperRepository.save(serviceCenterServiceTABMapper5);

        ServiceCenterServiceTABMapper serviceCenterServiceTABMapper6 = new ServiceCenterServiceTABMapper();
        serviceCenterServiceTABMapper6.setServiceCenter(serviceCenter2);
        serviceCenterServiceTABMapper6.setServiceTAB(serviceTAB4);
        serviceCenterServiceTABMapper6.setServiceRequirements(Arrays.asList(requiredDocument1, requiredDocument3));
        serviceCenterServiceTABMapperRepository.save(serviceCenterServiceTABMapper6);

        List<Branch> branchList = new ArrayList<Branch>();

        Branch branch = new Branch();
        branch.setAddress("G-11 Markaz, Islamabad");
        LocalTime a = LocalTime.of(0, 10);
        branch.setAverageServiceTime(a);
        branch.setAverageTicketPerDay(100);
        branch.setAverageWaitingTime(LocalTime.of(0, 18));
        branch.setBranchName("Meezan Bank G-11");
        branch.setBranchNameArabic("بنك الميزان إحدى عشر");
        branch.setBranchStatus(BranchStatus.OPEN.getStatus());
        branch.setCity("Islamabad");
        branch.setCreatedTime(OffsetDateTime.now().withNano(0));
        branch.setEmailAddress("g11@meezan.com");
        branch.setLatitude(33.671025);
        branch.setLongitude(72.995970);
        branch.setPhoneNumber("+92-3008112990");
        branch.setRating(3.5);
        branch.setServiceCenter(serviceCenter);
        branch.setWorkingStatus(true);
        branch = branchRepository.save(branch);
        branchList.add(branch);

        Branch branch2 = new Branch();
        branch2.setAddress("G-10 Markaz, Islamabad");
        branch2.setAverageServiceTime(LocalTime.of(0, 11));
        branch2.setAverageTicketPerDay(100);
        branch2.setAverageWaitingTime(LocalTime.of(0, 16));
        branch2.setBranchName("Meezan Bank G-10");
        branch2.setBranchNameArabic("بنك الميزان عشرة");
        branch2.setBranchStatus(BranchStatus.CLOSED.getStatus());
        branch2.setCity("Islamabad");
        branch2.setCreatedTime(OffsetDateTime.now().withNano(0));
        branch2.setEmailAddress("g10@meezan.com");
        branch2.setLatitude(33.678844);
        branch2.setLongitude(73.013311);
        branch2.setPhoneNumber("+92-3008112990");
        branch2.setRating(5.0);
        branch2.setServiceCenter(serviceCenter);
        branch2.setWorkingStatus(true);
        branch2 = branchRepository.save(branch2);
        branchList.add(branch2);

        Branch branch3 = new Branch();
        branch3.setAddress("G-9 Markaz, Islamabad");
        branch3.setAverageServiceTime(LocalTime.of(0, 12));
        branch3.setAverageTicketPerDay(100);
        branch3.setAverageWaitingTime(LocalTime.of(0, 10));
        branch3.setBranchName("Meezan Bank G-9");
        branch3.setBranchNameArabic("بنك الميزان تسعة");
        branch3.setBranchStatus(BranchStatus.OPEN.getStatus());
        branch3.setCity("Islamabad");
        branch3.setCreatedTime(OffsetDateTime.now().withNano(0));
        branch3.setEmailAddress("g9@meezan.com");
        branch3.setLatitude(33.690887);
        branch3.setLongitude(73.030611);
        branch3.setPhoneNumber("+92-3008112990");
        branch3.setRating(4.0);
        branch3.setServiceCenter(serviceCenter);
        branch3.setWorkingStatus(true);
        branch3 = branchRepository.save(branch3);
        branchList.add(branch3);

        Branch branch4 = new Branch();
        branch4.setAddress("F-10 Markaz, Islamabad");
        branch4.setAverageServiceTime(LocalTime.of(0, 13));
        branch4.setAverageTicketPerDay(100);
        branch4.setAverageWaitingTime(LocalTime.of(0, 19));
        branch4.setBranchName("Meezan Bank F-10");
        branch4.setBranchNameArabic("بنك الميزان فى عشرة");
        branch4.setBranchStatus(BranchStatus.OPEN.getStatus());
        branch4.setCity("Islamabad");
        branch4.setCreatedTime(OffsetDateTime.now().withNano(0));
        branch4.setEmailAddress("f10@meezan.com");
        branch4.setLatitude(33.696169);
        branch4.setLongitude(73.004366);
        branch4.setPhoneNumber("+92-3008112990");
        branch4.setRating(4.5);
        branch4.setServiceCenter(serviceCenter);
        branch4.setWorkingStatus(true);
        branch4 = branchRepository.save(branch4);
        branchList.add(branch4);

        Branch branch5 = new Branch();
        branch5.setAddress("F-11 Markaz, Islamabad");
        branch5.setAverageServiceTime(LocalTime.of(0, 14));
        branch5.setAverageTicketPerDay(100);
        branch5.setAverageWaitingTime(LocalTime.of(0, 8));
        branch5.setBranchName("Meezan Bank F-11");
        branch5.setBranchNameArabic("بنك الميزان فى إحدى عشر");
        branch5.setBranchStatus(BranchStatus.CLOSED.getStatus());
        branch5.setCity("Islamabad");
        branch5.setCreatedTime(OffsetDateTime.now().withNano(0));
        branch5.setEmailAddress("f11@meezan.com");
        branch5.setLatitude(33.685808);
        branch5.setLongitude(72.984448);
        branch5.setPhoneNumber("0302133");
        branch5.setRating(5.0);
        branch5.setServiceCenter(serviceCenter);
        branch5.setWorkingStatus(true);
        branch5 = branchRepository.save(branch5);
        branchList.add(branch5);

        Branch branch6 = new Branch();
        branch6.setAddress("G-11 Markaz, Islamabad");
        branch6.setAverageServiceTime(LocalTime.of(0, 14));
        branch6.setAverageTicketPerDay(100);
        branch6.setAverageWaitingTime(LocalTime.of(0, 12));
        branch6.setBranchName("HBL Bank G-11");
        branch6.setBranchNameArabic("سرقة بنك إحدى عشر");
        branch6.setBranchStatus(BranchStatus.OPEN.getStatus());
        branch6.setCity("Islamabad");
        branch6.setCreatedTime(OffsetDateTime.now().withNano(0));
        branch6.setEmailAddress("g11@meezan.com");
        branch6.setLatitude(33.670454);
        branch6.setLongitude(73.001958);
        branch6.setPhoneNumber("0302");
        branch6.setRating(4.0);
        branch6.setServiceCenter(serviceCenter2);
        branch6.setWorkingStatus(true);
        branch6 = branchRepository.save(branch6);
        branchList.add(branch6);

        Branch branch7 = new Branch();
        branch7.setAddress("G-10 Markaz, Islamabad");
        branch7.setAverageServiceTime(LocalTime.of(0, 13));
        branch7.setAverageTicketPerDay(100);
        branch7.setAverageWaitingTime(LocalTime.of(0, 11));
        branch7.setBranchName("HBL Bank G-10");
        branch7.setBranchNameArabic("سرقة بنك عشرة");
        branch7.setBranchStatus(BranchStatus.CLOSED.getStatus());
        branch7.setCity("Islamabad");
        branch7.setCreatedTime(OffsetDateTime.now().withNano(0));
        branch7.setEmailAddress("g10@hbl.com");
        branch7.setLatitude(33.673140);
        branch7.setLongitude(73.015780);
        branch7.setPhoneNumber("+92-3008112990");
        branch7.setRating(3.5);
        branch7.setServiceCenter(serviceCenter2);
        branch7.setWorkingStatus(true);
        branch7 = branchRepository.save(branch7);
        branchList.add(branch7);

        Branch branch8 = new Branch();
        branch8.setAddress("G-9 Markaz, Islamabad");
        branch8.setAverageServiceTime(LocalTime.of(0, 12));
        branch8.setAverageTicketPerDay(100);
        branch8.setAverageWaitingTime(LocalTime.of(0, 16));
        branch8.setBranchName("HBL Bank G-9");
        branch8.setBranchNameArabic("تسعة سرقة بنك");
        branch8.setBranchStatus(BranchStatus.OPEN.getStatus());
        branch8.setCity("Islamabad");
        branch8.setCreatedTime(OffsetDateTime.now().withNano(0));
        branch8.setEmailAddress("g9@hbl.com");
        branch8.setLatitude(33.685744);
        branch8.setLongitude(73.035715);
        branch8.setPhoneNumber("+92-3008112990");
        branch8.setRating(4.0);
        branch8.setServiceCenter(serviceCenter2);
        branch8.setWorkingStatus(true);
        branch8 = branchRepository.save(branch8);
        branchList.add(branch8);

        Branch branch9 = new Branch();
        branch9.setAddress("F-10 Markaz, Islamabad");
        branch9.setAverageServiceTime(LocalTime.of(0, 11));
        branch9.setAverageTicketPerDay(100);
        branch9.setAverageWaitingTime(LocalTime.of(0, 13));
        branch9.setBranchName("HBL Bank F-10");
        branch9.setBranchNameArabic("سرقة بنك فى عشرة");
        branch9.setBranchStatus(BranchStatus.OPEN.getStatus());
        branch9.setCity("Islamabad");
        branch9.setCreatedTime(OffsetDateTime.now().withNano(0));
        branch9.setEmailAddress("f10@hbl.com");
        branch9.setLatitude(33.687228);
        branch9.setLongitude(73.006622);
        branch9.setPhoneNumber("+92-3008112990");
        branch9.setRating(5.0);
        branch9.setServiceCenter(serviceCenter2);
        branch9.setWorkingStatus(true);
        branch9 = branchRepository.save(branch9);
        branchList.add(branch9);

        Branch branch10 = new Branch();
        branch10.setAddress("F-11 Markaz, Islamabad");
        branch10.setAverageServiceTime(LocalTime.of(0, 10));
        branch10.setAverageTicketPerDay(100);
        branch10.setAverageWaitingTime(LocalTime.of(0, 14));
        branch10.setBranchName("HBL Bank F-11");
        branch10.setBranchNameArabic("سرقة بنك فى إحدى عشر");
        branch10.setBranchStatus(BranchStatus.CLOSED.getStatus());
        branch10.setCity("Islamabad");
        branch10.setCreatedTime(OffsetDateTime.now().withNano(0));
        branch10.setEmailAddress("f11@hbl.com");
        branch10.setLatitude(33.682110);
        branch10.setLongitude(72.993513);
        branch10.setPhoneNumber("+92-3008112990");
        branch10.setRating(4.5);
        branch10.setServiceCenter(serviceCenter2);
        branch10.setWorkingStatus(true);
        branch10 = branchRepository.save(branch10);
        branchList.add(branch10);

        UserRole userRole = userRoleRepository.findByRoleName(RoleName.Agent.name()).get();

        userPrivilegeMappers = new ArrayList<UserPrivilegeMapper>();

        for (int i = 0; i < branchList.size(); i++) {
            // Monday
            BranchWorkingHours branchWorkingHours = new BranchWorkingHours();
            branchWorkingHours.setDay(WeekDays.MONDAY.getStatus());
            branchWorkingHours.setBranch(branchList.get(i));
            branchWorkingHours.setOpeningTime(LocalTime.of(9, 0));
            branchWorkingHours.setClosingTime(LocalTime.of(17, 0));
            branchWorkingHours = branchWorkingHoursRepository.save(branchWorkingHours);

            // Tuesday
            branchWorkingHours = new BranchWorkingHours();
            branchWorkingHours.setDay(WeekDays.TUESDAY.getStatus());
            branchWorkingHours.setBranch(branchList.get(i));
            branchWorkingHours.setOpeningTime(LocalTime.of(9, 0));
            branchWorkingHours.setClosingTime(LocalTime.of(17, 0));
            branchWorkingHours = branchWorkingHoursRepository.save(branchWorkingHours);

            // Wednesday
            branchWorkingHours = new BranchWorkingHours();
            branchWorkingHours.setDay(WeekDays.WEDNESDAY.getStatus());
            branchWorkingHours.setBranch(branchList.get(i));
            branchWorkingHours.setOpeningTime(LocalTime.of(9, 0));
            branchWorkingHours.setClosingTime(LocalTime.of(17, 0));
            branchWorkingHours = branchWorkingHoursRepository.save(branchWorkingHours);

            // Thursday
            branchWorkingHours = new BranchWorkingHours();
            branchWorkingHours.setDay(WeekDays.THURSDAY.getStatus());
            branchWorkingHours.setBranch(branchList.get(i));
            branchWorkingHours.setOpeningTime(LocalTime.of(9, 0));
            branchWorkingHours.setClosingTime(LocalTime.of(17, 0));
            branchWorkingHours = branchWorkingHoursRepository.save(branchWorkingHours);

            // Friday
            branchWorkingHours = new BranchWorkingHours();
            branchWorkingHours.setDay(WeekDays.FRIDAY.getStatus());
            branchWorkingHours.setBranch(branchList.get(i));
            branchWorkingHours.setOpeningTime(LocalTime.of(9, 0));
            branchWorkingHours.setClosingTime(LocalTime.of(17, 0));
            branchWorkingHours = branchWorkingHoursRepository.save(branchWorkingHours);

            BranchCounter branchCounter = new BranchCounter();
            branchCounter.setBranch(branchList.get(i));
            branchCounter.setCounterNumber("ABC-" + (i + 1));
            branchCounter = branchCounterRepository.save(branchCounter);

            User serviceCenterEmployeeUser = new User();
            serviceCenterEmployeeUser.setEmail("z" + i + "@meezan.com");
            serviceCenterEmployeeUser.setName("z" + i);
            serviceCenterEmployeeUser.setCreatedTime(OffsetDateTime.now().withNano(0));
            serviceCenterEmployeeUser.setPassword(Signature.encrypt("P@kistan", passEncryptionKey));
            serviceCenterEmployeeUser.setPhoneNumber("+92-3008112990");
            serviceCenterEmployeeUser.setProfileImageUrl("pic.jpg");
            serviceCenterEmployeeUser.setEmailVerified(true);
            serviceCenterEmployeeUser.setPhoneVerified(true);
            serviceCenterEmployeeUser.setRoleName(RoleName.Agent.name());
            serviceCenterEmployeeUser = userRepository.save(serviceCenterEmployeeUser);

            final User branchEmployeeUsr1 = serviceCenterEmployeeUser;

            for (UserPrivilege entity : userRole.getUserPrivileges()) {
                UserPrivilegeMapper userPrivilegeMapper = new UserPrivilegeMapper();
                userPrivilegeMapper.setUser(branchEmployeeUsr1);
                userPrivilegeMapper.setUserPrivilege(entity);
                userPrivilegeMappers.add(userPrivilegeMapper);
            }

            ServiceCenterEmployee serviceCenterEmployee = new ServiceCenterEmployee();
            serviceCenterEmployee.setEmployeeNumber("ABC123" + "Z" + i);
            serviceCenterEmployee.setServiceCenter(serviceCenter);
            serviceCenterEmployee.setUser(serviceCenterEmployeeUser);
            serviceCenterEmployee = serviceCenterEmployeeRepository.save(serviceCenterEmployee);

            Agent agent = new Agent();
            agent.setServiceCenterEmployee(serviceCenterEmployee);
            agent.setTotalTicketServed(0);
            agent.setBranch(branchList.get(i));
            agent.setTotalWorkHours(LocalTime.of(0, 0));
            agent.setAverageServiceTime(LocalTime.of(0, 0));
            agent.setLoginStatus(false);
            agent = agentRepository.save(agent);

            BranchCounterAgentMapper branchCounterAgentMapper = new BranchCounterAgentMapper();
            branchCounterAgentMapper.setAgent(agent);
            branchCounterAgentMapper.setBranchCounter(branchCounter);
            branchCounterAgentMapper = branchCounterAgentMapperRepository.save(branchCounterAgentMapper);

            BranchCounterServiceTABMapper branchCounterServiceTABMapper = new BranchCounterServiceTABMapper();
            branchCounterServiceTABMapper.setBranchCounter(branchCounter);
            branchCounterServiceTABMapper.setServiceTAB(serviceTAB1);
            branchCounterServiceTABMapper = branchCounterServiceTABMapperRepository.save(branchCounterServiceTABMapper);

            // second counter
            BranchCounter branchCounter2 = new BranchCounter();
            branchCounter2.setBranch(branchList.get(i));
            branchCounter2.setCounterNumber("ZAB-" + (i + 1));
            branchCounter2 = branchCounterRepository.save(branchCounter2);

            User serviceCenterEmployeeUser2 = new User();
            serviceCenterEmployeeUser2.setEmail("zse" + i + "@meezan.com");
            serviceCenterEmployeeUser2.setName("zse" + i);
            serviceCenterEmployeeUser2.setCreatedTime(OffsetDateTime.now().withNano(0));
            serviceCenterEmployeeUser2.setPassword(Signature.encrypt("P@kistan", passEncryptionKey));
            serviceCenterEmployeeUser2.setPhoneNumber("+92-3008112990");
            serviceCenterEmployeeUser2.setProfileImageUrl("pic.jpg");
            serviceCenterEmployeeUser2.setEmailVerified(true);
            serviceCenterEmployeeUser2.setPhoneVerified(true);
            serviceCenterEmployeeUser2.setRoleName(RoleName.Agent.name());
            serviceCenterEmployeeUser2 = userRepository.save(serviceCenterEmployeeUser2);

            final User branchEmployeeUsr2 = serviceCenterEmployeeUser2;

            for (UserPrivilege entity : userRole.getUserPrivileges()) {
                UserPrivilegeMapper userPrivilegeMapper = new UserPrivilegeMapper();
                userPrivilegeMapper.setUser(branchEmployeeUsr2);
                userPrivilegeMapper.setUserPrivilege(entity);
                userPrivilegeMappers.add(userPrivilegeMapper);
            }
            ;

            ServiceCenterEmployee serviceCenterEmployee2 = new ServiceCenterEmployee();
            serviceCenterEmployee2.setEmployeeNumber("RSX123" + "Z" + i);
            serviceCenterEmployee2.setServiceCenter(serviceCenter);
            serviceCenterEmployee2.setUser(serviceCenterEmployeeUser2);
            serviceCenterEmployee2 = serviceCenterEmployeeRepository.save(serviceCenterEmployee2);

            Agent agent2 = new Agent();
            agent2.setServiceCenterEmployee(serviceCenterEmployee2);
            agent2.setTotalTicketServed(0);
            agent2.setBranch(branchList.get(i));
            agent2.setTotalWorkHours(LocalTime.of(0, 0));
            agent2.setAverageServiceTime(LocalTime.of(0, 0));
            agent2.setLoginStatus(false);
            agent2 = agentRepository.save(agent2);

            BranchCounterAgentMapper branchCounterAgentMapper2 = new BranchCounterAgentMapper();
            branchCounterAgentMapper2.setAgent(agent2);
            branchCounterAgentMapper2.setBranchCounter(branchCounter2);
            branchCounterAgentMapper2 = branchCounterAgentMapperRepository.save(branchCounterAgentMapper2);

            BranchCounterServiceTABMapper branchCounterServiceTABMapper2 = new BranchCounterServiceTABMapper();
            branchCounterServiceTABMapper2.setBranchCounter(branchCounter2);
            branchCounterServiceTABMapper2.setServiceTAB(serviceTAB2);
            branchCounterServiceTABMapper2 = branchCounterServiceTABMapperRepository
                    .save(branchCounterServiceTABMapper2);

            if (i % 2 == 0) {

                // third counter
                BranchCounter branchCounter3 = new BranchCounter();
                branchCounter3.setBranch(branchList.get(i));
                branchCounter3.setCounterNumber("BFZ-" + (i + 1));
                branchCounter3 = branchCounterRepository.save(branchCounter3);

                User serviceCenterEmployeeUser3 = new User();
                serviceCenterEmployeeUser3.setEmail("zvf" + i + "@meezan.com");
                serviceCenterEmployeeUser3.setName("zvh" + i);
                serviceCenterEmployeeUser3.setCreatedTime(OffsetDateTime.now().withNano(0));
                serviceCenterEmployeeUser3.setPassword(Signature.encrypt("P@kistan", passEncryptionKey));
                serviceCenterEmployeeUser3.setPhoneNumber("+92-3008112990");
                serviceCenterEmployeeUser3.setProfileImageUrl("pic.jpg");
                serviceCenterEmployeeUser3.setEmailVerified(true);
                serviceCenterEmployeeUser3.setPhoneVerified(true);
                serviceCenterEmployeeUser3.setRoleName(RoleName.Agent.name());
                serviceCenterEmployeeUser3 = userRepository.save(serviceCenterEmployeeUser3);

                final User branchEmployeeUsr3 = serviceCenterEmployeeUser3;

                for (UserPrivilege entity : userRole.getUserPrivileges()) {
                    UserPrivilegeMapper userPrivilegeMapper = new UserPrivilegeMapper();
                    userPrivilegeMapper.setUser(branchEmployeeUsr3);
                    userPrivilegeMapper.setUserPrivilege(entity);
                    userPrivilegeMappers.add(userPrivilegeMapper);
                }

                ServiceCenterEmployee serviceCenterEmployee3 = new ServiceCenterEmployee();
                serviceCenterEmployee3.setEmployeeNumber("RYV123" + "Z" + i);
                serviceCenterEmployee3.setServiceCenter(serviceCenter);
                serviceCenterEmployee3.setUser(serviceCenterEmployeeUser3);
                serviceCenterEmployee3 = serviceCenterEmployeeRepository.save(serviceCenterEmployee3);

                Agent agent3 = new Agent();
                agent3.setServiceCenterEmployee(serviceCenterEmployee3);
                agent3.setTotalTicketServed(0);
                agent3.setBranch(branchList.get(i));
                agent3.setTotalWorkHours(LocalTime.of(0, 0));
                agent3.setAverageServiceTime(LocalTime.of(0, 0));
                agent3.setLoginStatus(false);
                agent3 = agentRepository.save(agent3);

                BranchCounterAgentMapper branchCounterAgentMapper3 = new BranchCounterAgentMapper();
                branchCounterAgentMapper3.setAgent(agent3);
                branchCounterAgentMapper3.setBranchCounter(branchCounter3);
                branchCounterAgentMapper3 = branchCounterAgentMapperRepository.save(branchCounterAgentMapper3);

                BranchCounterServiceTABMapper branchCounterServiceTABMapper3 = new BranchCounterServiceTABMapper();
                branchCounterServiceTABMapper3.setBranchCounter(branchCounter3);
                branchCounterServiceTABMapper3.setServiceTAB(serviceTAB3);
                branchCounterServiceTABMapper3 = branchCounterServiceTABMapperRepository
                        .save(branchCounterServiceTABMapper3);

                // service 1
                BranchServiceTABMapper branchServiceTABMapper = new BranchServiceTABMapper();
//				ServiceTABRequiredDocumentMapper serviceTABRequiredDocumentMapper = new ServiceTABRequiredDocumentMapper();
                Queue queue = new Queue();

                branchServiceTABMapper.setBranch(branchList.get(i));
                branchServiceTABMapper.setServiceTAB(serviceTAB1);
                branchServiceTABMapper = branchServiceTABMapperRepository.save(branchServiceTABMapper);

                queue.setQueueNumber("ABC" + i);
                queue.setBranch(branchList.get(i));
                queue.setService(serviceTAB1);
                queue.setAverageServiceTime(LocalTime.of(0, 20));
                queue.setAverageWaitTime(LocalTime.of(0, 30));
                queue = queueRepository.save(queue);

                // service 2
                branchServiceTABMapper = new BranchServiceTABMapper();
//				serviceTABRequiredDocumentMapper = new ServiceTABRequiredDocumentMapper();
                queue = new Queue();

                branchServiceTABMapper.setBranch(branchList.get(i));
                branchServiceTABMapper.setServiceTAB(serviceTAB2);
                branchServiceTABMapper = branchServiceTABMapperRepository.save(branchServiceTABMapper);

                queue.setQueueNumber("AXC" + i);
                queue.setBranch(branchList.get(i));
                queue.setService(serviceTAB2);
                queue.setAverageServiceTime(LocalTime.of(0, 20));
                queue.setAverageWaitTime(LocalTime.of(0, 30));
                queue = queueRepository.save(queue);

                // service 3
                branchServiceTABMapper = new BranchServiceTABMapper();
//				serviceTABRequiredDocumentMapper = new ServiceTABRequiredDocumentMapper();
                queue = new Queue();

                branchServiceTABMapper.setBranch(branchList.get(i));
                branchServiceTABMapper.setServiceTAB(serviceTAB3);
                branchServiceTABMapper = branchServiceTABMapperRepository.save(branchServiceTABMapper);

                queue.setQueueNumber("AJC" + i);
                queue.setBranch(branchList.get(i));
                queue.setService(serviceTAB3);
                queue.setAverageServiceTime(LocalTime.of(0, 20));
                queue.setAverageWaitTime(LocalTime.of(0, 30));
                queue = queueRepository.save(queue);

            } else {

                // third counter
                BranchCounter branchCounter3 = new BranchCounter();
                branchCounter3.setBranch(branchList.get(i));
                branchCounter3.setCounterNumber("BNZ-" + (i + 1));
                branchCounter3 = branchCounterRepository.save(branchCounter3);

                User serviceCenterEmployeeUser3 = new User();
                serviceCenterEmployeeUser3.setEmail("zrf" + i + "@meezan.com");
                serviceCenterEmployeeUser3.setName("zdh" + i);
                serviceCenterEmployeeUser3.setCreatedTime(OffsetDateTime.now().withNano(0));
                serviceCenterEmployeeUser3.setPassword(Signature.encrypt("P@kistan", passEncryptionKey));
                serviceCenterEmployeeUser3.setPhoneNumber("+92-3008112990");
                serviceCenterEmployeeUser3.setProfileImageUrl("pic.jpg");
                serviceCenterEmployeeUser3.setEmailVerified(true);
                serviceCenterEmployeeUser3.setPhoneVerified(true);
                serviceCenterEmployeeUser3.setRoleName(RoleName.Agent.name());
                serviceCenterEmployeeUser3 = userRepository.save(serviceCenterEmployeeUser3);

                final User branchEmployeeUsr3 = serviceCenterEmployeeUser3;

                for (UserPrivilege entity : userRole.getUserPrivileges()) {
                    UserPrivilegeMapper userPrivilegeMapper = new UserPrivilegeMapper();
                    userPrivilegeMapper.setUser(branchEmployeeUsr3);
                    userPrivilegeMapper.setUserPrivilege(entity);
                    userPrivilegeMappers.add(userPrivilegeMapper);
                }

                ServiceCenterEmployee serviceCenterEmployee3 = new ServiceCenterEmployee();
                serviceCenterEmployee3.setEmployeeNumber("RJK908" + "Z" + i);
                serviceCenterEmployee3.setServiceCenter(serviceCenter);
                serviceCenterEmployee3.setUser(serviceCenterEmployeeUser3);
                serviceCenterEmployee3 = serviceCenterEmployeeRepository.save(serviceCenterEmployee3);

                Agent agent3 = new Agent();
                agent3.setServiceCenterEmployee(serviceCenterEmployee3);
                agent3.setTotalTicketServed(0);
                agent3.setBranch(branchList.get(i));
                agent3.setTotalWorkHours(LocalTime.of(0, 0));
                agent3.setAverageServiceTime(LocalTime.of(0, 0));
                agent3.setLoginStatus(false);
                agent3 = agentRepository.save(agent3);

                BranchCounterAgentMapper branchCounterAgentMapper3 = new BranchCounterAgentMapper();
                branchCounterAgentMapper3.setAgent(agent3);
                branchCounterAgentMapper3.setBranchCounter(branchCounter3);
                branchCounterAgentMapper3 = branchCounterAgentMapperRepository.save(branchCounterAgentMapper3);

                BranchCounterServiceTABMapper branchCounterServiceTABMapper3 = new BranchCounterServiceTABMapper();
                branchCounterServiceTABMapper3.setBranchCounter(branchCounter3);
                branchCounterServiceTABMapper3.setServiceTAB(serviceTAB4);
                branchCounterServiceTABMapper3 = branchCounterServiceTABMapperRepository
                        .save(branchCounterServiceTABMapper3);

                BranchServiceTABMapper branchServiceTABMapper = new BranchServiceTABMapper();
//				ServiceTABRequiredDocumentMapper serviceTABRequiredDocumentMapper = new ServiceTABRequiredDocumentMapper();
                Queue queue = new Queue();

                branchServiceTABMapper.setBranch(branchList.get(i));
                branchServiceTABMapper.setServiceTAB(serviceTAB1);
                branchServiceTABMapper = branchServiceTABMapperRepository.save(branchServiceTABMapper);

                queue.setQueueNumber("ABC" + i);
                queue.setBranch(branchList.get(i));
                queue.setService(serviceTAB1);
                queue.setAverageServiceTime(LocalTime.of(0, 20));
                queue.setAverageWaitTime(LocalTime.of(0, 30));
                queue = queueRepository.save(queue);

                // service 2
                branchServiceTABMapper = new BranchServiceTABMapper();
//				serviceTABRequiredDocumentMapper = new ServiceTABRequiredDocumentMapper();
                queue = new Queue();

                branchServiceTABMapper.setBranch(branchList.get(i));
                branchServiceTABMapper.setServiceTAB(serviceTAB2);
                branchServiceTABMapper = branchServiceTABMapperRepository.save(branchServiceTABMapper);

                queue.setQueueNumber("AXC" + i);
                queue.setBranch(branchList.get(i));
                queue.setService(serviceTAB2);
                queue.setAverageServiceTime(LocalTime.of(0, 20));
                queue.setAverageWaitTime(LocalTime.of(0, 30));
                queue = queueRepository.save(queue);

                // service 3
                branchServiceTABMapper = new BranchServiceTABMapper();
//				serviceTABRequiredDocumentMapper = new ServiceTABRequiredDocumentMapper();
                queue = new Queue();

                branchServiceTABMapper.setBranch(branchList.get(i));
                branchServiceTABMapper.setServiceTAB(serviceTAB4);
                branchServiceTABMapper = branchServiceTABMapperRepository.save(branchServiceTABMapper);

                queue.setQueueNumber("AJC" + i);
                queue.setBranch(branchList.get(i));
                queue.setService(serviceTAB4);
                queue.setAverageServiceTime(LocalTime.of(0, 20));
                queue.setAverageWaitTime(LocalTime.of(0, 30));
                queue = queueRepository.save(queue);

            }
        }

        userPrivilegeMappers = userPrivilegeMapperRepository.saveAll(userPrivilegeMappers);

        User customerUser = new User();
        customerUser.setEmail("zeeshan@gmail.com");
        customerUser.setName("Zeeshan");
        customerUser.setCreatedTime(OffsetDateTime.now().withNano(0));
        customerUser.setPassword(Signature.encrypt("P@kistan", passEncryptionKey));
        customerUser.setPhoneNumber("0302");
        customerUser.setProfileImageUrl("pic.jpg");
        customerUser.setEmailVerified(true);
        customerUser.setPhoneVerified(true);
        customerUser = userRepository.save(customerUser);

        Customer customer = new Customer();
        customer.setUser(customerUser);
//		customer.setEmailVerified(true);
        customer = customerRepository.save(customer);

        Ticket ticket = new Ticket();
        ticket.setCreatedTime(OffsetDateTime.now().withNano(0));
        ticket.setService(serviceTAB1);
        ticket.setBranch(branch);
        ticket.setTicketNumber("ABC123");
        ticket.setTicketStatus(TicketStatus.BOOKED.getStatus());
        ticket.setTicketType(TicketType.STANDARD.getStatus());
        ticket = ticketRepository.save(ticket);

        TicketUserMapper ticketUserMapper = new TicketUserMapper();
        ticketUserMapper.setTicket(ticket);
        ticketUserMapper.setUser(customer.getUser());
        ticketUserMapper = ticketUserMapperRepository.save(ticketUserMapper);

        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setPaymentMethodName("Bank Credit Card");
        paymentMethod.setPaymentMethodImageUrl("pic.jpg");
        paymentMethod.setPaymentMethodDescription("Credit Card Functionalities");
        paymentMethod = paymentMethodRepository.save(paymentMethod);

        UserBankCard userBankCard = new UserBankCard();
        userBankCard.setBalance(0.0);
        userBankCard.setCardCvv("123");
        userBankCard.setCardNumber("12345");
        userBankCard.setCardTitle("Muhammad Zeeshan");
        userBankCard.setCardType("Credit Card");
        userBankCard.setExpiryMonth(LocalDate.now().plusYears(3).getMonthValue());
        userBankCard.setExpiryYear(LocalDate.now().plusYears(3).getYear());
        userBankCard.setPaymentMethod(paymentMethod);
        userBankCard.setUser(customer.getUser());
        userBankCard = userBankCardRepository.save(userBankCard);

        UserPaymentTransaction userPaymentTransaction = new UserPaymentTransaction();
        userPaymentTransaction.setUserBankCard(userBankCard);
        userPaymentTransaction.setAmount(20.0);
        userPaymentTransaction.setCreatedTime(OffsetDateTime.now().withNano(0));
        userPaymentTransaction.setCompletedTime(OffsetDateTime.now().withNano(0));
        userPaymentTransaction.setExpiryTime(OffsetDateTime.now().withNano(0));
        userPaymentTransaction.setPaymentMethod(userBankCard.getPaymentMethod());
        userPaymentTransaction.setTransactionStatus(0);
        userPaymentTransaction.setTransactionType(0);
        userPaymentTransaction = userPaymentTransactionRepository.save(userPaymentTransaction);

        TicketFastpass ticketFastpass = new TicketFastpass();
        ticketFastpass.setApplyTime(OffsetDateTime.now().withNano(0));
        ticketFastpass.setCharges(20.0);
        ticketFastpass.setStatus(TicketFastpassStatus.CONFIRMED.getStatus());
        ticketFastpass.setTicket(ticket);
        ticketFastpass.setUserPaymentTransaction(userPaymentTransaction);
        ticketFastpass = ticketFastpassRepository.save(ticketFastpass);

        ticket.setTicketType(TicketType.FASTPASS.getStatus());
        ticket = ticketRepository.save(ticket);

        Optional<Queue> queue = queueRepository.findByBranchAndServiceAndType(branch.getBranchId(),
                serviceTAB1.getServiceId());
        QueueTicketMapper queueTicketMapper = new QueueTicketMapper();
        queueTicketMapper.setQueue(queue.get());
        queueTicketMapper.setTicket(ticket);

        ticket.setTicketStatus(TicketStatus.WAITING.getStatus());
        ticket = ticketRepository.save(ticket);
        queueTicketMapper = queueTicketMapperRepository.save(queueTicketMapper);

        TicketFeedback ticketFeedback = new TicketFeedback();
        ticketFeedback.setComment("Great Service");
        ticketFeedback.setRating(5.0);
        ticketFeedback.setServicePoints(ServiceReviewStatus.SATISFIED.getStatus());
        ticketFeedback.setTicket(ticket);
        ticketFeedback.setUser(customerUser);
        ticketFeedbackRepository.save(ticketFeedback);

        User branchEmployeeUser = new User();
        branchEmployeeUser.setEmail("branchAdmin" + "@admin.com");
        branchEmployeeUser.setName("Branch Admin");
        branchEmployeeUser.setCreatedTime(OffsetDateTime.now().withNano(0));
        branchEmployeeUser.setPassword(Signature.encrypt("P@kistan", passEncryptionKey));
        branchEmployeeUser.setPhoneNumber("+92-3008112990");
        branchEmployeeUser.setProfileImageUrl("pic.jpg");
        branchEmployeeUser.setEmailVerified(true);
        branchEmployeeUser.setPhoneVerified(true);
        branchEmployeeUser.setRoleName(RoleName.Branch_Admin.name());
        branchEmployeeUser = userRepository.save(branchEmployeeUser);

        ServiceCenterEmployee branchEmployee = new ServiceCenterEmployee();
        branchEmployee.setEmployeeNumber("BRAD33");
        branchEmployee.setServiceCenter(serviceCenter);
        branchEmployee.setUser(branchEmployeeUser);
        branchEmployee = serviceCenterEmployeeRepository.save(branchEmployee);

        ServiceCenterEmpBranchMapper serviceCenterEmpBranchMapper = new ServiceCenterEmpBranchMapper();
        serviceCenterEmpBranchMapper.setBranch(branch);
        serviceCenterEmpBranchMapper.setServiceCenterEmployee(branchEmployee);
        serviceCenterEmpBranchMapper = serviceCenterEmpBranchMapperRepository.save(serviceCenterEmpBranchMapper);

        serviceCenterEmpBranchMapper = new ServiceCenterEmpBranchMapper();
        serviceCenterEmpBranchMapper.setBranch(branch2);
        serviceCenterEmpBranchMapper.setServiceCenterEmployee(branchEmployee);
        serviceCenterEmpBranchMapper = serviceCenterEmpBranchMapperRepository.save(serviceCenterEmpBranchMapper);

        serviceCenterEmpBranchMapper = new ServiceCenterEmpBranchMapper();
        serviceCenterEmpBranchMapper.setBranch(branch3);
        serviceCenterEmpBranchMapper.setServiceCenterEmployee(branchEmployee);
        serviceCenterEmpBranchMapper = serviceCenterEmpBranchMapperRepository.save(serviceCenterEmpBranchMapper);

        serviceCenterEmpBranchMapper = new ServiceCenterEmpBranchMapper();
        serviceCenterEmpBranchMapper.setBranch(branch7);
        serviceCenterEmpBranchMapper.setServiceCenterEmployee(branchEmployee);
        serviceCenterEmpBranchMapper = serviceCenterEmpBranchMapperRepository.save(serviceCenterEmpBranchMapper);

        serviceCenterEmpBranchMapper = new ServiceCenterEmpBranchMapper();
        serviceCenterEmpBranchMapper.setBranch(branch6);
        serviceCenterEmpBranchMapper.setServiceCenterEmployee(branchEmployee);
        serviceCenterEmpBranchMapper = serviceCenterEmpBranchMapperRepository.save(serviceCenterEmpBranchMapper);

        UserRole userRole1 = userRoleRepository.findByRoleName(RoleName.Branch_Admin.name()).get();

        List<UserPrivilegeMapper> userPrivilegeMappers1 = new ArrayList<UserPrivilegeMapper>();
        final User branchEmployeeUsr = branchEmployeeUser;

        userRole1.getUserPrivileges().forEach(entity -> {
            UserPrivilegeMapper userPrivilegeMapper = new UserPrivilegeMapper();
            userPrivilegeMapper.setUser(branchEmployeeUsr);
            userPrivilegeMapper.setUserPrivilege(entity);
            userPrivilegeMappers1.add(userPrivilegeMapper);
        });

        userPrivilegeMapperRepository.saveAll(userPrivilegeMappers1);
//		uri = dbConnectorMicroserviceURL + "/tab/userprivilegemapper/saveAll";
//		HttpEntity<List> httpEntity2 = new HttpEntity<>(userPrivilegeMappers1,
//				HttpHeadersUtils.getApplicationJsonHeader());
//		ResponseEntity<Boolean> createUserPrivilegeMappersResponse1 = restTemplate.postForEntity(uri, httpEntity2,
//				Boolean.class);
//
//		if (!createUserPrivilegeMappersResponse1.getStatusCode().equals(HttpStatus.OK)
//				|| createUserPrivilegeMappersResponse1.getBody().equals(false))
//			throw new TaboorQMSServiceException(xyzErrorMessage.USERPRIVILEGEMAPPER_NOT_CREATED.getErrorCode() + "::"
//					+ xyzErrorMessage.USERPRIVILEGEMAPPER_NOT_CREATED.getErrorDetails());

        return new GenStatusResponse(0, "Success");
    }

    @RequestMapping(value = "/entry/superadmin", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    GenStatusResponse addSuperAdmin() throws Exception, Throwable{
         User superAdminUser = new User();
        superAdminUser.setEmail("superadmin@taboor.com");
        superAdminUser.setCreatedTime(OffsetDateTime.now());
        superAdminUser.setName("Super Admin User");
        superAdminUser.setPhoneNumber("+92-3008112990");
        superAdminUser.setRoleName(RoleName.Taboor_Admin.name());
        superAdminUser.setEmailVerified(true);
        superAdminUser.setPhoneVerified(true);
        superAdminUser.setPassword(Signature.encrypt("P@kistan", passEncryptionKey));

        superAdminUser = userRepository.save(superAdminUser);

        TaboorEmployee taboorEmployee = new TaboorEmployee();
        taboorEmployee.setEmployeeNumber("TAB0001");
        taboorEmployee.setUser(superAdminUser);
        taboorEmployeeRepository.save(taboorEmployee);

        UserConfiguration configuration = new UserConfiguration();
        configuration.setEmailAllowed(true);
        configuration.setInAppNotificationAllowed(true);
        configuration.setSmsAllowed(true);
        configuration.setUser(superAdminUser);
        UserConfigurationRepository.save(configuration);

        UserNotificationType userNotificationType = new UserNotificationType();
        userNotificationType.setCustomerFeedback1Star(false);
        userNotificationType.setCustomerFeedback2Star(false);
        userNotificationType.setCustomerFeedback3Star(false);
        userNotificationType.setCustomerFeedback4Star(false);
        userNotificationType.setCustomerFeedback5Star(false);
        userNotificationType.setNewOrUpdateAgent(false);
        userNotificationType.setNewOrUpdateBranch(false);
        userNotificationType.setNewOrUpdateServiceCenter(true);
        userNotificationType.setNewOrUpdateUser(true);
        userNotificationType.setUser(superAdminUser);
        userNotificationTypeRepository.save(userNotificationType);

        UserRole superAdminRole = userRoleRepository.findByRoleName(RoleName.Taboor_Admin.name()).get();

        UserRoleMapper userRoleMapper = new UserRoleMapper();
        userRoleMapper.setUser(superAdminUser);
        userRoleMapper.setUserRole(superAdminRole);
        userRoleMapperRepository.save(userRoleMapper);

        List<UserPrivilegeMapper> userPrivilegeMappers = new ArrayList<UserPrivilegeMapper>();

        for (UserPrivilege entity : superAdminRole.getUserPrivileges()) {
            UserPrivilegeMapper userPrivilegeMapper = new UserPrivilegeMapper();
            userPrivilegeMapper.setUser(superAdminUser);
            userPrivilegeMapper.setUserPrivilege(entity);
            userPrivilegeMappers.add(userPrivilegeMapper);
        }
        userPrivilegeMappers = userPrivilegeMapperRepository.saveAll(userPrivilegeMappers);
        return new GenStatusResponse(0, "Success");
    }
}
