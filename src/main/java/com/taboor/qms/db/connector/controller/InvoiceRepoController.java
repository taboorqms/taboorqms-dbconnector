package com.taboor.qms.db.connector.controller;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.Invoice;
import com.taboor.qms.core.model.InvoiceItem;
import com.taboor.qms.core.model.ServiceCenter;
import com.taboor.qms.core.model.ServiceCenterSubscription;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.db.connector.repository.InvoiceRepository;
import com.taboor.qms.db.connector.repository.ServiceCenterSubscriptionRepository;

@RestController
@RequestMapping("/tab/invoices")
@CrossOrigin(origins = "*")
public class InvoiceRepoController {

	@Autowired
	InvoiceRepository invoiceRepository;

	@Autowired
	ServiceCenterSubscriptionRepository serviceCenterSubscriptionRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveInvoice(@RequestBody @Valid final Invoice invoice)
			throws Exception, Throwable {
		invoice.setInvoiceItems(GenericMapper.objectToJSONMapper(invoice.getInvoiceItemList()));
		return ResponseEntity.ok(invoiceRepository.save(invoice));
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<Invoice> entityList = invoiceRepository.findAll();
		if (entityList.isEmpty())
			return ResponseEntity.ok(null);
		for (Invoice entity : entityList)
			entity.setInvoiceItemList(
					GenericMapper.jsonToListObjectMapper(entity.getInvoiceItems(), InvoiceItem.class));
		return ResponseEntity.ok(entityList);
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long invoiceId)
			throws Exception, Throwable {

		Optional<Invoice> entity = invoiceRepository.findById(invoiceId);
		if (entity.isPresent()) {
			entity.get().setInvoiceItemList(
					GenericMapper.jsonToListObjectMapper(entity.get().getInvoiceItems(), InvoiceItem.class));
			return ResponseEntity.ok(entity.get());
		}
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/serviceCenter", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByServiceCenterId(@RequestParam @Valid final Long serviceCenterId)
			throws Exception, Throwable {

		ServiceCenter serviceCenter = new ServiceCenter();
		serviceCenter.setServiceCenterId(serviceCenterId);
		List<Invoice> entityList = invoiceRepository.findByServiceCenter(serviceCenter);
		for (Invoice entity : entityList)
			entity.setInvoiceItemList(
					GenericMapper.jsonToListObjectMapper(entity.getInvoiceItems(), InvoiceItem.class));
		return ResponseEntity.ok(entityList);
	}

	@RequestMapping(value = "/getNextNumber", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getNextNumber() throws Exception, Throwable {

		String nextInvoiceNumber = invoiceRepository.findNextInvoiceNumber();
		if (nextInvoiceNumber == null)
			nextInvoiceNumber = String.format("%06d", 1);
		else
			nextInvoiceNumber = String.format("%06d", Long.valueOf(nextInvoiceNumber) + 1);
		return ResponseEntity.ok(nextInvoiceNumber);
	}

	@RequestMapping(value = "/delete/id", method = RequestMethod.GET, produces = "application/json")
	@Transactional
	public @ResponseBody ResponseEntity<?> deleteById(@RequestParam @Valid final Long invoiceId)
			throws Exception, Throwable {

		Optional<Invoice> entity = invoiceRepository.findById(invoiceId);
		if (entity.isPresent()) {
			ServiceCenterSubscription subscription = entity.get().getServiceCenterSubscription();
			subscription.setNoOfInvoices(subscription.getNoOfInvoices() - 1);
			invoiceRepository.deleteById(entity.get().getInvoiceId());
			serviceCenterSubscriptionRepository.save(entity.get().getServiceCenterSubscription());
			return ResponseEntity.ok(true);
		}
		return ResponseEntity.ok(null);
	}
}
