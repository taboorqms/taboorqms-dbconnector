package com.taboor.qms.db.connector.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.Customer;

@Repository
@Transactional
public interface CustomerRepository extends JpaRepository<Customer, Long> {

	@Query("SELECT c FROM Customer c right join User u on c.user = u.userId where u.email = :userEmail")
	Optional<Customer> findByUserEmail(@Param("userEmail") String userEmail);

	@Query("select c from Customer c where user_id=:userId")
	Optional<Customer> findByUserId(Long userId);

}
