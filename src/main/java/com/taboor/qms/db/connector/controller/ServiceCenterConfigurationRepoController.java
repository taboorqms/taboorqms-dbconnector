package com.taboor.qms.db.connector.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.ServiceCenterConfiguration;
import com.taboor.qms.db.connector.repository.ServiceCenterConfigurationRepository;

@RestController
@RequestMapping("/tab/servicecenterconfigurations")
@CrossOrigin(origins = "*")
public class ServiceCenterConfigurationRepoController {

	@Autowired
	ServiceCenterConfigurationRepository serviceCenterConfigurationRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveServiceCenterConfiguration(
			@RequestBody @Valid final ServiceCenterConfiguration serviceCenterConfiguration)
			throws Exception, Throwable {
		return ResponseEntity.ok(serviceCenterConfigurationRepository.save(serviceCenterConfiguration));
	}

	@RequestMapping(value = "/get/serviceCenter", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByServiceCenter(@RequestParam @Valid final Long serviceCenterId)
			throws Exception, Throwable {

		ServiceCenterConfiguration entity = serviceCenterConfigurationRepository.findByServiceCenterId(serviceCenterId);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/default", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getDefault() throws Exception, Throwable {

		ServiceCenterConfiguration entity = serviceCenterConfigurationRepository.findDefaultConfiguration();
		return ResponseEntity.ok(entity);
	}

}
