package com.taboor.qms.db.connector.controller;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.BranchServiceTABMapper;
import com.taboor.qms.core.model.ServiceTAB;
import com.taboor.qms.core.payload.GetByIdListPayload;
import com.taboor.qms.db.connector.repository.BranchCounterServiceTABMapperRepository;
import com.taboor.qms.db.connector.repository.BranchServiceTABMapperRepository;
import com.taboor.qms.db.connector.repository.QueueRepository;

@RestController
@RequestMapping("/tab/branchservicetabmapper")
@CrossOrigin(origins = "*")
public class BranchServiceTABMapperRepoController {

	@Autowired
	BranchServiceTABMapperRepository branchServiceTABMapperRepository;

	@Autowired
	BranchCounterServiceTABMapperRepository branchCounterServiceTABMapperRepository;

	@Autowired
	QueueRepository queueRepository;

	@PersistenceContext
	EntityManager em;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveBranchServiceTABMapper(
			@RequestBody @Valid final BranchServiceTABMapper branchServiceTABMapper) throws Exception, Throwable {
		return ResponseEntity.ok(branchServiceTABMapperRepository.save(branchServiceTABMapper));
	}

	@RequestMapping(value = "/saveAll", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveAll(
			@RequestBody @Valid final List<BranchServiceTABMapper> branchServiceTABMappers)
			throws Exception, Throwable {
		return ResponseEntity.ok(branchServiceTABMapperRepository.saveAll(branchServiceTABMappers));
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<BranchServiceTABMapper> entity = branchServiceTABMapperRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/delete/branch", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteByBranch(@RequestParam @Valid final Long branchId)
			throws Exception, Throwable {

		int entity = branchServiceTABMapperRepository.deleteByBranch(branchId);
		if (entity == 1)
			return ResponseEntity.ok(true);
		else
			return ResponseEntity.ok(false);
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long branchServiceTABMapperId)
			throws Exception, Throwable {

		Optional<BranchServiceTABMapper> entity = branchServiceTABMapperRepository.findById(branchServiceTABMapperId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/getService/branch", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByBranchId(@RequestParam @Valid final Long branchId)
			throws Exception, Throwable {

		List<ServiceTAB> entity = branchServiceTABMapperRepository.findServiceByBranchId(branchId);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/getService/branchAndService", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByBranchIdAndServiceId(@RequestParam @Valid final Long branchId,
			@RequestParam @Valid final Long serviceId) throws Exception, Throwable {

		Optional<ServiceTAB> entity = branchServiceTABMapperRepository.findServiceByBranchIdAndServiceId(branchId,
				serviceId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/getServiceCount/branch", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getServiceCountByBranchId(@RequestParam @Valid final Long branchId)
			throws Exception, Throwable {

		int entity = branchServiceTABMapperRepository.countByBranchId(branchId);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/delete/branchAndService", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteBranchService(@RequestBody @Valid final GetByIdListPayload payload)
			throws Exception, Throwable {

		// 0- BranchId & 1- ServiceId
		int entity = branchServiceTABMapperRepository.deleteByBranchAndService(payload.getIds().get(0),
				payload.getIds().get(1));

		Branch branch = new Branch();
		branch.setBranchId(payload.getIds().get(0));
		ServiceTAB serviceTAB = new ServiceTAB();
		serviceTAB.setServiceId(payload.getIds().get(1));

		branchCounterServiceTABMapperRepository
				.deleteAll(branchCounterServiceTABMapperRepository.findByBranchAndService(branch, serviceTAB));

		queueRepository.deleteByBranchAndService(branch, serviceTAB);

		if (entity > 0)
			return ResponseEntity.ok(true);
		else
			return ResponseEntity.ok(false);
	}

}
