package com.taboor.qms.db.connector.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.TicketUserMapper;

@Repository
@Transactional
public interface TicketUserMapperRepository extends JpaRepository<TicketUserMapper, Long> {

	@Query("SELECT tum FROM TicketUserMapper tum inner join Ticket t on tum.ticket=t.ticketId where t.ticketId = :ticketId")
	Optional<TicketUserMapper> findByTicketId(@Param("ticketId") Long ticketId);

	@Query("SELECT count(tum) FROM TicketUserMapper tum inner join Ticket t on tum.ticket=t.ticketId where t.ticketStatus < 4 and t.ticketType < 4 and user_id = :userId")
	int countInQueueByUser(@Param("userId") Long userId);

}
