package com.taboor.qms.db.connector.repository;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.BranchBookmark;

@Repository
@Transactional
public interface BranchBookmarkRepository extends JpaRepository<BranchBookmark, Long> {

	@Query("select b.branchId from BranchBookmark bb inner join Branch b on b.branchId=bb.branch where user_id=:userId")
	List<Long> findBranchIdsByUserId(@Param("userId") Long userId);

	@Query("select branchBookmarkId from BranchBookmark where user_id =:userId and branch_id =:branchId")
	Long findIdsByUserAndBranch(@Param("userId") Long userId, @Param("branchId") Long branchId);

	@Modifying
	@Query("DELETE from BranchBookmark where branchBookmarkId = :branchBookmarkId")
	int deleteByBranchBookmarkId(@Param("branchBookmarkId") Long branchBookmarkId);

	@Query("select b.branch from BranchBookmark b where user_id =:userId")
	List<Branch> findBranchByUserId(@Valid Long userId);

	@Query("select count(b.branchBookmarkId) from BranchBookmark b where user_id =:userId")
	int findByUserId(@Valid Long userId);

	@Query("select count(b.branchBookmarkId) from BranchBookmark b where user_id =:userId and branch_id = :branchId")
	int checkByUserId(@Param("userId") Long userId, @Param("branchId") Long branchId);

}
