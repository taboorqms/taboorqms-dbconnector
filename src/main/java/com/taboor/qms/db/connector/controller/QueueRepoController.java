package com.taboor.qms.db.connector.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.Queue;
import com.taboor.qms.core.model.Ticket;
import com.taboor.qms.core.response.GetQueueListingResponse.QueueListingObject;
import com.taboor.qms.core.utils.TicketStatus;
import com.taboor.qms.core.utils.TicketType;
import com.taboor.qms.db.connector.repository.BranchCounterRepository;
import com.taboor.qms.db.connector.repository.BranchCounterServiceTABMapperRepository;
import com.taboor.qms.db.connector.repository.QueueRepository;
import com.taboor.qms.db.connector.repository.QueueTicketMapperRepository;
import com.taboor.qms.db.connector.repository.TicketFeedbackRepository;

@RestController
@RequestMapping("/tab/queues")
@CrossOrigin(origins = "*")
public class QueueRepoController {

	@Autowired
	QueueRepository queueRepository;

	@Autowired
	QueueTicketMapperRepository queueTicketMapperRepository;

	@Autowired
	BranchCounterRepository branchCounterRepository;

	@Autowired
	BranchCounterServiceTABMapperRepository branchCounterServiceTABMapperRepository;

	@Autowired
	TicketFeedbackRepository ticketFeedbackRepository;

	@PersistenceContext
	EntityManager em;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveQueue(@RequestBody @Valid final Queue queue)
			throws Exception, Throwable {
		return ResponseEntity.ok(queueRepository.save(queue));
	}

	@RequestMapping(value = "/saveAll", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveAll(@RequestBody @Valid final List<Queue> queueList)
			throws Exception, Throwable {
		return ResponseEntity.ok(queueRepository.saveAll(queueList));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long queueId)
			throws Exception, Throwable {

		Optional<Queue> entity = queueRepository.findById(queueId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<Queue> entity = queueRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/branchAndService", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByBranchAndServiceAndType(@RequestParam @Valid final Long branchId,
			@RequestParam @Valid final Long serviceId) throws Exception, Throwable {

		Optional<Queue> entity = queueRepository.findByBranchAndServiceAndType(branchId, serviceId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/branch", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByBranchAndServiceAndType(@RequestParam @Valid final Long branchId)
			throws Exception, Throwable {

		List<Queue> entity = queueRepository.findByBranch(branchId);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/getQueueListing/branchList", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getListingByBranch(@RequestBody @Valid final List<Long> branchIdList)
			throws Exception, Throwable {
		List<Queue> queueList;
		if (branchIdList.isEmpty())
			queueList = new ArrayList<Queue>();
		else
			queueList = queueRepository.findByBranchIdList(branchIdList);
		List<QueueListingObject> queueListingObjects = getQueueObjectListDetails(queueList);
		return ResponseEntity.ok(queueListingObjects);
	}

	@RequestMapping(value = "/count/branchList", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> countByBranch(@RequestBody @Valid final List<Long> branchIdList)
			throws Exception, Throwable {
		return ResponseEntity.ok(queueRepository.countByBranchIdList(branchIdList));
	}

	@RequestMapping(value = "/getQueueListing/queue", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getListingByQueue(@RequestBody @Valid final Queue queue)
			throws Exception, Throwable {
		return ResponseEntity.ok(getQueueObjectListDetails(Arrays.asList(queue)));
	}

	private List<QueueListingObject> getQueueObjectListDetails(List<Queue> queueList) {
		List<QueueListingObject> queueListingObjects = new ArrayList<QueueListingObject>();
		for (Queue queue : queueList) {
			QueueListingObject queueObject = new QueueListingObject();
			List<Ticket> ticketList = queueTicketMapperRepository.findTicketByQueue(queue.getQueueId());
			queueObject.setNoOfTickets(ticketList.size());
			List<Long> ticketIdList = new ArrayList<Long>();
			for (Ticket ticket : ticketList) {
				ticketIdList.add(ticket.getTicketId());
				if (ticket.getTicketType() == TicketType.FASTPASS.getStatus())
					queueObject.incrementNoOfFastpassTicket();
				if (ticket.getTicketStatus() == TicketStatus.WAITING.getStatus()
						|| ticket.getTicketStatus() == TicketStatus.SERVING.getStatus())
					queueObject.incrementNoOfInQueueTickets();
			}
			if (ticketIdList.isEmpty())
				queueObject.setRating(0);
			else {
				Query query = em.createNativeQuery(
						"select round(avg(rating)) from tab_ticket_feedback where ticket_id IN :ticketIdList");
				query.setParameter("ticketIdList", ticketIdList);
				queueObject.setRating(query.getFirstResult());
			}
			queueObject.setNoOfTotalCounters(branchCounterRepository.countByBranch(queue.getBranch()));
			queueObject.setNoOfServingCounters(branchCounterServiceTABMapperRepository
					.findBranchCounterByBranchAndService(queue.getBranch(), queue.getService()).stream().distinct()
					.collect(Collectors.toList()).size());
			queueObject.setNoOfAgents(queueObject.getNoOfServingCounters());
			queueObject.setQueue(queue);
			queueListingObjects.add(queueObject);
		}
		return queueListingObjects;
	}
	
	@RequestMapping(value = "/getQueues/branchList", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getQueueListingByBranch(@RequestBody @Valid final List<Long> branchIdList)
			throws Exception, Throwable {
		List<Queue> queueList;
		if (branchIdList.isEmpty())
			queueList = new ArrayList<Queue>();
		else
			queueList = queueRepository.findByBranchIdList(branchIdList);
		return ResponseEntity.ok(queueList);
	}

}
