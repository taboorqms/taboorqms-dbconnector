package com.taboor.qms.db.connector.controller;

import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.UserManual;
import com.taboor.qms.core.utils.FileDataService;
import com.taboor.qms.db.connector.repository.UserManualRepository;

@RestController
@RequestMapping("/tab/usermanuals")
@CrossOrigin(origins = "*")
public class UserManualRepoController {

	@Autowired
	UserManualRepository userManualRepository;

	@Autowired
	private FileDataService fileDataService;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveUserManual(@RequestBody @Valid final UserManual userManual)
			throws Exception, Throwable {
		return ResponseEntity.ok(userManualRepository.save(userManual));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long userManualId)
			throws Exception, Throwable {

		Optional<UserManual> entity = userManualRepository.findById(userManualId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<UserManual> entity = userManualRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> delete(@RequestParam @Valid final Long userManualId)
			throws Exception, Throwable {

		UserManual userManual = userManualRepository.findById(userManualId).get();
		userManualRepository.delete(userManual);
		fileDataService.remove(Arrays.asList(new String(Base64.getDecoder().decode(userManual.getPath().getBytes()))));
		return ResponseEntity.ok(true);
	}

}
