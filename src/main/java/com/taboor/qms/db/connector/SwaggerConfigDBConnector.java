package com.taboor.qms.db.connector;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

@EnableSwagger2WebMvc
@Import(SpringDataRestConfiguration.class)
public class SwaggerConfigDBConnector {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.any()).build();
	}

	/*
	 * private ApiInfo apiEndPointsInfo() { return new
	 * ApiInfoBuilder().title("TaboorQMS Core").
	 * description("TaboorQMS Core Rest API's") .contact(new
	 * Contact("TaboorQMS Backend", "www.funavry.com",
	 * "hr@funavry.com")).license("Apache 2.0")
	 * .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html").version(
	 * "1.0.0").build(); }
	 */

}
