package com.taboor.qms.db.connector.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.BranchCounterAgentMapper;

@Repository
@Transactional
public interface BranchCounterAgentRepository extends JpaRepository<BranchCounterAgentMapper, Long> {

	@Query("Select count(b) from BranchCounterAgentMapper b where branch_counter_id = :counterId")
	int countByBranchCounter(@Param("counterId") long counterId);

}
