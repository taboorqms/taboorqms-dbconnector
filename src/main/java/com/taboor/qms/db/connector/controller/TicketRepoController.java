package com.taboor.qms.db.connector.controller;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.exception.TaboorQMSDaoException;
import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.Ticket;
import com.taboor.qms.core.payload.GetActivitiesPayload;
import com.taboor.qms.core.utils.TicketType;
import com.taboor.qms.db.connector.repository.TicketRepository;

@RestController
@RequestMapping("/tab/tickets")
@CrossOrigin(origins = "*")
public class TicketRepoController {

	@Autowired
	TicketRepository ticketRepository;

	@PersistenceContext
	EntityManager em;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveTicket(@RequestBody @Valid final Ticket ticket)
			throws Exception, Throwable {
		return ResponseEntity.ok(ticketRepository.save(ticket));
	}

	@RequestMapping(value = "/get/number", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getTicketNumber() throws Exception, Throwable {
		return ResponseEntity.ok(Long.parseLong(
				em.createNativeQuery("select nextval('ticket_number_seq_pk')").getResultList().get(0).toString()));
	}

	@RequestMapping(value = "/count/branch", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> countByBranch(@RequestBody Branch branch) throws Exception, Throwable {
		return ResponseEntity.ok(ticketRepository.countByBranch(branch));
	}

	@RequestMapping(value = "/count/branchList", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> countByBranchList(@RequestBody GetActivitiesPayload getActivitiesPayload)
			throws Exception, Throwable {
		if (getActivitiesPayload.getIdList().isEmpty())
			return ResponseEntity.ok(new Long[] { Long.valueOf(0), Long.valueOf(0) });

		Long ticketCount = ticketRepository.countByBranchList(getActivitiesPayload.getIdList(),
				getActivitiesPayload.getStartDate(), getActivitiesPayload.getEndDate());
		Long fastpassTicketCount = ticketRepository.countFastpassByBranchList(getActivitiesPayload.getIdList(),
				TicketType.FASTPASS.getStatus(), getActivitiesPayload.getStartDate(),
				getActivitiesPayload.getEndDate());
		return ResponseEntity.ok(new Long[] { ticketCount, fastpassTicketCount });
	}

	@RequestMapping(value = "/getMaxVisitedBranch/branchList", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getMaxVisitedBranchByBranchList(
			@RequestBody GetActivitiesPayload getActivitiesPayload) throws Exception, Throwable {
		if (getActivitiesPayload.getIdList().isEmpty())
			getActivitiesPayload.getIdList().add(Long.valueOf(0));
		Query query = em.createNativeQuery(
				"select branch_id,count(ticket_id) from tab_ticket where branch_id IN :branchIdList and DATE(created_time) between :startDate and :endDate group by branch_id");
		query.setParameter("branchIdList", getActivitiesPayload.getIdList());
		query.setParameter("startDate", getActivitiesPayload.getStartDate());
		query.setParameter("endDate", getActivitiesPayload.getEndDate());
		List<Object[]> entityList = query.getResultList();

		Map<Long, Long> map = new HashMap<Long, Long>();
		for (Object[] i : entityList)
			map.put(Long.valueOf(i[0].toString()), Long.valueOf(i[1].toString()));

		Optional<Entry<Long, Long>> mostVistitedBranch = map.entrySet().stream()
				.max(Comparator.comparing(Map.Entry::getValue));

		if (mostVistitedBranch.isPresent())
			return ResponseEntity
					.ok(Arrays.asList(mostVistitedBranch.get().getKey(), mostVistitedBranch.get().getValue()));
		return ResponseEntity.ok(Arrays.asList(getActivitiesPayload.getIdList().get(0), 0));
	}

	@RequestMapping(value = "/countDayAverage/branch", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> countDayAverageByBranch(@RequestBody Branch branch)
			throws Exception, Throwable {
		return ResponseEntity.ok(ticketRepository.countDayAverageByBranch(branch));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long ticketId)
			throws Exception, Throwable {

		Optional<Ticket> entity = ticketRepository.findById(ticketId);
		if (entity.isPresent()) {
//			entity.get().getBranch().setServiceCenter(null);
			return ResponseEntity.ok(entity.get());
		}
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/getTicket/number", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByTicketNumber(@RequestParam @Valid final String ticketNumber)
			throws Exception, Throwable {

		Optional<Ticket> entity = ticketRepository.findByTicketNumber(ticketNumber);
		if (entity.isPresent()) {
//			entity.get().getBranch().setServiceCenter(null);
			return ResponseEntity.ok(entity.get());
		}
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<Ticket> entity = ticketRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> deleteTicket(@RequestParam @Valid final long ticketId)
			throws Exception, Throwable {
		Integer response = Integer.valueOf(ticketRepository.deleteByTicketId(ticketId));
		return ResponseEntity.ok(response);
	}

	@RequestMapping(value = "/get/timeline", method = RequestMethod.GET, produces = "application/json")
	@Transactional
	public @ResponseBody ResponseEntity<?> getTicketTimelineById(@RequestParam @Valid final Long ticketId)
			throws Exception, Throwable {

		String queryBuilder = "INSERT INTO ticket_timeline(ticketID, createdTime, stepOut, stepIn, fastpassTime, agentCall, completionTime, totalTime)"
				+ "VALUES (" +

				"(Select ticket_id from tab_ticket where ticket_id = :ticketId)," +

				"(Select created_time from tab_ticket where ticket_id = :ticketId)," +

				"(Select date_part('hour',age(tso.step_out_time, tk.created_time)) * 60 + "
				+ " date_part('minute', age(tso.step_out_time, tk.created_time)) * 60 + "
				+ " date_part('second', age(tso.step_out_time, tk.created_time)) as total_time from tab_ticket tk "
				+ " inner join tab_ticket_step_out tso on (tk.ticket_id is null and tso.ticket_id is null) or tk.ticket_id = tso.ticket_id "
				+ " where tk.ticket_id = :ticketId LIMIT 1)," +

				"(Select date_part('hour',age(tso.step_in_time, tk.created_time)) * 60 + "
				+ " date_part('minute', age(tso.step_in_time, tk.created_time)) * 60 + "
				+ " date_part('second', age(tso.step_in_time, tk.created_time)) as total_time from tab_ticket tk "
				+ " inner join tab_ticket_step_out tso on (tk.ticket_id is null and tso.ticket_id is null) or tk.ticket_id = tso.ticket_id "
				+ " where tk.ticket_id = :ticketId LIMIT 1)," +

				"(Select date_part('hour',age(tfp.apply_time, tk.created_time)) * 60 + "
				+ " date_part('minute', age(tfp.apply_time, tk.created_time)) * 60 + "
				+ " date_part('second', age(tfp.apply_time, tk.created_time)) as total_time from tab_ticket tk "
				+ " inner join tab_ticket_fastpass tfp on (tk.ticket_id is null and tfp.ticket_id is null) or tk.ticket_id = tfp.ticket_id "
				+ " where tk.ticket_id = :ticketId LIMIT 1)," +

				"(Select date_part('hour', age(tk.agent_call_time, tk.created_time)) * 60 + "
				+ " date_part('minute', age(tk.agent_call_time, tk.created_time)) * 60 + "
				+ " date_part('second', age(tk.agent_call_time, tk.created_time)) as createTime from tab_ticket tk "
				+ " where date_part('hour',age(tk.agent_call_time, tk.created_time)) * 60 + "
				+ " date_part('minute',age(tk.agent_call_time, tk.created_time)) is not null and tk.ticket_id = :ticketId LIMIT 1) , "
				+

				"(Select completion_time from tab_ticket_served where ticket_id = :ticketId)," +

				"(Select date_part('hour',age(tks.completion_time, tk.created_time)) * 60 + "
				+ " date_part('minute', age(tks.completion_time, tk.created_time)) * 60 + "
				+ " date_part('second', age(tks.completion_time, tk.created_time)) as totaltime from tab_ticket tk "
				+ " inner join tab_ticket_served tks on (tk.ticket_id is null and tks.ticket_id is null) or tk.ticket_id = tks.ticket_id "
				+ " where tk.ticket_id = :ticketId LIMIT 1)" +

				")";

		Query query = em.createNativeQuery(queryBuilder);
		query.setParameter("ticketId", ticketId);

		int result = query.executeUpdate();

		if (result != 1) {
			throw new TaboorQMSDaoException("Record not inserted in ticket timeline table");
		}

		query = em.createNativeQuery("Select * from ticket_timeline tt where tt.ticketID = :ticketId");
		query.setParameter("ticketId", ticketId);
		List<Object[]> entity = query.getResultList();

		HashMap<String, Object> ticketTimeline = new HashMap<String, Object>();

		Object[] row = entity.get(0);
		ticketTimeline.put("TicketID", row[0] != null ? row[0] : "");
		ticketTimeline.put("CreatedTime", row[1] != null ? row[1] : "");
		ticketTimeline.put("StepOutTime", row[2] != null ? Math.ceil(Double.valueOf(row[2].toString()) / 60) : "");
		ticketTimeline.put("StepInTime", row[3] != null ? Math.ceil(Double.valueOf(row[3].toString()) / 60) : "");
		ticketTimeline.put("FastPassTime", row[4] != null ? Math.ceil(Double.valueOf(row[4].toString()) / 60) : "");
		ticketTimeline.put("AgentCallTime", row[5] != null ? Math.ceil(Double.valueOf(row[5].toString()) / 60) : "");
		ticketTimeline.put("CompletionTime", row[6] != null ? row[6] : "");
		ticketTimeline.put("TotalTime", row[7] != null ? Math.ceil(Double.valueOf(row[7].toString()) / 60) : "");

		return ResponseEntity.ok(ticketTimeline);
	}

	@RequestMapping(value = "/getTicket/tickerId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getTicketByBranchCounter(@RequestParam @Valid final Long branchId,
			@RequestParam @Valid final String counterNo) throws Exception, Throwable {

		Optional<Ticket> entity = ticketRepository.findTicketByCounterAndBranch(branchId, counterNo);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/getTicket/branchId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getTicketByBranch(@RequestParam @Valid final Long branchId)
			throws Exception, Throwable {

		List<Ticket> entity = ticketRepository.findTicketByBranch(branchId);
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}
        
        @RequestMapping(value = "/delete/branch", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteByBranchId(@RequestBody @Valid final Branch branch)
			throws Exception, Throwable {
		int response = Integer.valueOf(ticketRepository.deleteByBranch(branch));
		return ResponseEntity.ok(response);
	}
        
        @RequestMapping(value = "/get/stepOut", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> findTicketByStepOut()
			throws Exception, Throwable {
		List<Ticket> response = ticketRepository.findTicketByStepOut();
		return ResponseEntity.ok(response);
	}
}
