package com.taboor.qms.db.connector.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.BranchCounter;
import com.taboor.qms.core.response.GetBranchCounterListResponse.BranchCounterObject;
import com.taboor.qms.db.connector.repository.BranchCounterAgentMapperRepository;
import com.taboor.qms.db.connector.repository.BranchCounterRepository;
import com.taboor.qms.db.connector.repository.BranchCounterServiceTABMapperRepository;

@RestController
@RequestMapping("/tab/branchcounters")
@CrossOrigin(origins = "*")
public class BranchCounterRepoController {

	@Autowired
	BranchCounterRepository branchCounterRepository;

	@PersistenceContext
	EntityManager em;

	@Autowired
	BranchCounterAgentMapperRepository branchCounterAgentMapperRepository;

	@Autowired
	BranchCounterServiceTABMapperRepository branchCounterServiceTABRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveBranchCounter(@RequestBody @Valid final BranchCounter branchCounter)
			throws Exception, Throwable {
		return ResponseEntity.ok(branchCounterRepository.save(branchCounter));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long branchCounterId)
			throws Exception, Throwable {

		Optional<BranchCounter> entity = branchCounterRepository.findById(branchCounterId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<BranchCounter> entity = branchCounterRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/branchId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByBranchId(@RequestParam @Valid final Long branchId)
			throws Exception, Throwable {

		List<BranchCounter> entity = branchCounterRepository.findByBranchId(branchId);
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/agentIdAndBranch", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByAgentId(@RequestParam @Valid final Long agentId,
			@RequestParam @Valid final Long branchId) throws Exception, Throwable {

		Optional<BranchCounter> entity = branchCounterRepository.findByAgentIdAndBranchId(agentId, branchId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/getNextCounterNumber/branch", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getNextCounterNumberByBranch(@RequestParam @Valid final Long branchId)
			throws Exception, Throwable {
		Pageable getLastestAndSortedByCounterIdDesc = PageRequest.of(0, 1, Sort.by("counterId").descending());
		List<String> counterNumberList = branchCounterRepository.getNextCounterNumberByBranch(branchId,
				getLastestAndSortedByCounterIdDesc);
		String counterNumber;
		if (!counterNumberList.isEmpty())
			counterNumber = String.format("%03d", Integer.valueOf(counterNumberList.get(0)) + 1);
		else
			counterNumber = String.format("%03d", 1);

		return ResponseEntity.ok(counterNumber);
	}

	@RequestMapping(value = "/get/branch", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByBranch(@RequestParam @Valid final Long branchId)
			throws Exception, Throwable {

		List<BranchCounterObject> branchCounterObjects = new ArrayList<BranchCounterObject>();
		List<BranchCounter> counterList = branchCounterRepository.findByBranchId(branchId);
		if (counterList != null) {
			for (int i = 0; i < counterList.size(); i++) {
				BranchCounterObject branchCounterObject = new BranchCounterObject();
				BranchCounter entity = counterList.get(i);
				branchCounterObject.setBranchCounter(entity);
				branchCounterObject.setProvidedServices(
						branchCounterServiceTABRepository.getServiceByBranchCounter(entity.getCounterId()));
				if (branchCounterAgentMapperRepository.findByCounterId(entity.getCounterId()) != null)
					branchCounterObject.setAssignedToAgent(true);
				else
					branchCounterObject.setAssignedToAgent(false);
				branchCounterObjects.add(branchCounterObject);
			}
			branchCounterObjects.forEach(branchCounterObject -> branchCounterObject.getBranchCounter().setBranch(null));
		}
		return ResponseEntity.ok(branchCounterObjects);
	}

	@RequestMapping(value = "/get/counterNumber", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getNextCounterNumberByBranch(@RequestParam @Valid final String counterNumber)
			throws Exception, Throwable {
		Optional<BranchCounter> branchCounter = branchCounterRepository.findByCounterNumber(counterNumber);
		if (branchCounter.isPresent())
			return ResponseEntity.ok(branchCounter.get());
		return ResponseEntity.of(null);
	}

	@RequestMapping(value = "/delete/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteCounterById(@RequestParam @Valid final long branchCounterId)
			throws Exception, Throwable {
		int response = branchCounterRepository.deleteByCounterId(branchCounterId);
		if (response == 1)
			return ResponseEntity.ok(true);
		return ResponseEntity.ok(false);
	}

	@RequestMapping(value = "/delete/idList", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteAgents(@RequestBody @Valid final List<Long> counterIdList)
			throws Exception, Throwable {
		branchCounterRepository.deleteByIdList(counterIdList);
		return ResponseEntity.ok(true);
	}
}
