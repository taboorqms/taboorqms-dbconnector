package com.taboor.qms.db.connector.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.ServiceRequirement;
import com.taboor.qms.db.connector.repository.ServiceRequirementRepository;

@RestController
@RequestMapping("/tab/servicerequirements")
@CrossOrigin(origins = "*")
public class ServiceRequirementRepoController {

	@Autowired
	ServiceRequirementRepository serviceRequirementRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveRequiredDocument(
			@RequestBody @Valid final ServiceRequirement serviceRequirement) throws Exception, Throwable {
		return ResponseEntity.ok(serviceRequirementRepository.save(serviceRequirement));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long serviceRequirementId)
			throws Exception, Throwable {

		Optional<ServiceRequirement> entity = serviceRequirementRepository.findById(serviceRequirementId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}
	
	@RequestMapping(value = "/getName", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final String requirementName)
			throws Exception, Throwable {

		Optional<ServiceRequirement> entity = serviceRequirementRepository.findByServiceRequirementName(requirementName);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}


}
