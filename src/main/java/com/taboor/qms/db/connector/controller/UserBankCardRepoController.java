package com.taboor.qms.db.connector.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.UserBankCard;
import com.taboor.qms.db.connector.repository.UserBankCardRepository;

@RestController
@RequestMapping("/tab/userbankcards")
@CrossOrigin(origins = "*")
public class UserBankCardRepoController {

	@Autowired
	UserBankCardRepository userBankCardRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveUserBankCard(@RequestBody @Valid final UserBankCard userBankCard)
			throws Exception, Throwable {
		return ResponseEntity.ok(userBankCardRepository.save(userBankCard));
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteBranchBookmark(@RequestParam @Valid final Long userBankCardId)
			throws Exception, Throwable {
		return ResponseEntity.ok(userBankCardRepository.deleteByUserBankCardId(userBankCardId));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long userBankCardId)
			throws Exception, Throwable {

		Optional<UserBankCard> entity = userBankCardRepository.findById(userBankCardId);
		if (entity.isPresent()) {
			entity.get().setUser(null);
			return ResponseEntity.ok(entity.get());
		}
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/user", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByUserId(@RequestParam @Valid final Long userId)
			throws Exception, Throwable {

		List<UserBankCard> entity = userBankCardRepository.findByUserId(userId);
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		else
			entity.forEach((userbankCard) -> userbankCard.setUser(null));
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/userAndPaymentMethodId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByUserId(@RequestParam @Valid final Long userId,
			@RequestParam @Valid final Long paymentMethodId) throws Exception, Throwable {

		Optional<UserBankCard> entity = userBankCardRepository.findByUserIdAndPaymentMethodId(userId, paymentMethodId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}
	
	@RequestMapping(value = "/get/serviceCenter", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByServiceCenterId(@RequestParam @Valid final Long serviceCenterId)
			throws Exception, Throwable {

		List<UserBankCard> entity = userBankCardRepository.findByServiceCenterId(serviceCenterId);
		if (entity.isEmpty())
			return ResponseEntity.ok(entity);
		else
			entity.forEach((userbankCard) -> userbankCard.setUser(null));
		return ResponseEntity.ok(entity);
	}

}
