package com.taboor.qms.db.connector.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.TicketFeedback;
import com.taboor.qms.core.payload.GetActivitiesPayload;
import com.taboor.qms.core.response.GetStatisticsResponse.ValueObject;
import com.taboor.qms.core.utils.ServiceReviewStatus;
import com.taboor.qms.db.connector.repository.TicketFeedbackRepository;

@RestController
@RequestMapping("/tab/ticketfeedbacks")
@CrossOrigin(origins = "*")
public class TicketFeedbackRepoController {

	@Autowired
	TicketFeedbackRepository ticketFeedbackRepository;

	@PersistenceContext
	EntityManager em;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveTicketFeedback(@RequestBody @Valid final TicketFeedback ticketFeedback)
			throws Exception, Throwable {
		return ResponseEntity.ok(ticketFeedbackRepository.save(ticketFeedback));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long ticketFeedbackId)
			throws Exception, Throwable {

		Optional<TicketFeedback> entity = ticketFeedbackRepository.findById(ticketFeedbackId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<TicketFeedback> entity = ticketFeedbackRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/count/branchList", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> countByBranchList(@RequestBody GetActivitiesPayload getActivitiesPayload)
			throws Exception, Throwable {
		if (getActivitiesPayload.getIdList().isEmpty())
			return ResponseEntity.ok(new ArrayList<ValueObject>());
		Query query = em.createNativeQuery(
				"select tf.service_points,count(service_points) from tab_ticket_feedback tf inner join tab_ticket t on tf.ticket_id = t.ticket_id where t.branch_id IN :branchIdList and DATE(t.created_time) between :startDate and :endDate group by tf.service_points");
		query.setParameter("branchIdList", getActivitiesPayload.getIdList());
		query.setParameter("startDate", getActivitiesPayload.getStartDate());
		query.setParameter("endDate", getActivitiesPayload.getEndDate());
		List<Object[]> entityList = query.getResultList();
		List<ValueObject> response = new ArrayList<ValueObject>();
		Boolean one = false, two = false, three = false;
		for (Object[] i : entityList) {
			if (Double.valueOf(i[0].toString()).equals(Double.valueOf("1.0")))
				one = true;
			else if (Double.valueOf(i[0].toString()).equals(Double.valueOf("2.0")))
				two = true;
			else if (Double.valueOf(i[0].toString()).equals(Double.valueOf("3.0")))
				three = true;
			response.add(new ValueObject(Double.valueOf(i[0].toString()).toString(), String.valueOf(i[1])));
		}
		if (!one)
			response.add(new ValueObject("1.0", "0"));
		if (!two)
			response.add(new ValueObject("2.0", "0"));
		if (!three)
			response.add(new ValueObject("3.0", "0"));
		return ResponseEntity.ok(response);
	}

	@RequestMapping(value = "/countAvgRatings/agent", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> countAvgRatingByAgentServed(
			@RequestBody GetActivitiesPayload getActivitiesPayload) throws Exception, Throwable {
		Query query = em.createNativeQuery(
				"select avg(tf.service_points) from tab_ticket_feedback tf inner join tab_ticket_served ts on tf.ticket_id = ts.ticket_id where ts.served_agent_id = :agentId and DATE(ts.completion_time) between :startDate and :endDate");
		query.setParameter("agentId", getActivitiesPayload.getIdList().get(0));
		query.setParameter("startDate", getActivitiesPayload.getStartDate());
		query.setParameter("endDate", getActivitiesPayload.getEndDate());
		List<Double> entity = query.getResultList();
		if (entity.isEmpty())
			return ResponseEntity.ok(Double.valueOf("0.0"));
		else if (entity.get(0) == null)
			return ResponseEntity.ok(Double.valueOf("0.0"));
		return ResponseEntity.ok(entity.get(0));
	}

	@RequestMapping(value = "/countRatings/agent", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> countAgentRatings(@RequestBody GetActivitiesPayload getActivitiesPayload)
			throws Exception, Throwable {
		Query query = em.createNativeQuery(
				"select tf.service_points from tab_ticket_feedback tf inner join tab_ticket_served ts on tf.ticket_id = ts.ticket_id where ts.served_agent_id = :agentId and DATE(ts.completion_time) between :startDate and :endDate");
		query.setParameter("agentId", getActivitiesPayload.getIdList().get(0));
		query.setParameter("startDate", getActivitiesPayload.getStartDate());
		query.setParameter("endDate", getActivitiesPayload.getEndDate());
		List<Integer> entity = query.getResultList();
		int satisfied = 0, avergae = 0, poor = 0;
		if (entity != null)
			for (int item : entity) {
				if (item == ServiceReviewStatus.AVERAGE.getStatus())
					avergae++;
				else if (item == ServiceReviewStatus.SATISFIED.getStatus())
					satisfied++;
				else
					poor++;
			}

		return ResponseEntity.ok(Arrays.asList(new ValueObject("SATISFIED", String.valueOf(satisfied)),
				new ValueObject("AVERAGE", String.valueOf(avergae)), new ValueObject("POOR", String.valueOf(poor))));
	}

	@RequestMapping(value = "/getByTicket", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> getByBranchAndService(Long branchId) throws Exception, Throwable {

		List<TicketFeedback> entity = ticketFeedbackRepository.findByTicket(branchId);
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/countAvgServicePoints/serviceCenterId", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> countAvgServicePointsByServiceCenter(
			@RequestBody GetActivitiesPayload getActivitiesPayload) throws Exception, Throwable {
		Query query = em.createNativeQuery(
				"select avg(tf.service_points) from tab_ticket_feedback tf inner join tab_ticket t on tf.ticket_id = t.ticket_id inner join tab_branch b on b.branch_id = t.branch_id where b.service_center_id = :centerId and DATE(t.created_time) between :startDate and :endDate");
		query.setParameter("centerId", getActivitiesPayload.getIdList().get(0));
		query.setParameter("startDate", getActivitiesPayload.getStartDate());
		query.setParameter("endDate", getActivitiesPayload.getEndDate());
		List<Double> entity = query.getResultList();
		if (entity.isEmpty())
			return ResponseEntity.ok(Double.valueOf("0.0"));
		else if (entity.get(0) == null)
			return ResponseEntity.ok(Double.valueOf("0.0"));

		return ResponseEntity.ok(entity.get(0));
	}

	@RequestMapping(value = "/countRatings/serviceCenterId", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> countServiceCenterRatings(
			@RequestBody GetActivitiesPayload getActivitiesPayload) throws Exception, Throwable {
		Query query = em.createNativeQuery(
				"select tf.service_points from tab_ticket_feedback tf inner join tab_ticket t on tf.ticket_id = t.ticket_id inner join tab_branch b on b.branch_id = t.branch_id where b.service_center_id = :centerId and DATE(t.created_time) between :startDate and :endDate");
		query.setParameter("centerId", getActivitiesPayload.getIdList().get(0));
		query.setParameter("startDate", getActivitiesPayload.getStartDate());
		query.setParameter("endDate", getActivitiesPayload.getEndDate());
		List<Integer> entity = query.getResultList();
		int satisfied = 0, avergae = 0, poor = 0;
		if (entity != null)
			for (int item : entity) {
				if (item == ServiceReviewStatus.AVERAGE.getStatus())
					avergae++;
				else if (item == ServiceReviewStatus.SATISFIED.getStatus())
					satisfied++;
				else
					poor++;
			}

		return ResponseEntity.ok(Arrays.asList(new ValueObject("SATISFIED", String.valueOf(satisfied)),
				new ValueObject("AVERAGE", String.valueOf(avergae)), new ValueObject("POOR", String.valueOf(poor))));
	}
}
