package com.taboor.qms.db.connector.controller;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.BranchCounter;
import com.taboor.qms.core.model.BranchCounterServiceTABMapper;
import com.taboor.qms.db.connector.repository.BranchCounterServiceTABMapperRepository;

@RestController
@RequestMapping("/tab/branchcounterservicetabmapper")
@CrossOrigin(origins = "*")
public class BranchCounterServiceTABMapperRepoController {

	@Autowired
	BranchCounterServiceTABMapperRepository branchCounterServiceTABMapperRepository;

	@PersistenceContext
	EntityManager em;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveBranchCounterServiceTABMapper(
			@RequestBody @Valid final BranchCounterServiceTABMapper branchCounterServiceTABMapper)
			throws Exception, Throwable {
		return ResponseEntity.ok(branchCounterServiceTABMapperRepository.save(branchCounterServiceTABMapper));
	}

	@RequestMapping(value = "/saveAll", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveAll(
			@RequestBody @Valid final List<BranchCounterServiceTABMapper> branchCounterServiceTABMapperList)
			throws Exception, Throwable {
		return ResponseEntity.ok(branchCounterServiceTABMapperRepository.saveAll(branchCounterServiceTABMapperList));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long branchCounterId)
			throws Exception, Throwable {

		Optional<BranchCounterServiceTABMapper> entity = branchCounterServiceTABMapperRepository
				.findById(branchCounterId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<BranchCounterServiceTABMapper> entity = branchCounterServiceTABMapperRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/delete/branch", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteByBranchCounterId(@RequestParam @Valid final Long branchCounterId)
			throws Exception, Throwable {

		int entity = branchCounterServiceTABMapperRepository.deleteByBranchId(branchCounterId);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/branchCounter", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByBranchCounterId(@RequestParam @Valid final Long branchCounterId)
			throws Exception, Throwable {

		List<BranchCounterServiceTABMapper> entity = branchCounterServiceTABMapperRepository
				.findByBranchCounter(branchCounterId);
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/branchAndService", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByBranchAndService(@RequestParam @Valid final Long branchId,
			@RequestParam @Valid final Long serviceId) throws Exception, Throwable {

		List<BranchCounter> entity = branchCounterServiceTABMapperRepository.findTransferableBranchCounters(branchId,
				serviceId);
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

}
