package com.taboor.qms.db.connector.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.QueueTicketMapper;
import com.taboor.qms.core.model.Ticket;

@Repository
@Transactional
public interface QueueTicketMapperRepository extends JpaRepository<QueueTicketMapper, Long> {

	@Query("SELECT q FROM QueueTicketMapper q inner join Ticket t on t.ticketId = q.ticket inner join TicketUserMapper tum"
			+ " on q.ticket=tum.ticket inner join User u on u.userId=tum.user "
			+ "where u.userId = :userId and t.ticketStatus!=4 and t.ticketType!=4")
	List<QueueTicketMapper> findUserPendingTickets(@Param("userId") Long userId);

	@Query("SELECT qtm FROM QueueTicketMapper qtm inner join Queue q on qtm.queue=q.queueId inner join Branch b on"
			+ " q.branch=b.branchId inner join ServiceTAB s on q.service=s.serviceId where s.serviceId = :serviceId and b.branchId = :branchId")
	List<QueueTicketMapper> findByBranchAndService(@Param("branchId") Long branchId,
			@Param("serviceId") Long serviceId);

	@Query("SELECT qtm FROM QueueTicketMapper qtm inner join Queue q on qtm.queue=q.queueId inner join Branch b on"
			+ " q.branch=b.branchId inner join ServiceTAB s on q.service=s.serviceId inner join Ticket t on t.ticketId = qtm.ticket "
			+ "where s.serviceId = :serviceId and b.branchId = :branchId and t.ticketStatus!=4 and t.ticketType!=4")
	List<QueueTicketMapper> getByBranchAndService(@Param("branchId") Long branchId, @Param("serviceId") Long serviceId);

	@Query("SELECT q FROM QueueTicketMapper q inner join TicketUserMapper tum on q.ticket=tum.ticket inner join Ticket t on t.ticketId=tum.ticket where t.ticketId = :ticketId")
	Optional<QueueTicketMapper> findByTicketId(@Param("ticketId") Long ticketId);

	@Query("Select qtm from QueueTicketMapper qtm where queue_id = :queueId")
	List<QueueTicketMapper> findByQueueId(@Param("queueId") Long queueId);

	@Modifying
	@Query("DELETE from QueueTicketMapper qtm where qtm.queueTicketId = :queueTicketId")
	int deleteByQueueTicketId(@Param("queueTicketId") Long queueTicketId);

	@Modifying
	@Query("DELETE from QueueTicketMapper qtm where ticket_id = :ticketId")
	int deleteByTicketId(@Param("ticketId") Long ticketId);

	@Query("SELECT count(qtm.queueTicketId) FROM QueueTicketMapper qtm inner join Queue q on q.queueId=qtm.queue inner join"
			+ " Ticket t on t.ticketId=qtm.ticket where q.queueId=:queueId and t.ticketType=:fastpassTypeId")
	int countQueueFastpassTicketsByQueueId(@Param("queueId") long queueId, @Param("fastpassTypeId") int fastpassTypeId);

	@Query("select qtm.ticket from QueueTicketMapper qtm where queue_id = :queueId")
	List<Ticket> findTicketByQueue(@Param("queueId") Long queueId);

	@Query("SELECT count(qtm) FROM QueueTicketMapper qtm where queue_id =:queueId")
	int countByQueueId(@Param("queueId") long queueId);
}
