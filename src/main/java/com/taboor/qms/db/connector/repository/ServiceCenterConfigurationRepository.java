package com.taboor.qms.db.connector.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.ServiceCenterConfiguration;

@Repository
@Transactional
public interface ServiceCenterConfigurationRepository extends JpaRepository<ServiceCenterConfiguration, Long> {

	@Query("select scc from ServiceCenterConfiguration scc where service_center_id=:serviceCenterId")
	ServiceCenterConfiguration findByServiceCenterId(Long serviceCenterId);
	
	@Query("select scc from ServiceCenterConfiguration scc where service_center_id IS NULL")
	ServiceCenterConfiguration findDefaultConfiguration();

}
