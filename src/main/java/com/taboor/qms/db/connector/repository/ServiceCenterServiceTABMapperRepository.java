package com.taboor.qms.db.connector.repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.ServiceCenter;
import com.taboor.qms.core.model.ServiceCenterServiceTABMapper;
import com.taboor.qms.core.model.ServiceRequirement;

@Repository
@Transactional
public interface ServiceCenterServiceTABMapperRepository extends JpaRepository<ServiceCenterServiceTABMapper, Long> {

	@Query("select scstm.serviceRequirements from ServiceCenterServiceTABMapper scstm where servicecenter_id =:serviceCenterId and servicetab_id =:serviceTABId")
	Collection<ServiceRequirement> findRequirementsByServiceCenterIdAndServiceId(
			@Param("serviceCenterId") Long serviceCenterId, @Param("serviceTABId") Long serviceId);

	@Query("select scsm from ServiceCenterServiceTABMapper scsm where servicecenter_id=:serviceCenterId")
	List<ServiceCenterServiceTABMapper> findServicesByServiceCenter(@Param("serviceCenterId") Long serviceCenterId);

	List<ServiceCenterServiceTABMapper> findByServiceCenter(ServiceCenter serviceCenter);
	
	@Query("select scstm from ServiceCenterServiceTABMapper scstm where servicecenter_id =:serviceCenterId and servicetab_id =:serviceTABId")
	Optional<ServiceCenterServiceTABMapper> findByServiceCenterIdAndServiceId(
			@Param("serviceCenterId") Long serviceCenterId, @Param("serviceTABId") Long serviceId);
	
	@Modifying
	@Query("delete from ServiceCenterServiceTABMapper scsm where servicetab_id =:serviceId and servicecenter_id =:serviceCenterId")
	int deleteByServiceId(@Param("serviceId") Long serviceId, @Param("serviceCenterId") Long serviceCenterId);
	
	@Query("select count(scsm) from ServiceCenterServiceTABMapper scsm where servicecenter_id=:serviceCenterId")
	int countByServiceCenter(@Param("serviceCenterId") Long serviceCenterId);

}
