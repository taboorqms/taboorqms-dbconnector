package com.taboor.qms.db.connector.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.ServiceCenterEmpBranchMapper;
import com.taboor.qms.core.model.ServiceCenterEmployee;

@Repository
@Transactional
public interface ServiceCenterEmpBranchMapperRepository extends JpaRepository<ServiceCenterEmpBranchMapper, Long> {

	@Query("select u.branch from ServiceCenterEmpBranchMapper u where employee_id = :employee_id")
	List<Branch> findBranchByEmpId(@Param("employee_id") Long employee_id);

	@Query("select ebm.branch from ServiceCenterEmpBranchMapper ebm inner join ServiceCenterEmployee emp"
			+ " on ebm.serviceCenterEmployee = emp.employeeId where user_id = :userId")
	List<Branch> findBranchByUserId(@Param("userId") Long userId);

	@Query("select distinct(u.serviceCenterEmployee) from ServiceCenterEmpBranchMapper u where branch_id IN :branchIdList")
	List<ServiceCenterEmployee> findEmpByBranchIdList(@Param("branchIdList") List<Long> branchIdList);

	int countByServiceCenterEmployee(ServiceCenterEmployee employee);

	@Modifying
	void deleteByServiceCenterEmployee(ServiceCenterEmployee serviceCenterEmployee);

	@Query("select count(employee_id) from ServiceCenterEmpBranchMapper where branch_id = :branchId")
	int countEmpByBranchId(@Param("branchId") long branchId);

}
