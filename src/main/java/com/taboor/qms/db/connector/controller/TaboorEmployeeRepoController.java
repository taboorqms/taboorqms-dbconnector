package com.taboor.qms.db.connector.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.ServiceCenterEmployee;
import com.taboor.qms.core.model.TaboorEmployee;
import com.taboor.qms.core.response.GetTaboorEmployeeListResponse;
import com.taboor.qms.core.response.GetTaboorEmployeeListResponse.TaboorEmployeeObject;
import com.taboor.qms.db.connector.repository.TaboorEmployeeRepository;
import com.taboor.qms.db.connector.repository.UserPrivilegeMapperRepository;
import com.taboor.qms.db.connector.repository.UserPrivilegeRepository;
import com.taboor.qms.db.connector.repository.UserRoleMapperRepository;

@RestController
@RequestMapping("/tab/tabooremployees")
@CrossOrigin(origins = "*")
public class TaboorEmployeeRepoController {

	@Autowired
	TaboorEmployeeRepository taboorEmployeeRepository;

	@Autowired
	UserPrivilegeRepository userPrivilegeRepository;

	@Autowired
	UserPrivilegeMapperRepository userPrivilegeMapperRepository;

	@Autowired
	UserRoleMapperRepository userRoleMapperRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveTaboorEmployee(@RequestBody @Valid final TaboorEmployee taboorEmployee)
			throws Exception, Throwable {
		return ResponseEntity.ok(taboorEmployeeRepository.save(taboorEmployee));
	}

	@RequestMapping(value = "/get/userId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByUserId(@RequestParam @Valid final Long userId)
			throws Exception, Throwable {

		Optional<TaboorEmployee> entity = taboorEmployeeRepository.findByUserId(userId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<TaboorEmployee> entity = taboorEmployeeRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/userEmail", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByUserEmail(@RequestParam @Valid final String email)
			throws Exception, Throwable {

		Optional<TaboorEmployee> entity = taboorEmployeeRepository.findByUserEmail(email);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/count", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> countEmpByTaboor() throws Exception, Throwable {

		Long entity = taboorEmployeeRepository.count();
		return ResponseEntity.ok(entity.intValue());
	}

	@RequestMapping(value = "/getList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getList() throws Exception, Throwable {

		List<TaboorEmployeeObject> employeeList = new ArrayList<TaboorEmployeeObject>();

		List<TaboorEmployee> empList = taboorEmployeeRepository.findAll();

		for (TaboorEmployee emp : empList) {
			TaboorEmployeeObject object = new TaboorEmployeeObject();
			object.setEmployee(emp);
			object.setPrivileges(userPrivilegeMapperRepository.findPrivilgesByUserId(emp.getUser().getUserId()));
			object.setRoleId(userRoleMapperRepository.findByUser(emp.getUser()).getUserRole().getRoleId());
			employeeList.add(object);
		}
		return ResponseEntity.ok(new GetTaboorEmployeeListResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.USER_FETCHED.getMessage(), employeeList, userPrivilegeRepository.count()));
	}

	@RequestMapping(value = "/getNextEmpNumber", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getNextEmpNumber() throws Exception, Throwable {

		Pageable getLastestAndSortedByEmployeeIdDesc = PageRequest.of(0, 1, Sort.by("taboorEmpId").descending());
		List<String> entityList = taboorEmployeeRepository.getNextEmpNumber(getLastestAndSortedByEmployeeIdDesc);
		String entity;
		if (entityList == null)
			entity = "FirstEntry";
		else if (entityList.isEmpty())
			entity = "FirstEntry";
		else
			entity = entityList.get(0).substring(0, 3)
					+ String.format("%04d", Long.valueOf(entityList.get(0).substring(3)) + 1);
		return ResponseEntity.ok(entity);
	}
        
        @RequestMapping(value = "/delete", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteUser(@RequestParam @Valid final long userId)
			throws Exception, Throwable {
		TaboorEmployee scEmp = taboorEmployeeRepository.findById(userId).get();
		taboorEmployeeRepository.delete(scEmp);
		return ResponseEntity.ok(1);
	}
}
