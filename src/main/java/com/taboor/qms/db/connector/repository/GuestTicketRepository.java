package com.taboor.qms.db.connector.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.GuestTicket;

@Repository
@Transactional
public interface GuestTicketRepository extends JpaRepository<GuestTicket, Long> {
	
	@Query("select gt from GuestTicket gt where ticket_id = :ticketId")
	Optional<GuestTicket> findByTicket(@Param("ticketId") Long ticketId);
	
}
