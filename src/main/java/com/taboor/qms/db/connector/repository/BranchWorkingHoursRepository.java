package com.taboor.qms.db.connector.repository;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.BranchWorkingHours;

@Repository
@Transactional
public interface BranchWorkingHoursRepository extends JpaRepository<BranchWorkingHours, Long> {

	@Query("SELECT bwh FROM BranchWorkingHours bwh where branch_id = :branchId")
	List<BranchWorkingHours> findByBranchId(@Param("branchId") Long branchId);

	@Query("select b from BranchWorkingHours b where branch_id =:branchId and day =:workingDay")
	BranchWorkingHours findWorkingHoursByBranchIdAndDay(@Valid Long branchId, @Valid int workingDay);

	@Modifying
	@Query("delete from BranchWorkingHours where branch_id = :branchId")
	int deleteByBranch(@Param("branchId") Long branchId);

}
