package com.taboor.qms.db.connector.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.ServiceCenterEmployee;
import com.taboor.qms.core.model.TaboorEmployee;

@Repository
@Transactional
public interface TaboorEmployeeRepository extends JpaRepository<TaboorEmployee, Long> {

	@Query("SELECT te FROM TaboorEmployee te right join User u on te.user = u.userId where u.email = :userEmail")
	Optional<TaboorEmployee> findByUserEmail(@Param("userEmail") String userEmail);

	@Query("select te from TaboorEmployee te where user_id=:userId")
	Optional<TaboorEmployee> findByUserId(Long userId);

	List<ServiceCenterEmployee> findByEmployeeNumberStartsWith(String string,
			Pageable getLastestAndSortedByAgentIdDesc);

	@Query("select te.employeeNumber from TaboorEmployee te")
	List<String> getNextEmpNumber(Pageable getLastestAndSortedByEmployeeIdDesc);

}
