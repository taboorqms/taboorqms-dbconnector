package com.taboor.qms.db.connector.controller;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.BranchCounter;
import com.taboor.qms.core.model.BranchCounterAgentMapper;
import com.taboor.qms.db.connector.repository.BranchCounterAgentMapperRepository;

@RestController
@RequestMapping("/tab/branchcounteragent")
@CrossOrigin(origins = "*")
public class BranchCounterAgentMapperRepoController {

	@Autowired
	BranchCounterAgentMapperRepository branchCounterAgentMapperRepository;

	@PersistenceContext
	EntityManager em;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveBranchCounterAgent(
			@RequestBody @Valid final BranchCounterAgentMapper branchCounterAgentMapper) throws Exception, Throwable {
		return ResponseEntity.ok(branchCounterAgentMapperRepository.save(branchCounterAgentMapper));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long branchCounterAgentId)
			throws Exception, Throwable {

		Optional<BranchCounterAgentMapper> entity = branchCounterAgentMapperRepository.findById(branchCounterAgentId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<BranchCounterAgentMapper> entity = branchCounterAgentMapperRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/counter", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getBranchCounterByCustomerId(@RequestParam @Valid final Long counterId)
			throws Exception, Throwable {

		BranchCounterAgentMapper entity = branchCounterAgentMapperRepository.findByCounterId(counterId);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/getCounter/agent", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getBranchCounterByAgentId(@RequestParam @Valid final Long agentId)
			throws Exception, Throwable {

		BranchCounter entity = branchCounterAgentMapperRepository.findBranchCounterByAgentId(agentId);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/agent", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByAgentId(@RequestParam @Valid final Long agentId)
			throws Exception, Throwable {

		Optional<BranchCounterAgentMapper> entity = branchCounterAgentMapperRepository.findByAgentId(agentId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity);
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/delete/agent", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteByAgentId(@RequestParam @Valid final Long agentId)
			throws Exception, Throwable {

		branchCounterAgentMapperRepository.deleteByAgentId(agentId);
		return ResponseEntity.ok(true);
	}

}
