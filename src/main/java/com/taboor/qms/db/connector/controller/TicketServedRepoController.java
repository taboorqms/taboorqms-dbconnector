package com.taboor.qms.db.connector.controller;

import java.time.Duration;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.Agent;
import com.taboor.qms.core.model.Queue;
import com.taboor.qms.core.model.TicketServed;
import com.taboor.qms.db.connector.repository.AgentRepository;
import com.taboor.qms.db.connector.repository.BranchRepository;
import com.taboor.qms.db.connector.repository.QueueRepository;
import com.taboor.qms.db.connector.repository.TicketServedRepository;

@RestController
@RequestMapping("/tab/ticketserveds")
@CrossOrigin(origins = "*")
public class TicketServedRepoController {

	@Autowired
	TicketServedRepository ticketServedRepository;

	@Autowired
	QueueRepository queueRepository;

	@Autowired
	AgentRepository agentRepository;

	@Autowired
	BranchRepository branchRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveTicketServed(@RequestBody @Valid final TicketServed ticketServed)
			throws Exception, Throwable {
		return ResponseEntity.ok(ticketServedRepository.save(ticketServed));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long ticketServedId)
			throws Exception, Throwable {

		Optional<TicketServed> entity = ticketServedRepository.findById(ticketServedId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<TicketServed> entity = ticketServedRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/getCounterNumber/tickerId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getCounterNumberByTicketId(@RequestParam @Valid final Long ticketId)
			throws Exception, Throwable {

		Optional<String> entity = ticketServedRepository.findCounterNumberByTicketId(ticketId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/tickerId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByTicketId(@RequestParam @Valid final Long ticketId)
			throws Exception, Throwable {

		Optional<TicketServed> entity = ticketServedRepository.findByTicketId(ticketId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/ticket", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByTicket(@RequestParam @Valid final Long ticketId)
			throws Exception, Throwable {

		Optional<TicketServed> entity = ticketServedRepository.findByTicket(ticketId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/getAgent/ticket", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAgentByTicketId(@RequestParam @Valid final Long ticketId)
			throws Exception, Throwable {

		Optional<Agent> entity = ticketServedRepository.findServedAgentByTicketId(ticketId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/updateTimings", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> updateTimings(@RequestParam @Valid final Long ticketId)
			throws Exception, Throwable {
		
		Boolean update = false;
		Optional<TicketServed> ticketServedOptional = ticketServedRepository.findByTicketId(ticketId);
		if (ticketServedOptional.isPresent()) {
			TicketServed ticketServed = ticketServedOptional.get();

			if (ticketServed.getCompletionTime() != null & ticketServed.getAgentCallTime() != null) {
				int serviceTime = (int) Duration
						.between(ticketServed.getAgentCallTime(), ticketServed.getCompletionTime()).toMillis() / 1000;
				int waitingTime = (int) Duration
						.between(ticketServed.getTicket().getCreatedTime(), ticketServed.getAgentCallTime()).toMillis()
						/ 1000;

				if (ticketServed.getServedAgent() != null) {
					if (serviceTime > 0) {
						ticketServed.getServedAgent()
								.setAverageServiceTime(LocalTime.of(0, 0).plusSeconds(calculateAverage(
										(ticketServed.getServedAgent().getAverageServiceTime().getHour() * 60
												+ ticketServed.getServedAgent().getAverageServiceTime().getMinute())
												* 60
												+ ticketServed.getServedAgent().getAverageServiceTime().getSecond(),
										serviceTime, ticketServed.getServedAgent().getAverageServiceTimeCount())));
						ticketServed.getServedAgent().setAverageServiceTimeCount(
								ticketServed.getServedAgent().getAverageServiceTimeCount() + 1);
					}
					ticketServed.getServedAgent()
							.setTotalTicketServed(ticketServed.getServedAgent().getTotalTicketServed() + 1);
					agentRepository.save(ticketServed.getServedAgent());
				}
				if (ticketServed.getTicket().getBranch() != null & serviceTime > 0 & waitingTime > 0) {
					if (serviceTime > 0) {
						ticketServed.getTicket().getBranch()
								.setAverageServiceTime(LocalTime.of(0, 0).plusSeconds(calculateAverage(
										(ticketServed.getTicket().getBranch().getAverageServiceTime().getHour() * 60
												+ ticketServed.getTicket().getBranch().getAverageServiceTime()
														.getMinute())
												* 60
												+ ticketServed.getTicket().getBranch().getAverageServiceTime()
														.getSecond(),
										serviceTime,
										ticketServed.getTicket().getBranch().getAverageServiceTimeCount())));
						ticketServed.getTicket().getBranch().setAverageServiceTimeCount(
								ticketServed.getTicket().getBranch().getAverageServiceTimeCount() + 1);
						update = true;
					}
					if (waitingTime > 0) {
						ticketServed.getTicket().getBranch()
								.setAverageWaitingTime(LocalTime.of(0, 0).plusSeconds(calculateAverage(
										(ticketServed.getTicket().getBranch().getAverageWaitingTime().getHour() * 60
												+ ticketServed.getTicket().getBranch().getAverageWaitingTime()
														.getMinute())
												* 60
												+ ticketServed.getTicket().getBranch().getAverageWaitingTime()
														.getSecond(),
										waitingTime,
										ticketServed.getTicket().getBranch().getAverageWaitingTimeCount())));
						ticketServed.getTicket().getBranch().setAverageWaitingTimeCount(
								ticketServed.getTicket().getBranch().getAverageWaitingTimeCount() + 1);
						update = true;
					}
					if (update)
						branchRepository.save(ticketServed.getTicket().getBranch());
				}
				Queue queue = queueRepository
						.findByBranchAndServiceAndType(ticketServed.getTicket().getBranch().getBranchId(),
								ticketServed.getTicket().getService().getServiceId())
						.get();
				if (queue != null & serviceTime > 0 & waitingTime > 0) {
					update = false;
					if (serviceTime > 0) {
						queue.setAverageServiceTime(LocalTime.of(0, 0)
								.plusSeconds(calculateAverage(
										(queue.getAverageServiceTime().getHour() * 60
												+ queue.getAverageServiceTime().getMinute()) * 60
												+ queue.getAverageServiceTime().getSecond(),
										serviceTime, queue.getAverageServiceTimeCount())));
						queue.setAverageServiceTimeCount(queue.getAverageServiceTimeCount() + 1);
						update = true;
					}
					if (waitingTime > 0) {
						queue.setAverageWaitTime(LocalTime.of(0, 0).plusSeconds(calculateAverage(
								(queue.getAverageWaitTime().getHour() * 60 + queue.getAverageWaitTime().getMinute())
										* 60 + queue.getAverageWaitTime().getMinute(),
								waitingTime, queue.getAverageWaitTimeCount())));
						queue.setAverageWaitTimeCount(queue.getAverageWaitTimeCount() + 1);
						update = true;
					}
					if (update)
						queueRepository.save(queue);
				}
			}
		}
		return ResponseEntity.ok(true);
	}

	private long calculateAverage(long valueInSeconds, long newSeconds, long count) {
		return (valueInSeconds * count + newSeconds) / (count + 1);
	}
}
