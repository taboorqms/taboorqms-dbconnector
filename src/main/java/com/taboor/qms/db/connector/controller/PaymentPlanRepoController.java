package com.taboor.qms.db.connector.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.model.PaymentPlan;
import com.taboor.qms.core.model.PlanPriceObject;
import com.taboor.qms.core.model.ServiceCenterSubscription;
import com.taboor.qms.core.response.GetPaymentPlanListResponse.PaymentPlanObject;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.SubscriptionStatus;
import com.taboor.qms.db.connector.repository.PaymentPlanRepository;
import com.taboor.qms.db.connector.repository.ServiceCenterEmployeeRepository;
import com.taboor.qms.db.connector.repository.ServiceCenterRepository;
import com.taboor.qms.db.connector.repository.ServiceCenterSubscriptionRepository;
import com.taboor.qms.db.connector.repository.UserRepository;

@RestController
@RequestMapping("/tab/paymentplans")
@CrossOrigin(origins = "*")
public class PaymentPlanRepoController {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(PaymentPlanRepoController.class);

	@Autowired
	PaymentPlanRepository paymentPlanRepository;

	@Autowired
	ServiceCenterSubscriptionRepository serviceCenterSubscriptionRepository;

	@Autowired
	ServiceCenterRepository serviceCenterRepository;

	@Autowired
	ServiceCenterEmployeeRepository serviceCenterEmployeeRepository;

	@Autowired
	UserRepository userRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> savePaymentPlan(@RequestBody @Valid final PaymentPlan paymentPlan)
			throws Exception, Throwable {
		paymentPlan.setPricesCollection(GenericMapper.objectToJSONMapper(paymentPlan.getPrices()));
		return ResponseEntity.ok(paymentPlanRepository.save(paymentPlan));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long paymentPlanId)
			throws Exception, Throwable {

		Optional<PaymentPlan> entity = paymentPlanRepository.findById(paymentPlanId);
		if (entity.isPresent()) {
			entity.get().setPrices(
					GenericMapper.jsonToListObjectMapper(entity.get().getPricesCollection(), PlanPriceObject.class));
			return ResponseEntity.ok(entity.get());
		}
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<PaymentPlan> entityList = paymentPlanRepository.findActivePlans();
		if (entityList.isEmpty())
			return ResponseEntity.ok(null);
		List<PaymentPlanObject> response = new ArrayList<PaymentPlanObject>();
		for (PaymentPlan entity : entityList) {
			entity.setPrices(GenericMapper.jsonToListObjectMapper(entity.getPricesCollection(), PlanPriceObject.class));
			response.add(new PaymentPlanObject(entity, 0));
		}
		return ResponseEntity.ok(response);
	}

	@RequestMapping(value = "/getListing", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getLising() throws Exception, Throwable {

		List<PaymentPlan> entityList = paymentPlanRepository.findAll();
		if (entityList.isEmpty())
			return ResponseEntity.ok(null);
		List<PaymentPlanObject> response = new ArrayList<PaymentPlanObject>();
		for (PaymentPlan entity : entityList) {
			entity.setPrices(GenericMapper.jsonToListObjectMapper(entity.getPricesCollection(), PlanPriceObject.class));
			response.add(new PaymentPlanObject(entity, serviceCenterSubscriptionRepository
					.countSubscribersByPlanId(entity.getPaymentPlanId(), SubscriptionStatus.ACTIVE)));
		}
		return ResponseEntity.ok(response);
	}

	@RequestMapping(value = "/getDetails", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getDetails(@RequestParam Long paymentPlanId) throws Exception, Throwable {

		Optional<PaymentPlan> entity = paymentPlanRepository.findById(paymentPlanId);
		if (entity.isPresent()) {
			entity.get().setPrices(
					GenericMapper.jsonToListObjectMapper(entity.get().getPricesCollection(), PlanPriceObject.class));
			return ResponseEntity.ok(new PaymentPlanObject(entity.get(), 0));
		}
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/delete/id", method = RequestMethod.GET, produces = "application/json")
	@Transactional
	public @ResponseBody ResponseEntity<?> deleteById(@RequestParam @Valid final Long paymentPlanId)
			throws Exception, Throwable {

		Optional<PaymentPlan> entity = paymentPlanRepository.findById(paymentPlanId);
		if (entity.isPresent()) {
//			List<ServiceCenterSubscription> subscriptions = serviceCenterSubscriptionRepository
//					.findByPaymentPlan(entity.get());
//			List<Long> centerIds = new ArrayList<Long>();
//			if (!subscriptions.isEmpty())
//				for (ServiceCenterSubscription item : subscriptions) {
//					centerIds.add(item.getServiceCenter().getServiceCenterId());
//					if (item.getNoOfInvoices() > 0)
//						return ResponseEntity.ok(xyzResponseCode.ERROR.getCode());
//				}
			paymentPlanRepository.deleteById(entity.get().getPaymentPlanId());
//			for (Long id : centerIds) {
//				serviceCenterRepository.deleteById(id);
//				userRepository.deleteAll(serviceCenterEmployeeRepository.findUserByServiceCenterId(id));
//			}
			return ResponseEntity.ok(xyzResponseCode.SUCCESS.getCode());
		}
		return ResponseEntity.ok(null);
	}
}
