package com.taboor.qms.db.connector.repository;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.BranchCounter;

@Repository
@Transactional
public interface BranchCounterRepository extends JpaRepository<BranchCounter, Long> {

	@Query("select bc from BranchCounter bc where branch_id =:branchId")
	List<BranchCounter> findByBranchId(@Param("branchId") Long branchId);

	@Query("select bc from BranchCounter bc where agent_id =:agentId and branch_id =:branchId")
	Optional<BranchCounter> findByAgentIdAndBranchId(@Param("agentId") Long agentId, @Param("branchId") Long branchId);

	@Query("select bc.counterNumber from BranchCounter bc where branch_id =:branchId")
	List<String> getNextCounterNumberByBranch(@Param("branchId") Long branchId,
			Pageable getLastestAndSortedByCounterIdDesc);

	@Query("select bc from BranchCounter bc where branch_id =:branchId and bc.counterNumber = :counterNumber")
	Optional<BranchCounter> findByBranchAndCounterNumber(@Param("branchId") Long branchId,
			@Param("counterNumber") String counterNumber);

	Optional<BranchCounter> findByCounterNumber(@Valid String counterNumber);

	int countByBranch(Branch branch);

	@Modifying
	@Query("delete from BranchCounter bc where bc.counterId =:branchCounterId")
	int deleteByCounterId(@Param("branchCounterId") long branchCounterId);

	@Modifying
	@Query("delete from BranchCounter where counterId IN :counterIdList")
	void deleteByIdList(@Valid List<Long> counterIdList);

}
