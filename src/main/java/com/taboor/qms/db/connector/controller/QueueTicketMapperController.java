package com.taboor.qms.db.connector.controller;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.QueueTicketMapper;
import com.taboor.qms.core.model.Ticket;
import com.taboor.qms.core.payload.GetActivitiesPayload;
import com.taboor.qms.core.utils.TicketStatus;
import com.taboor.qms.core.utils.TicketType;
import com.taboor.qms.db.connector.repository.QueueTicketMapperRepository;

@RestController
@RequestMapping("/tab/queueticketmapper")
@CrossOrigin(origins = "*")
public class QueueTicketMapperController {

	@Autowired
	QueueTicketMapperRepository queueTicketMapperRepository;

	@PersistenceContext
	EntityManager em;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveQueueTicketMapper(
			@RequestBody @Valid final QueueTicketMapper queueTicketMapper) throws Exception, Throwable {
		return ResponseEntity.ok(queueTicketMapperRepository.save(queueTicketMapper));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long queueTicketMapperId)
			throws Exception, Throwable {

		Optional<QueueTicketMapper> entity = queueTicketMapperRepository.findById(queueTicketMapperId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<QueueTicketMapper> entity = queueTicketMapperRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/getTickets/pending/user", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getUserPendingTickets(@RequestParam @Valid final Long userId)
			throws Exception, Throwable {

		List<Ticket> response = new ArrayList<Ticket>();
		List<QueueTicketMapper> entity = queueTicketMapperRepository.findUserPendingTickets(userId);
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		for (int i = 0; i < entity.size(); i++) {
			Ticket ticket = entity.get(i).getTicket();
			ticket.getBranch().setServiceCenter(null);
			response.add(ticket);
		}
		return ResponseEntity.ok(response);
	}

	@RequestMapping(value = "/get/branchAndService", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> findQueueTickets(@RequestParam @Valid final Long branchId,
			@RequestParam @Valid final Long serviceId) throws Exception, Throwable {

		List<QueueTicketMapper> entity = queueTicketMapperRepository.getByBranchAndService(branchId, serviceId);
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/getTickets/branchAndService", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getQueueTickets(@RequestParam @Valid final Long branchId,
			@RequestParam @Valid final Long serviceId) throws Exception, Throwable {

		List<QueueTicketMapper> entity = queueTicketMapperRepository.findByBranchAndService(branchId, serviceId);
		TicketServedRepoController ticketServedRepoController = new TicketServedRepoController();
		Ticket ticket;
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		int j = 1;
		for (int i = 0; i < entity.size(); i++) {
			ticket = entity.get(i).getTicket();
			if (ticket.getTicketStatus() == TicketStatus.SERVING.getStatus())
				ticket.setCounterNumber(
						String.valueOf(ticketServedRepoController.getCounterNumberByTicketId(ticket.getTicketId())));
			else if (ticket.getTicketStatus() == TicketStatus.WAITING.getStatus())
				ticket.setWaitingTime(LocalTime.of(0, 0, 0)
						.plusSeconds(entity.get(i).getQueue().getAverageServiceTime().toSecondOfDay() * (j++)));
//			ticket.setBranch(null);
//			ticket.setService(null);
//			entity.get(i).getQueue().setBranch(null);
//			entity.get(i).getQueue().setService(null);
		}
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/ticketId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByTicketId(@RequestParam @Valid final Long ticketId)
			throws Exception, Throwable {

		Optional<QueueTicketMapper> entity = queueTicketMapperRepository.findByTicketId(ticketId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}
	
	@RequestMapping(value = "/get/queueId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByQueueId(@RequestParam @Valid final Long queueId)
			throws Exception, Throwable {

		List<QueueTicketMapper> entity = queueTicketMapperRepository.findByQueueId(queueId);
		if (entity != null)
			return ResponseEntity.ok(entity);
		return ResponseEntity.ok(null);
	}
	
	@RequestMapping(value = "/get/all/ticket", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAllByTicketId(@RequestParam @Valid final Long ticketId)
			throws Exception, Throwable {

		Optional<QueueTicketMapper> entity = queueTicketMapperRepository.findByTicketId(ticketId);
		if (entity.isPresent())
			return ResponseEntity.ok(getByQueueId(entity.get().getQueue().getQueueId()));
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteQueueTicketMapper(@RequestParam @Valid final long queueTicketMapperId)
			throws Exception, Throwable {
		return ResponseEntity.ok(queueTicketMapperRepository.deleteByQueueTicketId(queueTicketMapperId));
	}

	@RequestMapping(value = "/delete/ticketId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteTicketFromQueue(@RequestParam @Valid final Long ticketId)
			throws Exception, Throwable {
		return ResponseEntity.ok(queueTicketMapperRepository.deleteByTicketId(ticketId));
	}

	@RequestMapping(value = "/queue/countFastpass", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> countQueueFastpassTickets(@RequestParam @Valid final long queueId)
			throws Exception, Throwable {
		return ResponseEntity.ok(queueTicketMapperRepository.countQueueFastpassTicketsByQueueId(queueId,
				TicketType.FASTPASS.getStatus()));
	}

	@RequestMapping(value = "/count/queue", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> countQueueTickets(@RequestParam @Valid final long queueId)
			throws Exception, Throwable {
		return ResponseEntity.ok(queueTicketMapperRepository.countByQueueId(queueId));
	}

	@RequestMapping(value = "/getHighestDemandService/branch", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getHighestDemandService(
			@RequestBody GetActivitiesPayload getActivitiesPayload) throws Exception, Throwable {
		if (getActivitiesPayload.getIdList().isEmpty())
			getActivitiesPayload.getIdList().add(Long.valueOf(0));
		Query query = em.createNativeQuery(
				"select service_id,count(ticket_id) from tab_ticket where branch_id =:branchId and service_id IN :serviceIdList and DATE(created_time) between :startDate and :endDate group by branch_id,service_id");
		query.setParameter("branchId", getActivitiesPayload.getIdList().get(0));
		getActivitiesPayload.getIdList().remove(0);
		query.setParameter("serviceIdList", getActivitiesPayload.getIdList());
		query.setParameter("startDate", getActivitiesPayload.getStartDate());
		query.setParameter("endDate", getActivitiesPayload.getEndDate());
		List<Object[]> entityList = query.getResultList();

		Map<Long, Long> map = new HashMap<Long, Long>();
		for (Object[] i : entityList)
			map.put(Long.valueOf(i[0].toString()), Long.valueOf(i[1].toString()));

		Optional<Entry<Long, Long>> highestDemandService = map.entrySet().stream()
				.max(Comparator.comparing(Map.Entry::getValue));

		if (highestDemandService.isPresent())
			return ResponseEntity
					.ok(Arrays.asList(highestDemandService.get().getKey(), highestDemandService.get().getValue()));
		return ResponseEntity.ok(Arrays.asList(getActivitiesPayload.getIdList().get(0), 0));
	}
}
