package com.taboor.qms.db.connector.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.UserNotificationType;

@Repository
@Transactional
public interface UserNotificationTypeRepository extends JpaRepository<UserNotificationType, Long> {

	@Query("select unt from UserNotificationType unt where user_id =:userId")
	Optional<UserNotificationType> findByUserId(@Param("userId") Long userId);

}
