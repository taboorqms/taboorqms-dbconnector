package com.taboor.qms.db.connector.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.UserFAQType;
import com.taboor.qms.db.connector.repository.UserFAQTypeRepository;

@RestController
@RequestMapping("/tab/userfaqtypes")
@CrossOrigin(origins = "*")
public class UserFAQTypeRepoController {

	@Autowired
	UserFAQTypeRepository userFAQTypeRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveUserFeedback(@RequestBody @Valid final UserFAQType userFAQType)
			throws Exception, Throwable {
		return ResponseEntity.ok(userFAQTypeRepository.save(userFAQType));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long userFAQTypeId)
			throws Exception, Throwable {

		Optional<UserFAQType> entity = userFAQTypeRepository.findById(userFAQTypeId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<UserFAQType> entity = userFAQTypeRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}
}
