package com.taboor.qms.db.connector.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.ServiceCenter;
import com.taboor.qms.core.model.ServiceCenterSubscription;
import com.taboor.qms.core.response.GetServiceCentreListingResponse.ServiceCenterObject;
import com.taboor.qms.core.utils.SubscriptionStatus;
import com.taboor.qms.core.utils.SupportTicketStatus;
import com.taboor.qms.db.connector.repository.BranchRepository;
import com.taboor.qms.db.connector.repository.ServiceCenterEmployeeRepository;
import com.taboor.qms.db.connector.repository.ServiceCenterRepository;
import com.taboor.qms.db.connector.repository.ServiceCenterSubscriptionRepository;
import com.taboor.qms.db.connector.repository.SupportTicketRepository;

@RestController
@RequestMapping("/tab/servicecenters")
@CrossOrigin(origins = "*")
public class ServiceCenterRepoController {

	@Autowired
	ServiceCenterRepository serviceCenterRepository;

	@Autowired
	BranchRepository branchRepository;

	@Autowired
	SupportTicketRepository supportTicketRepository;

	@Autowired
	ServiceCenterSubscriptionRepository serviceCenterSubscriptionRepository;

	@Autowired
	ServiceCenterEmployeeRepository serviceCenterEmployeeRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveServiceCenter(@RequestBody @Valid final ServiceCenter serviceCenter)
			throws Exception, Throwable {
		return ResponseEntity.ok(serviceCenterRepository.save(serviceCenter));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long serviceCenterId)
			throws Exception, Throwable {

		Optional<ServiceCenter> entity = serviceCenterRepository.findById(serviceCenterId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<ServiceCenter> entity = serviceCenterRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/count", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> countAll() throws Exception, Throwable {
		Long entity = serviceCenterRepository.count();
		return ResponseEntity.ok(entity.intValue());
	}

	@RequestMapping(value = "/get/userId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByUserId(@RequestParam @Valid final Long userId)
			throws Exception, Throwable {

		Optional<ServiceCenter> entity = serviceCenterRepository.findByUserId(userId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/getListing", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getListing() throws Exception, Throwable {

		List<ServiceCenter> entityList = serviceCenterRepository.findAll();
		List<ServiceCenterObject> objectList = new ArrayList<ServiceCenterObject>();
		for (ServiceCenter center : entityList) {
			ServiceCenterObject object = new ServiceCenterObject();
			object.setBranchCount(branchRepository.countByServiceCenter(center.getServiceCenterId()));
			object.setIssueCount(supportTicketRepository.openCountByServiceCenter(center, SupportTicketStatus.Closed));
			Optional<ServiceCenterSubscription> subscription = serviceCenterSubscriptionRepository
					.findActiveSubscriptionByServiceCenterId(center.getServiceCenterId(), SubscriptionStatus.ACTIVE);

			if (subscription.isPresent()) {
				object.setPlanName(subscription.get().getPaymentPlan().getPlanName());
				object.setPlanNameArabic(subscription.get().getPaymentPlan().getPlanNameArabic());
				object.setNextPaymentDueDate(subscription.get().getNextPaymentDueDate());
			}
			center.setLogoDataString(new String(center.getLogoData()));
			object.setServiceCenter(center);
			object.setUserCount(serviceCenterEmployeeRepository.countEmpByServiceCenter(center.getServiceCenterId()));
			objectList.add(object);
		}
		return ResponseEntity.ok(objectList);
	}

	@RequestMapping(value = "/delete/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteCounterById(@RequestParam @Valid final long serviceCentreId)
			throws Exception, Throwable {
		if (serviceCenterRepository.deleteByServiceCenterId(serviceCentreId) > 0)
			return ResponseEntity.ok(true);
		return ResponseEntity.ok(false);
	}

}
