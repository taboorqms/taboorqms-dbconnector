package com.taboor.qms.db.connector.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.Queue;
import com.taboor.qms.core.model.ServiceTAB;

@Repository
@Transactional
public interface QueueRepository extends JpaRepository<Queue, Long> {

	@Query("SELECT q FROM Queue q where branch_id = :branchId and service_id = :serviceId")
	Optional<Queue> findByBranchAndServiceAndType(@Param("branchId") Long branchId, @Param("serviceId") Long serviceId);

	@Query("SELECT q FROM Queue q where branch_id IN :branchIdList")
	List<Queue> findByBranchIdList(@Param("branchIdList") List<Long> branchIdList);

	@Query("SELECT q FROM Queue q where branch_id = :branchIdList")
	List<Queue> findByBranch(@Param("branchIdList") Long branchId);

	@Query("SELECT count(q) FROM Queue q where branch_id IN :branchIdList")
	int countByBranchIdList(@Param("branchIdList") List<Long> branchIdList);

	@Modifying
	void deleteByBranchAndService(Branch branch, ServiceTAB serviceTAB);

}
