package com.taboor.qms.db.connector.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.User;
import com.taboor.qms.core.model.UserRoleMapper;
import com.taboor.qms.db.connector.repository.UserRoleMapperRepository;

@RestController
@RequestMapping("/tab/userrolemapper")
@CrossOrigin(origins = "*")
public class UserRoleMapperController {

	@Autowired
	UserRoleMapperRepository userRoleMapperRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveUserRoleMapper(@RequestBody @Valid final UserRoleMapper userRoleMapper)
			throws Exception, Throwable {
		return ResponseEntity.ok(userRoleMapperRepository.save(userRoleMapper));
	}

	@RequestMapping(value = "/get/user", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByUser(@RequestParam @Valid final Long userId)
			throws Exception, Throwable {
		User user = new User();
		user.setUserId(userId);
		return ResponseEntity.ok(userRoleMapperRepository.findByUser(user));
	}
}
