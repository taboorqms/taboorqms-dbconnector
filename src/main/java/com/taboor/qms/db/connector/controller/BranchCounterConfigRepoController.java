package com.taboor.qms.db.connector.controller;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.BranchCounterConfig;
import com.taboor.qms.db.connector.repository.BranchCounterConfigRepository;

@RestController
@RequestMapping("/tab/branchcounterconfigs")
@CrossOrigin(origins = "*")
public class BranchCounterConfigRepoController {

	@Autowired
	BranchCounterConfigRepository branchCounterConfigRepository;

	@PersistenceContext
	EntityManager em;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveBranchCounter(
			@RequestBody @Valid final BranchCounterConfig branchCounterConfig) throws Exception, Throwable {
		return ResponseEntity.ok(branchCounterConfigRepository.save(branchCounterConfig));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long branchCounterConfigId)
			throws Exception, Throwable {

		Optional<BranchCounterConfig> entity = branchCounterConfigRepository.findById(branchCounterConfigId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<BranchCounterConfig> entity = branchCounterConfigRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/counter", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteCounterById(@RequestParam @Valid final Long branchCounterId)
			throws Exception, Throwable {
		Optional<BranchCounterConfig> entity = branchCounterConfigRepository.getByCounter(branchCounterId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/agentId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getCounterConfigByAgentId(@RequestParam @Valid final Long agentId)
			throws Exception, Throwable {
		Optional<BranchCounterConfig> entity = branchCounterConfigRepository.findByAgentId(agentId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/delete/deviceToken", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteByDeviceToken(@RequestParam @Valid final String deviceToken)
			throws Exception, Throwable {
		int entity = branchCounterConfigRepository.deleteByDeviceToken(deviceToken);
		return ResponseEntity.ok(true);
	}
}
