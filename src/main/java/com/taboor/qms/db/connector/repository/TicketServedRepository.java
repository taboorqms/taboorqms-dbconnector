package com.taboor.qms.db.connector.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.Agent;
import com.taboor.qms.core.model.TicketServed;

@Repository
@Transactional
public interface TicketServedRepository extends JpaRepository<TicketServed, Long> {

	@Query("select bc.counterNumber from TicketServed ts inner join BranchCounter bc"
			+ " on bc.counterId=ts.branchCounter inner join Ticket t on t.ticketId=ts.ticket where t.ticketId = :ticketId")
	Optional<String> findCounterNumberByTicketId(@Param("ticketId") Long ticketId);

	@Query("select ts from TicketServed ts where ticket_id = :ticketId")
	Optional<TicketServed> findByTicketId(@Param("ticketId") Long ticketId);

	@Query("select ts from TicketServed ts where ticket_id = :ticketId and branch_counter_id != null")
	Optional<TicketServed> findByTicket(@Param("ticketId") Long ticketId);
	
	@Query("select ts.servedAgent from TicketServed ts where ticket_id = :ticketId")
	Optional<Agent> findServedAgentByTicketId(@Param("ticketId") Long ticketId);

}
