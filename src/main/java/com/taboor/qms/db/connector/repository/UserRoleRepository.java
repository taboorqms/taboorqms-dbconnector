package com.taboor.qms.db.connector.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.UserRole;

@Repository
@Transactional
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

	public Optional<UserRole> findByRoleName(String roleName);

	@Query("Select ur from UserRole ur where roleName not like 'Taboor%' and roleName != :customerRoleName")
	public List<UserRole> findByRoleNameNotStartsWith(String customerRoleName);

}
