package com.taboor.qms.db.connector.controller;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.BranchWorkingHours;
import com.taboor.qms.db.connector.repository.BranchWorkingHoursRepository;

@RestController
@RequestMapping("/tab/branchworkinghours")
@CrossOrigin(origins = "*")
public class BranchWorkingHoursRepoController {

	@Autowired
	BranchWorkingHoursRepository branchWorkingHoursRepository;

	@PersistenceContext
	EntityManager em;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveBranchWorkingHours(
			@RequestBody @Valid final BranchWorkingHours branchWorkingHours) throws Exception, Throwable {
		return ResponseEntity.ok(branchWorkingHoursRepository.save(branchWorkingHours));
	}

	@RequestMapping(value = "/saveAll", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveAll(
			@RequestBody @Valid final List<BranchWorkingHours> branchWorkingHours) throws Exception, Throwable {
		return ResponseEntity.ok(branchWorkingHoursRepository.saveAll(branchWorkingHours));
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<BranchWorkingHours> entity = branchWorkingHoursRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long branchWorkingHoursId)
			throws Exception, Throwable {

		Optional<BranchWorkingHours> entity = branchWorkingHoursRepository.findById(branchWorkingHoursId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/branch", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByBranchId(@RequestParam @Valid final Long branchId)
			throws Exception, Throwable {

		List<BranchWorkingHours> entity = branchWorkingHoursRepository.findByBranchId(branchId);
		entity.forEach((branchWorkingHours) -> branchWorkingHours.setBranch(null));
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/branch/day", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getBranchWorkingHoursByDay(@RequestParam @Valid final Long branchId,
			@RequestParam @Valid final int workingDay) throws Exception, Throwable {
		BranchWorkingHours entity = branchWorkingHoursRepository.findWorkingHoursByBranchIdAndDay(branchId, workingDay);
		if (entity == null)
			return ResponseEntity.ok(null);

		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/delete/branch", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteByBranch(@RequestParam @Valid final Long branchId)
			throws Exception, Throwable {
		int entity = branchWorkingHoursRepository.deleteByBranch(branchId);
		return ResponseEntity.ok(true);
	}

}
