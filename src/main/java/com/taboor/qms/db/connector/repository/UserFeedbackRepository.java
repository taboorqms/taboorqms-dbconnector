package com.taboor.qms.db.connector.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.UserFeedback;

@Repository
@Transactional
public interface UserFeedbackRepository extends JpaRepository<UserFeedback, Long> {

}
