package com.taboor.qms.db.connector.repository;

import java.time.LocalDate;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.AgentTicketCount;

@Repository
@Transactional
public interface AgentTicketCountRepository extends JpaRepository<AgentTicketCount, Long> {

	@Query("select atc from AgentTicketCount atc where atc.servingDate = :dateServing and user_id = :userId")
	Optional<AgentTicketCount> findByDateOfServing(@Valid LocalDate dateServing, @Valid Long userId);
	
	@Query("select atc from AgentTicketCount atc where atc.servingDate = :dateServing and counter_id = :counterId")
	Optional<AgentTicketCount> findByServingCounter(@Valid LocalDate dateServing, @Valid Long counterId);
	
	@Query("select atc from AgentTicketCount atc where atc.servingDate = :dateServing and counter_id = :counterId and user_id = :userId")
	Optional<AgentTicketCount> findByServingCounterAndUser(@Valid LocalDate dateServing, @Valid Long counterId,
			@Valid Long userId);
}
