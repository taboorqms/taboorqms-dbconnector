package com.taboor.qms.db.connector.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.UserVerification;

@Repository
@Transactional
public interface UserVerificationRepository extends JpaRepository<UserVerification, Long> {

	@Query("Select uv from UserVerification uv inner join User u on uv.user=u.userId where u.email = :email"
			+ " and uv.verificationMode = :mode and uv.verificationCode = :code and uv.verificationStatus = 701")
	Optional<UserVerification> findByUserEmailAndModeAndCode(@Param("email") String email,
			@Param("mode") int mode, @Param("code") String verificationCode);
	@Query("Select uv from UserVerification uv inner join User u on uv.user=u.userId where u.phoneNumber = :email"
			+ " and uv.verificationMode = :mode and uv.verificationCode = :code and uv.verificationStatus = 701")
	Optional<UserVerification> findByUserPhoneAndModeAndCode(@Param("email") String email,
			@Param("mode") int mode, @Param("code") String verificationCode);
	@Modifying
	@Query("DELETE from UserVerification where userId = :user_id")
	int deleteByUserId(@Param("user_id") Long userId);
	
	@Query("Select uv from UserVerification uv where user_id = :user_id and uv.verificationMode = 1 "
			+ "and uv.verificationStatus = 700 and uv.verificationMedium = :email")
	Optional<UserVerification> findEmailVerificationByUser(@Param("user_id") Long user_id, @Param("email") String email);
}
