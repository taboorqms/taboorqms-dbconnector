package com.taboor.qms.db.connector.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.Ticket;
import com.taboor.qms.core.model.TicketStepOut;

@Repository
@Transactional
public interface TicketStepOutRepository extends JpaRepository<TicketStepOut, Long> {

	@Query("Select st from TicketStepOut st where ticket_id=:ticketId")
	Optional<TicketStepOut> findByTicket(@Param("ticketId") Long ticketId);
	
	Boolean existsByTicket(Ticket ticket);

}
