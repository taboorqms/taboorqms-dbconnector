package com.taboor.qms.db.connector.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.UserPrivilege;
import com.taboor.qms.db.connector.repository.UserPrivilegeRepository;

@RestController
@RequestMapping("/tab/userprivileges")
@CrossOrigin(origins = "*")
public class UserPrivilegeRepoController {

	@Autowired
	UserPrivilegeRepository userPrivilegeRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveUserPrivilege(@RequestBody @Valid final UserPrivilege userPrivilege)
			throws Exception, Throwable {
		return ResponseEntity.ok(userPrivilegeRepository.save(userPrivilege));
	}

	@RequestMapping(value = "/saveAll", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveAll(@RequestBody @Valid final List<UserPrivilege> userPrivileges)
			throws Exception, Throwable {
		userPrivilegeRepository.saveAll(userPrivileges);
		return ResponseEntity.ok(true);
	}

	@RequestMapping(value = "/count", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> count() throws Exception, Throwable {
		return ResponseEntity.ok(userPrivilegeRepository.count());
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {
		return ResponseEntity.ok(userPrivilegeRepository.findAll());
	}

}
