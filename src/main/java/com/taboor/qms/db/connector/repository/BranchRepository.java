package com.taboor.qms.db.connector.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.Branch;

@Repository
@Transactional
public interface BranchRepository extends JpaRepository<Branch, Long> {

	@Query("Select b from Branch b where service_center_id = :serviceCentreId")
	List<Branch> findByServiceCenter(@Param("serviceCentreId") Long serviceCentreId);

	@Query("Select count(b) from Branch b where service_center_id = :serviceCentreId")
	int countByServiceCenter(@Param("serviceCentreId") Long serviceCenterId);

	@Query("Select distinct count(b.city) from Branch b where service_center_id = :serviceCentreId")
	int countCityByServiceCenter(@Param("serviceCentreId") Long serviceCenterId);

	@Modifying
	@Query("DELETE from Branch where branchId = :branchId")
	int deleteByBranchId(@Param("branchId") Long branchId);
}
