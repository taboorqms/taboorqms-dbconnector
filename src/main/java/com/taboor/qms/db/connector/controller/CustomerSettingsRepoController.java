package com.taboor.qms.db.connector.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.Customer;
import com.taboor.qms.core.model.CustomerSettings;
import com.taboor.qms.db.connector.repository.CustomerRepository;
import com.taboor.qms.db.connector.repository.CustomerSettingsRepository;

@RestController
@RequestMapping("/tab/customersettings")
@CrossOrigin(origins = "*")
public class CustomerSettingsRepoController {

	@Autowired
	CustomerSettingsRepository customerSettingsRepository;

	@Autowired
	CustomerRepository customerRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveCustomerSettings(
			@RequestBody @Valid final CustomerSettings customerSettings) throws Exception, Throwable {
		return ResponseEntity.ok(customerSettingsRepository.save(customerSettings));
	}

	@RequestMapping(value = "/getByCustomerId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long customerId)
			throws Exception, Throwable {

		Optional<Customer> customer = customerRepository.findById(customerId);
		Optional<CustomerSettings> entity = customerSettingsRepository.findByCustomer(customer.get());
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}
}
