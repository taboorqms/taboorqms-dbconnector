package com.taboor.qms.db.connector.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.PaymentPlan;

@Repository
@Transactional
public interface PaymentPlanRepository extends JpaRepository<PaymentPlan, Long> {

	@Query("select p from PaymentPlan p where paymentPlanId > 0")
	List<PaymentPlan> findActivePlans();

}
