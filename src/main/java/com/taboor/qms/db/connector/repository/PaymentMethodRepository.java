package com.taboor.qms.db.connector.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.PaymentMethod;

@Repository
@Transactional
public interface PaymentMethodRepository extends JpaRepository<PaymentMethod, Long> {

	PaymentMethod findByPaymentMethodName(String name);
	
	@Modifying
	@Query("delete from PaymentMethod pm where pm.paymentMethodId =:paymentMethodId")
	int deleteByPaymentId(@Param("paymentMethodId") long paymentMethodId);
}
