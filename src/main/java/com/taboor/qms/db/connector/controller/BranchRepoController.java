package com.taboor.qms.db.connector.controller;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.payload.GetNearByBranchPayload;
import com.taboor.qms.core.response.GetBranchListingResponse.BranchListingObject;
import com.taboor.qms.db.connector.repository.AgentRepository;
import com.taboor.qms.db.connector.repository.BranchCounterRepository;
import com.taboor.qms.db.connector.repository.BranchRepository;
import com.taboor.qms.db.connector.repository.BranchServiceTABMapperRepository;

@RestController
@RequestMapping("/tab/branches")
@CrossOrigin(origins = "*")
public class BranchRepoController {

	private static final Logger logger = LoggerFactory.getLogger(BranchRepoController.class);

	@Autowired
	BranchRepository branchRepository;

	@Autowired
	BranchCounterRepository branchCounterRepository;

	@Autowired
	AgentRepository agentRepository;

	@Autowired
	BranchServiceTABMapperRepository branchServiceTABMapperRepository;

	@PersistenceContext
	EntityManager em;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveBranch(@RequestBody @Valid final Branch branch)
			throws Exception, Throwable {
		return ResponseEntity.ok(branchRepository.save(branch));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long branchId)
			throws Exception, Throwable {

		Optional<Branch> entity = branchRepository.findById(branchId);
		if (entity.isPresent()) {
			return ResponseEntity.ok(entity.get());
		}
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/serviceCenter", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByServiceCenter(@RequestParam @Valid final Long serviceCentreId)
			throws Exception, Throwable {

		List<Branch> entity = branchRepository.findByServiceCenter(serviceCentreId);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<Branch> entity = branchRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/count/serviceCenterId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> countByServiceCenter(Long serviceCenterId) throws Exception, Throwable {

		int entity = branchRepository.countByServiceCenter(serviceCenterId);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/countCity/serviceCenterId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> countCityByServiceCenter(Long serviceCenterId) throws Exception, Throwable {

		int entity = branchRepository.countCityByServiceCenter(serviceCenterId);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/nearby", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getNearByBranches(@RequestBody @Valid final GetNearByBranchPayload payload)
			throws Exception, Throwable {

		String queryBuilder = "Select distinct(branch_row.*) from tab_branch branch_row ";
//		int dayofWeek = payload.getCustomerDateTime().getDayOfWeek().getValue();
//		LocalTime currentTime = LocalTime.of(payload.getCustomerDateTime().getHour(),
//				payload.getCustomerDateTime().getMinute());
		List<Long> serviceIds = payload.getServiceIds();
		List<Branch> branchList = new ArrayList<Branch>();

		if (payload.isFiltersApplied()) {
			// In this case filters are applied

			// search by service name
			if (payload.isSearchByServiceName()) {
				queryBuilder = queryBuilder.concat(
						" INNER JOIN tab_branch_service_mapper service_row ON branch_row.branch_id = service_row.branch_id ");
			}

			// open now
//			if (payload.isStatusOpenNow()) { 
//				queryBuilder = queryBuilder.concat(" INNER JOIN tab_working_hours working_row ON branch_row.branch_id = working_row.branch_id ");
//			}

			queryBuilder = queryBuilder.concat(
					" where sqrt( (111.12 * (branch_row.latitude - :latitude)) * (111.12 * (branch_row.latitude - :latitude))"
							+ "+(111.12 * (branch_row.longitude - :longitude) * cos(:latitude / 92.215)) * (111.12 * (branch_row.longitude - :longitude) * cos(:latitude / 92.215)) ) < :radius ");

			// apply conditions
			// search by service name
			if (!serviceIds.isEmpty() && payload.isSearchByServiceName()) {
				queryBuilder = queryBuilder.concat(" AND service_row.servicetab_id IN (" + serviceIds + ") ");
				queryBuilder = queryBuilder.replace("[", "");
				queryBuilder = queryBuilder.replace("]", "");
			}

			// search by service center
			if (payload.getServiceCentreId() != null) {
				queryBuilder = queryBuilder
						.concat(" AND branch_row.service_center_id = " + payload.getServiceCentreId() + " ");
			}

			// open now
			if (payload.isStatusOpenNow()) {
				queryBuilder = queryBuilder.concat(" AND branch_row.branch_status = 1 ");
			}

			// sort by distance
			if (payload.isSortByDistance()) {
				queryBuilder = queryBuilder.concat(
						" Order By sqrt( (111.12 * (branch_row.latitude - :latitude)) * (111.12 * (branch_row.latitude - :latitude))"
								+ "+(111.12 * (branch_row.longitude - :longitude) * cos(:latitude / 92.215)) * (111.12 * (branch_row.longitude - :longitude) * cos(:latitude / 92.215)) ) ");
			}

			// sort by service time
			if (payload.isSortByServiceTime()) {
				queryBuilder = queryBuilder.concat(" Order By branch_row.average_service_time ");
			}

			// sort by waiting time
			if (payload.isSortByWaitingTime()) {
				queryBuilder = queryBuilder.concat(" Order By branch_row.average_waiting_time ");
			}
		} else {
			queryBuilder = queryBuilder.concat(
					" where sqrt( (111.12 * (branch_row.latitude - :latitude)) * (111.12 * (branch_row.latitude - :latitude)) + (111.12 * (branch_row.longitude - :longitude) * cos(:latitude / 92.215)) * (111.12 * (branch_row.longitude - :longitude) * cos(:latitude / 92.215)) ) < :radius ");
		}

		Query query = em.createNativeQuery(queryBuilder);
		query.setParameter("radius", payload.getRadius());
		query.setParameter("latitude", payload.getCustomerLat());
		query.setParameter("longitude", payload.getCustomerLong());
//		if (payload.isStatusOpenNow() && payload.isFiltersApplied()) { 
//			query.setParameter("current_time", currentTime);
//			query.setParameter("working_day", dayofWeek);			
//		}
//		logger.info("QUERY: " + queryBuilder);

		List<Object[]> entity = query.getResultList();

		if (entity.isEmpty())
			return null;
		else {

			for (Object[] row : entity) {
				Branch branch = new Branch();
				branch.setBranchId(Long.parseLong(row[0].toString()));
				branch.setAddress(row[1].toString());
				branch.setAverageServiceTime(LocalTime.parse(row[2].toString()));
				branch.setAverageTicketPerDay(Integer.parseInt(row[3].toString()));
				branch.setAverageWaitingTime(LocalTime.parse(row[4].toString()));
				branch.setBranchName(row[5].toString());
				branch.setBranchNameArabic(row[6] != null ? row[6].toString() : "");
				branch.setBranchStatus(Integer.parseInt(row[7].toString()));
				branch.setCity(row[8].toString());
				branch.setCreatedTime(OffsetDateTime.of(
						LocalDateTime.parse(row[9].toString(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")),
						ZoneOffset.UTC));
				branch.setEmailAddress(row[10].toString());
				branch.setLatitude(Double.parseDouble(row[11].toString()));
				branch.setLongitude(Double.parseDouble(row[12].toString()));
				branch.setPhoneNumber(row[13].toString());
				branch.setRating(Double.parseDouble(row[14].toString()));
				branch.setServiceCenter(null);
				branchList.add(branch);
			}
		}
		return ResponseEntity.ok(branchList);
	}

	@RequestMapping(value = "/getBranchListing/branchList", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getListingByQueue(@RequestBody @Valid final List<Branch> branchList)
			throws Exception, Throwable {
		List<BranchListingObject> branchListingObjects = new ArrayList<BranchListingObject>();
		for (Branch branch : branchList) {
			BranchListingObject branchObject = new BranchListingObject();
			branchObject.setBranch(branch);
			branchObject.setNoOfTotalCounters(branchCounterRepository.countByBranch(branch));
			branchObject.setNoOfAgents(agentRepository.countByBranch(branch));
			branchObject
					.setProvidedServices(branchServiceTABMapperRepository.findServiceByBranchId(branch.getBranchId()));
			branchListingObjects.add(branchObject);
		}

		return ResponseEntity.ok(branchListingObjects);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteBranch(@RequestParam @Valid Long branchId)
			throws Exception, Throwable {
		if (branchRepository.deleteByBranchId(branchId) > 0)
			return ResponseEntity.ok(true);
		return ResponseEntity.ok(false);
	}
}
