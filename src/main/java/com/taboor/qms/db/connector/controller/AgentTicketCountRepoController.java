package com.taboor.qms.db.connector.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.AgentTicketCount;
import com.taboor.qms.core.payload.CheckAgentTicketCountPayload;
import com.taboor.qms.core.payload.GetAgentTicketCountPayload;
import com.taboor.qms.db.connector.repository.AgentTicketCountRepository;

@RestController
@RequestMapping("/tab/agentticketcounts")
@CrossOrigin(origins = "*")
public class AgentTicketCountRepoController {

	@Autowired
	AgentTicketCountRepository agentTicketCountRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveCustomerSettings(
			@RequestBody @Valid final AgentTicketCount agentTicketCount) throws Exception, Throwable {
		return ResponseEntity.ok(agentTicketCountRepository.save(agentTicketCount));
	}

	@RequestMapping(value = "/getByDateAndUser", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestBody @Valid final CheckAgentTicketCountPayload payload)
			throws Exception, Throwable {

		Optional<AgentTicketCount> entity = agentTicketCountRepository.findByDateOfServing(payload.getServingDate(),
				payload.getUserId());
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}
	
	@RequestMapping(value = "/getByDateAndBranch", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByDateAndBranch(@RequestBody @Valid final GetAgentTicketCountPayload payload)
			throws Exception, Throwable {

		Optional<AgentTicketCount> entity = agentTicketCountRepository.findByServingCounter(payload.getServingDate(),
				payload.getBranchCounterId());
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/getByDateAndBranchAndUser", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByDateAndBranchAndUser(
			@RequestBody @Valid final CheckAgentTicketCountPayload payload) throws Exception, Throwable {

		Optional<AgentTicketCount> entity = agentTicketCountRepository.findByServingCounterAndUser(payload.getServingDate(),
				payload.getBranchCounterId(), payload.getUserId());
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

}
