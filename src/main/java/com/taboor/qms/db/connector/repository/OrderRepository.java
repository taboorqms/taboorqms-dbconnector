/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taboor.qms.db.connector.repository;

import com.taboor.qms.core.model.BranchBookmark;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author S H K
 */
public interface OrderRepository extends JpaRepository<BranchBookmark, Long> {
    
}
