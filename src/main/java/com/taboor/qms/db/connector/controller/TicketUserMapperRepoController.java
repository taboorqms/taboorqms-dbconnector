package com.taboor.qms.db.connector.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.TicketUserMapper;
import com.taboor.qms.db.connector.repository.TicketUserMapperRepository;

@RestController
@RequestMapping("/tab/ticketusermapper")
@CrossOrigin(origins = "*")
public class TicketUserMapperRepoController {

	@Autowired
	TicketUserMapperRepository ticketUserMapperRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveTicketUserMapper(
			@RequestBody @Valid final TicketUserMapper ticketUserMapper) throws Exception, Throwable {
		return ResponseEntity.ok(ticketUserMapperRepository.save(ticketUserMapper));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long ticketUserMapperId)
			throws Exception, Throwable {

		Optional<TicketUserMapper> entity = ticketUserMapperRepository.findById(ticketUserMapperId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<TicketUserMapper> entity = ticketUserMapperRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/ticketId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByTicketId(@RequestParam @Valid final Long ticketId)
			throws Exception, Throwable {

		Optional<TicketUserMapper> entity = ticketUserMapperRepository.findByTicketId(ticketId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}
	
	@RequestMapping(value = "/get/inQueue", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getInQueue(@RequestParam @Valid final Long userId) throws Exception, Throwable {

		int entity = ticketUserMapperRepository.countInQueueByUser(userId);
		return ResponseEntity.ok(entity);
	}
}
