package com.taboor.qms.db.connector.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.TaboorBankAccount;

@Repository
@Transactional
public interface TaboorBankAccountRepository extends JpaRepository<TaboorBankAccount, Long> {

	List<TaboorBankAccount> findByDefaultAccount(boolean defaultAccount);

}
