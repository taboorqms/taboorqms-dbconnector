package com.taboor.qms.db.connector.repository;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.BranchCounter;
import com.taboor.qms.core.model.BranchCounterServiceTABMapper;
import com.taboor.qms.core.model.ServiceTAB;

@Repository
@Transactional
public interface BranchCounterServiceTABMapperRepository extends JpaRepository<BranchCounterServiceTABMapper, Long> {

	@Query("Select count(b) from BranchCounterServiceTABMapper b where branch_counter_id = :counterId")
	int countByBranchCounter(@Param("counterId") long counterId);

	@Query("Select b.serviceTAB from BranchCounterServiceTABMapper b where branch_counter_id = :counterId")
	List<ServiceTAB> getServiceByBranchCounter(@Param("counterId") long counterId);

	@Query("Select bcst from BranchCounterServiceTABMapper bcst where branch_counter_id = :counterId")
	List<BranchCounterServiceTABMapper> findByBranchCounter(@Param("counterId") Long counterId);

	@Query("Select bcst.branchCounter from BranchCounterServiceTABMapper bcst inner join BranchCounter bc"
			+ " on bcst.branchCounter = bc.counterId where bc.branch.branchId = :branchId and servicetab_id = :serviceId")
	List<BranchCounter> findTransferableBranchCounters(@Param("branchId") Long branchId,
			@Param("serviceId") Long serviceId);

	@Modifying
	@Query("delete from BranchCounterServiceTABMapper b where branch_counter_id = :counterId")
	int deleteByBranchId(@Valid Long counterId);

	@Query("SELECT bcstm.branchCounter FROM BranchCounterServiceTABMapper bcstm inner join BranchCounter bc"
			+ " on bcstm.branchCounter = bc.counterId where bc.branch = :branch and bcstm.serviceTAB = :serviceTAB")
	List<BranchCounter> findBranchCounterByBranchAndService(@Param("branch") Branch branch,
			@Param("serviceTAB") ServiceTAB serviceTAB);

	@Query("SELECT bcstm FROM BranchCounterServiceTABMapper bcstm inner join BranchCounter bc"
			+ " on bcstm.branchCounter = bc.counterId where bc.branch = :branch and bcstm.serviceTAB = :serviceTAB")
	List<BranchCounterServiceTABMapper> findByBranchAndService(@Param("branch") Branch branch,
			@Param("serviceTAB") ServiceTAB serviceTAB);
}
