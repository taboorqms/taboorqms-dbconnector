package com.taboor.qms.db.connector.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.User;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findByEmail(String email);
	Optional<User> findByPhoneNumber(String number);

	Boolean existsByEmail(String email);
	Boolean existsByPhoneNumber(String phoneNumber);

	@Modifying
	@Query("DELETE from User where userId = :user_id")
	int deleteByUserId(@Param("user_id") Long userId);

//	@RestResource(rel = "action", path = "savee")
//	@Override
//	default <S extends User> S save(S entity) {
//		return entity;
//	}
}
