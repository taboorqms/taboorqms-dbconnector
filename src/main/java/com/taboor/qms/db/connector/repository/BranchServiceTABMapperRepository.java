package com.taboor.qms.db.connector.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.BranchServiceTABMapper;
import com.taboor.qms.core.model.ServiceTAB;

@Repository
@Transactional
public interface BranchServiceTABMapperRepository extends JpaRepository<BranchServiceTABMapper, Long> {

	@Query("SELECT bsm.serviceTAB FROM BranchServiceTABMapper bsm where branch_id = :branchId")
	List<ServiceTAB> findServiceByBranchId(@Param("branchId") Long branchId);

	@Query("SELECT bsm.serviceTAB FROM BranchServiceTABMapper bsm where branch_id = :branchId and servicetab_id = :serviceId")
	Optional<ServiceTAB> findServiceByBranchIdAndServiceId(@Param("branchId") Long branchId,
			@Param("serviceId") Long serviceId);

	@Query("SELECT count(bsm.branchServiceId) FROM BranchServiceTABMapper bsm where branch_id = :branchId")
	int countByBranchId(@Param("branchId") Long branchId);

	@Modifying
	@Query("delete from BranchServiceTABMapper where branch_id = :branchId")
	int deleteByBranch(@Param("branchId") Long branchId);

	@Modifying
	@Query("delete from BranchServiceTABMapper where branch_id = :branchId and servicetab_id = :serviceId")
	int deleteByBranchAndService(@Param("branchId") Long branchId, @Param("serviceId") Long serviceId);

}
