package com.taboor.qms.db.connector.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.BranchTVConfig;

@Repository
@Transactional
public interface BranchTVConfigRepository extends JpaRepository<BranchTVConfig, Long> {
	
	@Query("select bt from BranchTVConfig bt where branch_id = :branchId")
	Optional<BranchTVConfig> getByBranch(@Param("branchId") long branchId);
	
	@Modifying
	@Query("delete from BranchTVConfig btc where btc.deviceToken = :token")
	int deleteByDeviceToken(@Param("token") String token);
	
}
