package com.taboor.qms.db.connector.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.ServiceTAB;

@Repository
@Transactional
public interface ServiceTABRepository extends JpaRepository<ServiceTAB, Long> {

	@Query("select s.serviceName from ServiceTAB s")
	List<String> findAllNames();

	@Query("select s from ServiceTAB s where s.serviceName = :serviceName")
	Optional<ServiceTAB> findByServiceName(@Param("serviceName") String serviceName);
}
