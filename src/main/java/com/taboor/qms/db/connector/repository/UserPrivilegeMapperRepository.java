package com.taboor.qms.db.connector.repository;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.UserPrivilege;
import com.taboor.qms.core.model.UserPrivilegeMapper;
import com.taboor.qms.core.utils.PrivilegeName;

@Repository
@Transactional
public interface UserPrivilegeMapperRepository extends JpaRepository<UserPrivilegeMapper, Long> {

	@Query("select up.privilegeName from UserPrivilegeMapper upm inner join UserPrivilege up on up.privilegeId = upm.userPrivilege where user_id = :userId")
	List<PrivilegeName> findPrivilegeNameByUserId(@Param("userId") Long userId);

	@Query("select upm.userPrivilege from UserPrivilegeMapper upm where user_id = :userId")
	List<UserPrivilege> findPrivilgesByUserId(@Valid Long userId);

	@Modifying
	@Query("delete from UserPrivilegeMapper where user_id = :userId")
	int deleteByUserId(@Param("userId") Long userId);
}
