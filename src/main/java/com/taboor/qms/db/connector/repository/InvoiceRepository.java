package com.taboor.qms.db.connector.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.Invoice;
import com.taboor.qms.core.model.ServiceCenter;
import com.taboor.qms.core.utils.InvoiceStatus;
import com.taboor.qms.core.utils.InvoiceType;

@Repository
@Transactional
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

	@Query("SELECT i FROM Invoice i inner join ServiceCenterSubscription scs on i.serviceCenterSubscription = scs.subscriptionId WHERE scs.serviceCenter = :serviceCenter")
	List<Invoice> findByServiceCenter(@Param("serviceCenter") ServiceCenter serviceCenter);

	@Query("SELECT max(invoiceNumber) FROM Invoice")
	String findNextInvoiceNumber();

	@Query("select i from Invoice i where invoiceType = :invoiceType and invoiceStatus = :invoiceStatus and date(dueDate) = :dueDate")
	List<Invoice> findByTypeAndStatusAndDueDate(@Param("invoiceType") InvoiceType invoiceType,
			@Param("invoiceStatus") InvoiceStatus invoiceStatus, @Param("dueDate") LocalDate dueDate);

}
