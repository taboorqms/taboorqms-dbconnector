package com.taboor.qms.db.connector.controller;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.GuestTicket;
import com.taboor.qms.db.connector.repository.GuestTicketRepository;

@RestController
@RequestMapping("/tab/guesttickets")
@CrossOrigin(origins = "*")
public class GuestTicketRepoController {

	@Autowired
	GuestTicketRepository guestTicketRepository;

	@PersistenceContext
	EntityManager em;

	@RequestMapping(value = "/save", method = RequestMethod.POST, 
			consumes = "application/json; charset=UTF-8", produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveAgentCounter(
			@RequestBody @Valid final GuestTicket guestTicket) throws Exception, Throwable {
		return ResponseEntity.ok(guestTicketRepository.save(guestTicket));
	}

	@RequestMapping(value = "/get/ticket", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByTicket(@RequestParam @Valid final Long ticketId) 
			throws Exception, Throwable {
		Optional<GuestTicket> entity = guestTicketRepository.findByTicket(ticketId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}
	
}
