package com.taboor.qms.db.connector.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.ServiceRequirement;

@Repository
@Transactional
public interface ServiceRequirementRepository extends JpaRepository<ServiceRequirement, Long> {

	@Query("select sr from ServiceRequirement sr where sr.requirementName = :requirementName")
	Optional<ServiceRequirement> findByServiceRequirementName(@Param("requirementName") String requirementName);
}
