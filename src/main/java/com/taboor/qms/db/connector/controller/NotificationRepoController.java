package com.taboor.qms.db.connector.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.Notification;
import com.taboor.qms.core.payload.GetByIdListPayload;
import com.taboor.qms.core.payload.GetNotificationByUserPayload;
import com.taboor.qms.core.utils.NotificationType;
import com.taboor.qms.db.connector.repository.NotificationRepository;

@RestController
@RequestMapping("/tab/notifications")
@CrossOrigin(origins = "*")
public class NotificationRepoController {

	@Autowired
	NotificationRepository notificationRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveNotification(@RequestBody @Valid final Notification notification)
			throws Exception, Throwable {

		return ResponseEntity.ok(notificationRepository.save(notification));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long notificationId)
			throws Exception, Throwable {

		Optional<Notification> entity = notificationRepository.findById(notificationId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/count", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getCountByUserId(@RequestParam @Valid final Long userId)
			throws Exception, Throwable {

		Integer entity = Integer.valueOf(notificationRepository.findUnReadNotificationCountByUserId(userId));
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/user", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getNotificationByUser(
			@RequestBody @Valid final GetNotificationByUserPayload payload) throws Exception, Throwable {

		Pageable pageable = PageRequest.of(payload.getPageNo(), payload.getLimit());
		List<Notification> entity = notificationRepository.findByUserAndType(payload.getUser().getUserId(),
				NotificationType.IN_APP.getType(), pageable);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/email/user", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getEmailNotificationByUser(
			@RequestBody @Valid final GetNotificationByUserPayload payload) throws Exception, Throwable {

		Pageable pageable = PageRequest.of(payload.getPageNo(), payload.getLimit());
		List<Notification> entity = notificationRepository.findByUserAndType(payload.getUser().getUserId(),
				NotificationType.EMAIL.getType(), pageable);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<Notification> entity = notificationRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/update/readstatus", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> updateReadStatus(@RequestBody @Valid final GetByIdListPayload payload)
			throws Exception, Throwable {

		if (!payload.getIds().isEmpty())
			notificationRepository.updateUserNotificationReadStatus(payload.getIds());
		return ResponseEntity.ok(payload.getIds());
	}

}
