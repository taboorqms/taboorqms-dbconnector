package com.taboor.qms.db.connector.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.exception.xyzResponseCode;
import com.taboor.qms.core.exception.xyzResponseMessage;
import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.ServiceCenter;
import com.taboor.qms.core.model.ServiceCenterEmployee;
import com.taboor.qms.core.response.EmployeeObject;
import com.taboor.qms.core.response.GetEmployeeListResponse;
import com.taboor.qms.core.utils.RoleName;
import com.taboor.qms.db.connector.repository.BranchRepository;
import com.taboor.qms.db.connector.repository.ServiceCenterEmpBranchMapperRepository;
import com.taboor.qms.db.connector.repository.ServiceCenterEmployeeRepository;
import com.taboor.qms.db.connector.repository.UserPrivilegeMapperRepository;
import com.taboor.qms.db.connector.repository.UserPrivilegeRepository;
import com.taboor.qms.db.connector.repository.UserRoleMapperRepository;

@RestController
@RequestMapping("/tab/servicecenteremployees")
@CrossOrigin(origins = "*")
public class ServiceCenterEmployeeRepoController {

	@Autowired
	ServiceCenterEmployeeRepository serviceCenterEmployeeRepository;

	@Autowired
	ServiceCenterEmpBranchMapperRepository serviceCenterEmpBranchMapperRepository;

	@Autowired
	UserPrivilegeMapperRepository userPrivilegeMapperRepository;

	@Autowired
	UserPrivilegeRepository userPrivilegeRepository;

	@Autowired
	BranchRepository branchRepository;

	@Autowired
	UserRoleMapperRepository userRoleMapperRepository;

	@PersistenceContext
	EntityManager em;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveServiceCenterEmployee(
			@RequestBody @Valid final ServiceCenterEmployee serviceCenterEmployee) throws Exception, Throwable {
		return ResponseEntity.ok(serviceCenterEmployeeRepository.save(serviceCenterEmployee));
	}

	@RequestMapping(value = "/get/userId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByUserId(@RequestParam @Valid final Long userId)
			throws Exception, Throwable {

		Optional<ServiceCenterEmployee> entity = serviceCenterEmployeeRepository.findByUserId(userId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<ServiceCenterEmployee> entity = serviceCenterEmployeeRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/userEmail", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByUserEmail(@RequestParam @Valid final String email)
			throws Exception, Throwable {

		Optional<ServiceCenterEmployee> entity = serviceCenterEmployeeRepository.findByUserEmail(email);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/countEmp/serviceCenter", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> countEmpByServiceCenter(@RequestParam @Valid final Long serviceCenterId)
			throws Exception, Throwable {

		int entity = serviceCenterEmployeeRepository.countEmpByServiceCenter(serviceCenterId);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/getAdmin/serviceCenter", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAdminByServiceCenter(@RequestBody final ServiceCenter serviceCenter)
			throws Exception, Throwable {

		List<ServiceCenterEmployee> entity = serviceCenterEmployeeRepository
				.findAdminByServiceCenter(RoleName.Service_Center_Admin.name(), serviceCenter.getServiceCenterId());
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/getList", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getList(@RequestBody final ServiceCenterEmployee serviceCenterEmployee)
			throws Exception, Throwable {

		List<EmployeeObject> employeeList = new ArrayList<EmployeeObject>();

		List<Branch> empBranchList = serviceCenterEmpBranchMapperRepository
				.findBranchByEmpId(serviceCenterEmployee.getEmployeeId());
		List<Long> branchIdList = new ArrayList<Long>();
		for (Branch branch : empBranchList)
			branchIdList.add(branch.getBranchId());

		List<ServiceCenterEmployee> employees;
		if (branchIdList.isEmpty())
			employees = new ArrayList<ServiceCenterEmployee>();
		else
			employees = serviceCenterEmpBranchMapperRepository.findEmpByBranchIdList(branchIdList);

		for (ServiceCenterEmployee emp : employees) {
			EmployeeObject employeeObject = new EmployeeObject();
			employeeObject.setEmployee(emp);
			employeeObject.setBranchList(serviceCenterEmpBranchMapperRepository.findBranchByEmpId(emp.getEmployeeId()));
			employeeObject
					.setPrivileges(userPrivilegeMapperRepository.findPrivilgesByUserId(emp.getUser().getUserId()));
			employeeObject.setRoleId(userRoleMapperRepository.findByUser(emp.getUser()).getUserRole().getRoleId());
			employeeList.add(employeeObject);
		}

		return ResponseEntity.ok(new GetEmployeeListResponse(xyzResponseCode.SUCCESS.getCode(),
				xyzResponseMessage.USER_FETCHED.getMessage(), employeeList,
				branchRepository.countByServiceCenter(serviceCenterEmployee.getServiceCenter().getServiceCenterId()),
				userPrivilegeRepository.count()));
	}

	@RequestMapping(value = "/getNextEmpNumber", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getNextEmpNumber(@RequestParam @Valid final Long serviceCenterId)
			throws Exception, Throwable {

		Pageable getLastestAndSortedByEmployeeIdDesc = PageRequest.of(0, 1, Sort.by("employeeId").descending());
		List<String> entityList = serviceCenterEmployeeRepository.getCenterNextEmpNumber(serviceCenterId,
				getLastestAndSortedByEmployeeIdDesc);
		String entity;
		if (entityList == null)
			entity = "FirstEntry";
		else if (entityList.isEmpty())
			entity = "FirstEntry";
		else
			entity = entityList.get(0).substring(0, 2)
					+ String.format("%04d", Long.valueOf(entityList.get(0).substring(2)) + 1);
		return ResponseEntity.ok(entity);
	}
        
        @RequestMapping(value = "/delete", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteUser(@RequestParam @Valid final long userId)
			throws Exception, Throwable {
		ServiceCenterEmployee scEmp = serviceCenterEmployeeRepository.findById(userId).get();
		serviceCenterEmployeeRepository.delete(scEmp);
		return ResponseEntity.ok(1);
	}
}
