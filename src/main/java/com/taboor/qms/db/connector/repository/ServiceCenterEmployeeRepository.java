package com.taboor.qms.db.connector.repository;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.ServiceCenter;
import com.taboor.qms.core.model.ServiceCenterEmployee;
import com.taboor.qms.core.model.User;

@Repository
@Transactional
public interface ServiceCenterEmployeeRepository extends JpaRepository<ServiceCenterEmployee, Long> {

	@Query("SELECT sce FROM ServiceCenterEmployee sce right join User u on sce.user = u.userId where u.email = :userEmail")
	Optional<ServiceCenterEmployee> findByUserEmail(@Param("userEmail") String userEmail);

	@Query("select sce from ServiceCenterEmployee sce where user_id=:userId")
	Optional<ServiceCenterEmployee> findByUserId(Long userId);

	@Query("select count(sce) from ServiceCenterEmployee sce where service_center_id=:serviceCenterId")
	int countEmpByServiceCenter(@Valid Long serviceCenterId);

	@Query("select sce from ServiceCenterEmployee sce inner join User u on u.userId=sce.user where u.roleName =:roleName and service_center_id=:serviceCenterId")
	List<ServiceCenterEmployee> findAdminByServiceCenter(@Param("roleName") String roleName,
			@Param("serviceCenterId") Long serviceCenterId);

	List<ServiceCenterEmployee> findByServiceCenter(ServiceCenter serviceCenter);

	@Query("select sce.employeeNumber from ServiceCenterEmployee sce where service_center_id=:serviceCenterId")
	List<String> getCenterNextEmpNumber(@Param("serviceCenterId") Long serviceCenterId,
			Pageable getLastestAndSortedByEmployeeIdDesc);

	@Query("select sce.user from ServiceCenterEmployee sce where service_center_id=:serviceCenterId")
	List<User> findUserByServiceCenterId(@Param("serviceCenterId") Long serviceCenterId);

}
