package com.taboor.qms.db.connector.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.UserFAQ;
import com.taboor.qms.db.connector.repository.UserFAQRepository;

@RestController
@RequestMapping("/tab/userfaqs")
@CrossOrigin(origins = "*")
public class UserFAQRepoController {

	@Autowired
	UserFAQRepository userFAQRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveUserFeedback(@RequestBody @Valid final UserFAQ userFAQ)
			throws Exception, Throwable {
		return ResponseEntity.ok(userFAQRepository.save(userFAQ));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long userFAQId)
			throws Exception, Throwable {

		Optional<UserFAQ> entity = userFAQRepository.findById(userFAQId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<UserFAQ> entity = userFAQRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> delete(@RequestParam @Valid final Long userFAQId)
			throws Exception, Throwable {
		userFAQRepository.deleteById(userFAQId);
		return ResponseEntity.ok(true);
	}

}
