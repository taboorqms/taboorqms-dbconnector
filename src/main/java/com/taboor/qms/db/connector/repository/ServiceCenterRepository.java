package com.taboor.qms.db.connector.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.ServiceCenter;

@Repository
@Transactional
public interface ServiceCenterRepository extends JpaRepository<ServiceCenter, Long> {

	@Query("select sc from ServiceCenter sc where user_id=:userId")
	Optional<ServiceCenter> findByUserId(Long userId);
	
	@Modifying
	@Query("delete from ServiceCenter ss where ss.serviceCenterId =:serviceCenterId")
	int deleteByServiceCenterId(@Param("serviceCenterId") long serviceCenterId);
}
