package com.taboor.qms.db.connector.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.TicketFastpass;
import com.taboor.qms.core.model.UserPaymentTransaction;

@Repository
@Transactional
public interface TicketFastpassRepository extends JpaRepository<TicketFastpass, Long> {

	@Query("select tfp from TicketFastpass tfp inner join TicketUserMapper tum on tum.ticket=tfp.ticket inner join User u on tum.user=u.userId where u.userId=:userId")
	List<TicketFastpass> findByUserId(@Param("userId") Long userId);

	@Modifying
	@Query("DELETE from TicketFastpass tfp where tfp.ticketFastpassId = :ticketFastpassId")
	int deleteByTicketFastpassId(@Param("ticketFastpassId") long ticketFastpassId);

	@Query("select tfp from TicketFastpass tfp where ticket_id=:ticketId")
	Optional<TicketFastpass> findByTicketId(@Param("ticketId") Long ticketId);

	@Query("select userPaymentTransaction from TicketFastpass tfp where ticket_id=:ticketId and tfp.status = :fastpassStatus order by payment_transaction_id desc")
	List<UserPaymentTransaction> findTransactionIdByTicketLatest(@Param("ticketId") Long ticketId,
			@Param("fastpassStatus") int fastpassStatus, Pageable pageable);

}
