package com.taboor.qms.db.connector.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.ServiceCenter;
import com.taboor.qms.core.model.SupportTicket;
import com.taboor.qms.core.utils.SupportTicketStatus;

@Repository
@Transactional
public interface SupportTicketRepository extends JpaRepository<SupportTicket, Long> {

	@Query("Select st from SupportTicket st inner join ServiceCenterEmployee sce on st.empCreatedBy = sce.employeeId where sce.serviceCenter = :serviceCenter")
	List<SupportTicket> findByServiceCenter(@Param("serviceCenter") ServiceCenter serviceCentre);

	@Modifying
	@Query("delete from SupportTicket st where st.supportTicketId =:supportTicketId")
	int deleteBySupportTicketId(@Param("supportTicketId") long supportTicketId);

	@Query("Select count(st) from SupportTicket st inner join ServiceCenterEmployee sce on st.empCreatedBy = sce.employeeId where sce.serviceCenter = :serviceCenter")
	int countByServiceCenter(@Param("serviceCenter") ServiceCenter serviceCentre);
        
        @Query("Select count(st) from SupportTicket st inner join ServiceCenterEmployee sce on st.empCreatedBy = sce.employeeId where sce.serviceCenter = :serviceCenter and st.supportTicketStatus != :supportTicketStatus")
	int openCountByServiceCenter(@Param("serviceCenter") ServiceCenter serviceCentre, @Param("supportTicketStatus") SupportTicketStatus supportTicketStatus);

}
