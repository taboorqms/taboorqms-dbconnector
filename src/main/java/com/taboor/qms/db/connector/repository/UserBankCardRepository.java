package com.taboor.qms.db.connector.repository;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.UserBankCard;

@Repository
@Transactional
public interface UserBankCardRepository extends JpaRepository<UserBankCard, Long> {

	@Query("select ubc from UserBankCard ubc where user_id=:userId")
	List<UserBankCard> findByUserId(@Param("userId") Long userId);

	@Modifying
	@Query("DELETE from UserBankCard where bankCardId = :userBankCardId")
	int deleteByUserBankCardId(@Valid Long userBankCardId);

	@Query("select ubc from UserBankCard ubc inner join PaymentMethod pm on pm.paymentMethodId=ubc.paymentMethod where user_id=:userId and pm.paymentMethodId = :paymentMethodId")
	Optional<UserBankCard> findByUserIdAndPaymentMethodId(@Param("userId") Long userId,
			@Param("paymentMethodId") Long paymentMethodId);
	
	@Query("select ubc from UserBankCard ubc where service_center_id=:serviceCenterId")
	List<UserBankCard> findByServiceCenterId(@Param("serviceCenterId") Long serviceCenterId);
}
