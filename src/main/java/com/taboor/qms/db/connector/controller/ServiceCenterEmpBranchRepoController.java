package com.taboor.qms.db.connector.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.ServiceCenterEmpBranchMapper;
import com.taboor.qms.core.model.ServiceCenterEmployee;
import com.taboor.qms.core.utils.SubscriptionStatus;
import com.taboor.qms.db.connector.repository.ServiceCenterEmpBranchMapperRepository;
import com.taboor.qms.db.connector.repository.ServiceCenterSubscriptionRepository;

@RestController
@RequestMapping("/tab/employeebranchmapper")
@CrossOrigin(origins = "*")
public class ServiceCenterEmpBranchRepoController {

	@Autowired
	ServiceCenterEmpBranchMapperRepository userBranchMapperRepository;

	@Autowired
	ServiceCenterSubscriptionRepository serviceCenterSubscriptionRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveServiceCenterEmpBranchMapper(
			@RequestBody @Valid final ServiceCenterEmpBranchMapper serviceCenterEmpBranchMapper)
			throws Exception, Throwable {
		return ResponseEntity.ok(userBranchMapperRepository.save(serviceCenterEmpBranchMapper));
	}

	@RequestMapping(value = "/saveAll", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveAll(
			@RequestBody @Valid final List<ServiceCenterEmpBranchMapper> serviceCenterEmpBranchMappers)
			throws Exception, Throwable {
		return ResponseEntity.ok(userBranchMapperRepository.saveAll(serviceCenterEmpBranchMappers));
	}

	@RequestMapping(value = "/updateAll", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> updateAll(
			@RequestBody @Valid final List<ServiceCenterEmpBranchMapper> serviceCenterEmpBranchMappers)
			throws Exception, Throwable {
		userBranchMapperRepository
				.deleteByServiceCenterEmployee(serviceCenterEmpBranchMappers.get(0).getServiceCenterEmployee());
		return ResponseEntity.ok(userBranchMapperRepository.saveAll(serviceCenterEmpBranchMappers));
	}

	@RequestMapping(value = "/getBranch/serviceCenterEmp", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getBranchByEmp(@RequestParam Long employeeId) throws Exception, Throwable {

		List<Branch> entity = userBranchMapperRepository.findBranchByEmpId(employeeId);
		if (entity == null)
			return ResponseEntity.ok(new ArrayList<Branch>());
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/countBranch/serviceCenterEmp", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> countBranchByEmp(@RequestParam Long employeeId) throws Exception, Throwable {

		ServiceCenterEmployee employee = new ServiceCenterEmployee();
		employee.setEmployeeId(employeeId);
		return ResponseEntity.ok(userBranchMapperRepository.countByServiceCenterEmployee(employee));
	}

	@RequestMapping(value = "/getBranch/user", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getBranchByUser(@RequestParam Long userId) throws Exception, Throwable {

		List<Branch> entity = userBranchMapperRepository.findBranchByUserId(userId);
		if (entity == null)
			return ResponseEntity.ok(new ArrayList<Branch>());
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/checkBranchUserLimit", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> checkBranchUserLimit(@RequestBody List<Long> branchIds)
			throws Exception, Throwable {

		List<Long> response = new ArrayList<Long>();
		long serviceCenterId = branchIds.remove(branchIds.size() - 1);
		int noOfUsersPerBranch = serviceCenterSubscriptionRepository
				.findNoOfUsersPerBranchByServiceCenterActiveSubscription(serviceCenterId, SubscriptionStatus.ACTIVE);

		if (noOfUsersPerBranch > 0)
			for (int i = 0; i < branchIds.size(); i++)
				if (userBranchMapperRepository.countEmpByBranchId(branchIds.get(i)) < noOfUsersPerBranch)
					response.add(branchIds.get(i));
		return ResponseEntity.ok(response);

	}

}
