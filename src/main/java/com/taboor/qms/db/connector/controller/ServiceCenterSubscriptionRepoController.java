package com.taboor.qms.db.connector.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.PlanPriceObject;
import com.taboor.qms.core.model.ServiceCenter;
import com.taboor.qms.core.model.ServiceCenterSubscription;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.SubscriptionStatus;
import com.taboor.qms.db.connector.repository.BranchRepository;
import com.taboor.qms.db.connector.repository.ServiceCenterServiceTABMapperRepository;
import com.taboor.qms.db.connector.repository.ServiceCenterSubscriptionRepository;

@RestController
@RequestMapping("/tab/servicecentersubscriptions")
@CrossOrigin(origins = "*")
public class ServiceCenterSubscriptionRepoController {

	@Autowired
	ServiceCenterSubscriptionRepository serviceCenterSubscriptionRepository;

	@Autowired
	ServiceCenterServiceTABMapperRepository serviceCenterServiceTABMapperRepository;

	@Autowired
	BranchRepository branchRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveServiceCenterSubscription(
			@RequestBody @Valid final ServiceCenterSubscription serviceCenterSubscription) throws Exception, Throwable {
		return ResponseEntity.ok(serviceCenterSubscriptionRepository.save(serviceCenterSubscription));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam final Long subscriptionId)
			throws Exception, Throwable {

		Optional<ServiceCenterSubscription> entity = serviceCenterSubscriptionRepository.findById(subscriptionId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/serviceCenter", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByServiceCenter(@RequestBody @Valid final ServiceCenter serviceCenter)
			throws Exception, Throwable {

		Optional<ServiceCenterSubscription> entity = serviceCenterSubscriptionRepository
				.findByServiceCenter(serviceCenter);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/getPaidPercentage/serviceCenter", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getPaidPercentageByServiceCenter(
			@RequestParam @Valid final Long serviceCenterId) throws Exception, Throwable {

		Double entity = serviceCenterSubscriptionRepository.findPaidPercentageByServiceCenterId(serviceCenterId,
				SubscriptionStatus.ACTIVE);
		if (entity == null)
			entity = 0.0;
		return ResponseEntity.ok(entity.intValue());
	}

	@RequestMapping(value = "/getAll/active", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAllActive() throws Exception, Throwable {

		List<ServiceCenterSubscription> entityList = serviceCenterSubscriptionRepository
				.findBySubscriptionStatus(SubscriptionStatus.ACTIVE);
		return ResponseEntity.ok(entityList);
	}

	@RequestMapping(value = "/get/serviceCenterId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByServiceCenterId(@RequestParam final long serviceCenterId)
			throws Exception, Throwable {

		Optional<ServiceCenterSubscription> entity = serviceCenterSubscriptionRepository
				.findByServiceCenterId(serviceCenterId);
		if (entity.isPresent()) {
			entity.get().getPaymentPlan().setPrices(GenericMapper.jsonToListObjectMapper(
					entity.get().getPaymentPlan().getPricesCollection(), PlanPriceObject.class));
			return ResponseEntity.ok(entity.get());
		}
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/checkBranchLimit", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> checkBranchLimit(@RequestParam final long serviceCenterId)
			throws Exception, Throwable {

		ServiceCenterSubscription entity = serviceCenterSubscriptionRepository
				.findActiveSubscriptionByServiceCenterId(serviceCenterId, SubscriptionStatus.ACTIVE).get();
		int branchCount = branchRepository.countByServiceCenter(serviceCenterId);
		if (branchCount < entity.getNoOfBranches())
			return ResponseEntity.ok(false);
		return ResponseEntity.ok(true);
	}

	@RequestMapping(value = "/checkServiceLimit", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> checkServiceLimit(@RequestParam final long serviceCenterId)
			throws Exception, Throwable {

		ServiceCenterSubscription entity = serviceCenterSubscriptionRepository
				.findActiveSubscriptionByServiceCenterId(serviceCenterId, SubscriptionStatus.ACTIVE).get();
		int serviceCount = serviceCenterServiceTABMapperRepository.countByServiceCenter(serviceCenterId);
		if (serviceCount < entity.getPaymentPlan().getNoOfServices())
			return ResponseEntity.ok(false);
		return ResponseEntity.ok(true);
	}
}
