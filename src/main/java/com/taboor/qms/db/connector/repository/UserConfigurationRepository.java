package com.taboor.qms.db.connector.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.UserConfiguration;

@Repository
@Transactional
public interface UserConfigurationRepository extends JpaRepository<UserConfiguration, Long> {

	@Query("select uc from UserConfiguration uc where user_id =:userId")
	Optional<UserConfiguration> findByUserId(@Param("userId") Long userId);

}
