package com.taboor.qms.db.connector.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.Agent;
import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.User;
import com.taboor.qms.core.model.UserPrivilege;
import com.taboor.qms.core.response.GetAgentListResponse.AgentObject;
import com.taboor.qms.core.utils.FileDataService;
import com.taboor.qms.db.connector.repository.AgentRepository;
import com.taboor.qms.db.connector.repository.BranchCounterAgentMapperRepository;
import com.taboor.qms.db.connector.repository.ServiceCenterEmployeeRepository;
import com.taboor.qms.db.connector.repository.TicketFeedbackRepository;
import com.taboor.qms.db.connector.repository.UserPrivilegeMapperRepository;
import com.taboor.qms.db.connector.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("/tab/agents")
@CrossOrigin(origins = "*")
public class AgentRepoController {

    
    private static final Logger logger = LoggerFactory.getLogger(AgentRepoController.class);


	@Autowired
	AgentRepository agentRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	BranchCounterAgentMapperRepository branchCounterAgentMapperRepository;

	@Autowired
	UserPrivilegeMapperRepository userPrivilegeMapperRepository;

	@Autowired
	TicketFeedbackRepository ticketFeedbackRepository;

	@Autowired
	ServiceCenterEmployeeRepository serviceCenterEmployeeRepository;

	@Autowired
	private FileDataService fileDataService;

	@PersistenceContext
	EntityManager em;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveAgent(@RequestBody @Valid final Agent agent)
			throws Exception, Throwable {
		return ResponseEntity.ok(agentRepository.save(agent));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long agentId)
			throws Exception, Throwable {

		Optional<Agent> entity = agentRepository.findById(agentId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<Agent> entity = agentRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/count/serviceCenterId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> countByServiceCenter(@RequestParam @Valid final Long serviceCenterId)
			throws Exception, Throwable {

		int entity = agentRepository.countByServiceCenter(serviceCenterId);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/count/branchList", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> countByServiceCenter(@RequestBody @Valid final List<Long> branchIdList)
			throws Exception, Throwable {

		if (branchIdList.isEmpty())
			return ResponseEntity.ok(0);
		int entity = agentRepository.countByBranchList(branchIdList);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/getNextAgentEmpNumber/serviceCenter", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getNextAgentEmpNumber(@RequestParam @Valid final String namePrefix)
			throws Exception, Throwable {

//		Pageable getLastestAndSortedByAgentIdDesc = PageRequest.of(0, 1, Sort.by("employeeId").descending());
//		List<ServiceCenterEmployee> entityList = serviceCenterEmployeeRepository
//				.findByEmployeeNumberStartsWith(namePrefix, getLastestAndSortedByAgentIdDesc);
//
//		String entity;
//		if (entityList.size() > 0)
//			entity = entityList.get(0).getEmployeeNumber().substring(0, namePrefix.length()) + String
//					.valueOf(Long.valueOf(entityList.get(0).getEmployeeNumber().substring(namePrefix.length())) + 1);
//		else
//			entity = "FirstEntry";

		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/serviceCenter", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByServiceCenter(@RequestParam @Valid final Long serviceCenterId)
			throws Exception, Throwable {

		List<AgentObject> agentObjectList = new ArrayList<AgentObject>();
		List<Agent> entity = agentRepository.findByServiceCenter(serviceCenterId);
		entity.forEach(agent -> {
			AgentObject agentObject = new AgentObject();
			agentObject.setAgent(agent);
			agentObject.setAssignedBranchCounter(
					branchCounterAgentMapperRepository.findBranchCounterByAgentId(agent.getAgentId()));
			
                        List<UserPrivilege> assignedPrivileges=userPrivilegeMapperRepository
					.findPrivilgesByUserId(agent.getServiceCenterEmployee().getUser().getUserId());
//                        if (assignedPrivileges==null || assignedPrivileges.isEmpty()==true){
////                                             logger.info("assigned privilege check"+assignedPrivileges.size()+assignedPrivileges.get(0).getPrivilegeName().name());
//                            assignedPrivileges=new ArrayList<UserPrivilege>();
//                        }
                        logger.info("user privilege check"+assignedPrivileges.size()+assignedPrivileges.get(0).getPrivilegeName());
                        
                        agentObject.setAssignedPrivileges(assignedPrivileges); 
			Double rating = ticketFeedbackRepository.avgRatingByAgent(agent);
			if (rating == null)
				rating = 0.0;
			agentObject.setRating(rating);
			agentObjectList.add(agentObject);

		});
                logger.info(ResponseEntity.ok(agentObjectList).toString());
		return ResponseEntity.ok(agentObjectList);
	}

	@RequestMapping(value = "/getAgentListing/branchList", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByBranchList(@RequestBody @Valid final List<Long> branchIdList)
			throws Exception, Throwable {

		List<AgentObject> agentObjectList = new ArrayList<AgentObject>();
		List<Agent> entity;
		if (branchIdList.isEmpty())
			entity = new ArrayList<Agent>();
		else
			entity = agentRepository.findByBranchList(branchIdList);
		entity.forEach(agent -> {
			AgentObject agentObject = new AgentObject();
			agentObject.setAgent(agent);
			agentObject.setAssignedBranchCounter(
					branchCounterAgentMapperRepository.findBranchCounterByAgentId(agent.getAgentId()));
//			agentObject.setAssignedPrivileges(new ArrayList<UserPrivilege>());
//			agentObject.setAssignedPrivileges(userPrivilegeMapperRepository
//					.findPrivilgesByUserId(agent.getServiceCenterEmployee().getUser().getUserId()));
			Double rating = ticketFeedbackRepository.avgRatingByAgent(agent);
			if (rating == null)
				rating = 0.0;
			agentObject.setRating(rating);

			agentObjectList.add(agentObject);

		});
		return ResponseEntity.ok(agentObjectList);
	}

	@RequestMapping(value = "/getAgentCount/branchList", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> countByBranchList(@RequestBody @Valid final List<Long> branchIdList)
			throws Exception, Throwable {
		return ResponseEntity.ok(agentRepository.countByBranchList(branchIdList));
	}

	@RequestMapping(value = "/get/branch", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByBranch(@RequestBody Branch branch) throws Exception, Throwable {

		List<AgentObject> agentObjectList = new ArrayList<AgentObject>();
		List<Agent> entity = agentRepository.findByBranch(branch);
		entity.forEach(agent -> {
			AgentObject agentObject = new AgentObject();
			agentObject.setAgent(agent);
			agentObject.setAssignedBranchCounter(
					branchCounterAgentMapperRepository.findBranchCounterByAgentId(agent.getAgentId()));
			agentObject.setAssignedPrivileges(userPrivilegeMapperRepository
					.findPrivilgesByUserId(agent.getServiceCenterEmployee().getUser().getUserId()));
			agentObjectList.add(agentObject);

		});
		return ResponseEntity.ok(agentObjectList);
	}

	@RequestMapping(value = "/delete/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteByAgentId(@RequestParam @Valid final Long agentId)
			throws Exception, Throwable {

		int entity = agentRepository.deleteByBranchId(agentId);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/delete/idList", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteAgents(@RequestBody @Valid final List<Long> agentIdList)
			throws Exception, Throwable {
		List<Agent> agents = agentRepository.findByIdList(agentIdList);
		List<User> users = agents.stream().map(agent -> agent.getServiceCenterEmployee().getUser())
				.collect(Collectors.toList());
		List<String> filePathList = users.stream()
				.map(user -> new String(Base64.getDecoder().decode(user.getProfileImageUrl().getBytes())))
				.collect(Collectors.toList());
		userRepository.deleteAll(users);
		fileDataService.remove(filePathList);
		return ResponseEntity.ok(true);
	}

	@RequestMapping(value = "/getDeatils/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAgentDetails(@RequestParam @Valid final Long agentId)
			throws Exception, Throwable {

		Optional<Agent> agent = agentRepository.findById(agentId);
		if (agent.isPresent()) {
			AgentObject agentObject = new AgentObject();
			agentObject.setAgent(agent.get());
			agentObject.setAssignedBranchCounter(
					branchCounterAgentMapperRepository.findBranchCounterByAgentId(agent.get().getAgentId()));
			agentObject.setAssignedPrivileges(userPrivilegeMapperRepository
					.findPrivilgesByUserId(agent.get().getServiceCenterEmployee().getUser().getUserId()));
			return ResponseEntity.ok(agentObject);
		}
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/empNo", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAgentByEmpNo(@RequestParam @Valid final String agentNo)
			throws Exception, Throwable {

		Optional<Agent> agent = agentRepository.findByEmpNo(agentNo);
		if (agent.isPresent()) {
			return ResponseEntity.ok(agent.get());
		}
		return ResponseEntity.ok(null);
	}
}
