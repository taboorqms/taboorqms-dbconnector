package com.taboor.qms.db.connector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.model.UserPrivilege;
import com.taboor.qms.core.model.UserRole;
import com.taboor.qms.core.utils.PrivilegeName;
import com.taboor.qms.core.utils.RoleName;
import com.taboor.qms.db.connector.repository.UserPrivilegeRepository;
import com.taboor.qms.db.connector.repository.UserRoleRepository;
import com.taboor.qms.db.connector.service.ScheduledService;

import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

@ComponentScan({ "com.taboor.qms.db.connector", "com.taboor.qms.core" })
@EnableJpaRepositories(basePackages = { "com.taboor.qms.db.connector.repository" })
@EnableAsync
@EnableScheduling
@EntityScan("com.taboor.qms.core.model")
@SpringBootApplication
@EnableSwagger2WebMvc
@Import(SpringDataRestConfiguration.class)
public class TaboorQmsDbConnectorApplication implements CommandLineRunner {

	@Autowired
	UserRoleRepository userRoleRepository;

	@Autowired
	UserPrivilegeRepository userPrivilegeRepository;

	@Autowired
	private ScheduledService scheduledService;

	public static void main(String[] args) {
		SpringApplication.run(TaboorQmsDbConnectorApplication.class, args);
	}

	@PostConstruct
	@Transactional
	void DumpIntoDB() {

		List<UserRole> savedUserRoles = userRoleRepository.findAll();
		List<UserPrivilege> savedUserPrivileges = userPrivilegeRepository.findAll();

		List<UserPrivilege> userPrivilegeList = new ArrayList<UserPrivilege>();
		UserPrivilege userPrivilege;
		for (PrivilegeName privilegeName : PrivilegeName.values()) {
			userPrivilege = savedUserPrivileges.stream()
					.filter(entity -> privilegeName.equals(entity.getPrivilegeName())).findAny()
					.orElse(new UserPrivilege());
			if (userPrivilege.getPrivilegeName() == null)
				userPrivilege.setPrivilegeName(privilegeName);
			userPrivilegeList.add(userPrivilege);
		}
		userPrivilegeList = userPrivilegeRepository.saveAll(userPrivilegeList);

		List<UserRole> userRoleList = new ArrayList<UserRole>();
		UserRole userRole;

		userRole = savedUserRoles.stream().filter(entity -> RoleName.Customer.name().equals(entity.getRoleName()))
				.findAny().orElse(new UserRole());
		if (userRole.getRoleName() == null)
			userRole.setRoleName(RoleName.Customer.name());
		userRole.setUserPrivileges(
				getPrivileges(userPrivilegeList, PrivilegeName.Customer_Login, PrivilegeName.Customer_Actions));
		userRoleList.add(userRole);

		userRole = savedUserRoles.stream().filter(entity -> RoleName.Agent.name().equals(entity.getRoleName()))
				.findAny().orElse(new UserRole());
		if (userRole.getRoleName() == null)
			userRole.setRoleName(RoleName.Agent.name());
		userRole.setUserPrivileges(
				getPrivileges(userPrivilegeList, PrivilegeName.Agent_Login, PrivilegeName.Agent_Serve));
		userRoleList.add(userRole);

		userRole = savedUserRoles.stream()
				.filter(entity -> RoleName.Service_Center_Admin.name().equals(entity.getRoleName())).findAny()
				.orElse(new UserRole());
		if (userRole.getRoleName() == null)
			userRole.setRoleName(RoleName.Service_Center_Admin.name());
		userRole.setUserPrivileges(getPrivileges(userPrivilegeList, PrivilegeName.Admin_Panel_View_Only,
				PrivilegeName.Service_Center_Profile_View, PrivilegeName.Service_Center_Profile_Edit,
				PrivilegeName.Support_Ticket_View, PrivilegeName.Support_Ticket_Edit, PrivilegeName.Branch_View,
				PrivilegeName.Branch_Edit, PrivilegeName.Agent_View, PrivilegeName.Agent_Edit, PrivilegeName.Queue_View,
				PrivilegeName.Queue_Edit, PrivilegeName.User_View, PrivilegeName.User_Managment, PrivilegeName.FAQ_View,
				PrivilegeName.Manual_View, PrivilegeName.Branch_Managment));
		userRoleList.add(userRole);

		userRole = savedUserRoles.stream().filter(entity -> RoleName.Branch_Admin.name().equals(entity.getRoleName()))
				.findAny().orElse(new UserRole());
		if (userRole.getRoleName() == null)
			userRole.setRoleName(RoleName.Branch_Admin.name());
		userRole.setUserPrivileges(getPrivileges(userPrivilegeList, PrivilegeName.Branch_Managment));
		userRoleList.add(userRole);

		userRole = savedUserRoles.stream().filter(entity -> RoleName.Taboor_Admin.name().equals(entity.getRoleName()))
				.findAny().orElse(new UserRole());
		if (userRole.getRoleName() == null)
			userRole.setRoleName(RoleName.Taboor_Admin.name());
		userRole.setUserPrivileges(
				getPrivileges(userPrivilegeList, PrivilegeName.Service_Center_View, PrivilegeName.Service_Center_Edit,
						PrivilegeName.Invoice_View, PrivilegeName.Invoice_Edit, PrivilegeName.Subscription_Plan_View,
						PrivilegeName.Subscription_Plan_Edit, PrivilegeName.FAQ_View, PrivilegeName.FAQ_Edit,
						PrivilegeName.Manual_View, PrivilegeName.Manual_Edit, PrivilegeName.Admin_Panel_View_Only,
						PrivilegeName.Service_Center_Profile_View, PrivilegeName.Support_Ticket_View,
						PrivilegeName.Support_Ticket_Edit, PrivilegeName.User_View, PrivilegeName.User_Managment));
		userRoleList.add(userRole);

		userRoleRepository.saveAll(userRoleList);
		System.out.println("Done");
	}

	private Collection<UserPrivilege> getPrivileges(List<UserPrivilege> userPrivilegeList,
			PrivilegeName... privilegeNames) {
		List<PrivilegeName> assignPrivilegesList = Arrays.asList(privilegeNames);
		List<UserPrivilege> userPrivileges = new ArrayList<UserPrivilege>();
		assignPrivilegesList.forEach(name -> userPrivileges.add(userPrivilegeList.stream()
				.filter(entity -> name.equals(entity.getPrivilegeName())).findAny().orElse(null)));
		return userPrivileges;
	}

	// At 00:00:00am every day - 0 0 0 * * ?
	@Scheduled(cron = "0 0 0 * * ?")
	void scheduledFunction() throws TaboorQMSServiceException, Exception {
		System.out.println("Scheduled CRON Job Started");
		scheduledService.chargeInvoices();
		scheduledService.updateInvoiceStatus();
		scheduledService.updateBranchTicketsPerDay();
	}

	@Override
	public void run(String... args) throws Exception {

		FirebaseOptions options = new FirebaseOptions.Builder()
				.setCredentials(GoogleCredentials.fromStream(getClass().getClassLoader()
						.getResourceAsStream("taboor-firebase-adminsdk-0p6y8-b25b763338.json")))
				.setDatabaseUrl("https://taboor.firebaseio.com").build();

		FirebaseApp.initializeApp(options);
	}

}
