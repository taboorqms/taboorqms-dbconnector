package com.taboor.qms.db.connector.controller;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.Session;
import com.taboor.qms.core.model.UserPrivilege;
import com.taboor.qms.core.response.SessionAndPrivilegesResponse;
import com.taboor.qms.db.connector.repository.SessionRepository;
import com.taboor.qms.db.connector.repository.UserPrivilegeMapperRepository;
import com.taboor.qms.db.connector.repository.UserRepository;

@RestController
@RequestMapping("/tab/sessions")
@CrossOrigin(origins = "*")
public class SessionRepoController {

	@Autowired
	SessionRepository sessionRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	UserPrivilegeMapperRepository userPrivilegeMapperRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveSession(@RequestBody @Valid final Session session)
			throws Exception, Throwable {
		if (session.getLoginTime() != null & session.getUser() != null) {
			session.getUser().setLastLoginTime(OffsetDateTime.now().withNano(0));
			userRepository.save(session.getUser());
		}
		return ResponseEntity.ok(sessionRepository.save(session));
	}

	@RequestMapping(value = "/getActiveSession/sessionToken", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getActiveSessionBySessionToken(
			@RequestParam @Valid final String sessionToken) throws Exception, Throwable {

		Optional<Session> entity = sessionRepository.findBySessionTokenAndLogoutTimeIsNull(sessionToken);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/getActiveSession/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getActiveSessionBySessionId(@RequestParam @Valid final Long sessionId)
			throws Exception, Throwable {

		Optional<Session> entity = sessionRepository.findBySessionIdAndLogoutTimeIsNull(sessionId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/getActiveSessionAndPrivileges/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getActiveSessionAndPrivilegesBySessionId(
			@RequestParam @Valid final Long sessionId) throws Exception, Throwable {

		Optional<Session> entity = sessionRepository.findBySessionIdAndLogoutTimeIsNull(sessionId);
		if (entity.isPresent()) {
			List<UserPrivilege> privilges = new ArrayList<UserPrivilege>();
			if (entity.get().getUser() != null)
				privilges = userPrivilegeMapperRepository.findPrivilgesByUserId(entity.get().getUser().getUserId());

			return ResponseEntity.ok(new SessionAndPrivilegesResponse(entity.get(), privilges));
		}
		return ResponseEntity.ok(new SessionAndPrivilegesResponse(null, null));
	}

	@RequestMapping(value = "/getByRefreshToken", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByrefreshToken(@RequestParam @Valid final String refreshToken)
			throws Exception, Throwable {

		Optional<Session> entity = sessionRepository.findByRefreshToken(refreshToken);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/user", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByUser(@RequestParam @Valid final Long userId)
			throws Exception, Throwable {

		Optional<Session> entity = sessionRepository.findByUser(userId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/getByUserEmail", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByUserEmail(@RequestParam @Valid final String email)
			throws Exception, Throwable {

		List<Session> entity = sessionRepository.findByUserEmailAndLogoutTime(email);
		if (entity != null)
			return ResponseEntity.ok(entity);
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/getByUserId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByUserId(@RequestParam @Valid final Long userId)
			throws Exception, Throwable {

		List<Session> entity = sessionRepository.findByUserIdAndLogoutTime(userId);
		if (entity != null)
			return ResponseEntity.ok(entity);
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/getActiveSession/userId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getActiveSessionByUserId(@RequestParam @Valid final Long userId)
			throws Exception, Throwable {

		List<Session> entity = sessionRepository.findActiveSessionByUserIdAndLogoutTime(userId);
		if (entity != null)
			return ResponseEntity.ok(entity.get(0));
		return ResponseEntity.ok(null);
	}

}
