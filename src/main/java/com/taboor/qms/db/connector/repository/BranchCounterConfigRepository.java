package com.taboor.qms.db.connector.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.BranchCounterConfig;

@Repository
@Transactional
public interface BranchCounterConfigRepository extends JpaRepository<BranchCounterConfig, Long> {

	@Query("select bcc from BranchCounterConfig bcc where branch_counter_id = :branchCounterId")
	Optional<BranchCounterConfig> getByCounter(@Param("branchCounterId") long branchCounterId);

	@Query("select bcc from BranchCounterConfig bcc inner join BranchCounterAgentMapper bam"
			+ " on bcc.branchCounter=bam.branchCounter where agent_id =:agentId")
	Optional<BranchCounterConfig> findByAgentId(@Param("agentId") Long agentId);

	@Modifying
	@Query("delete from BranchCounterConfig bcc where bcc.deviceToken = :token")
	int deleteByDeviceToken(@Param("token") String token);
}
