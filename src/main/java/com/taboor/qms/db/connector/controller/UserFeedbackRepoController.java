package com.taboor.qms.db.connector.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.UserFeedback;
import com.taboor.qms.db.connector.repository.UserFeedbackRepository;

@RestController
@RequestMapping("/tab/userfeedbacks")
@CrossOrigin(origins = "*")
public class UserFeedbackRepoController {

	@Autowired
	UserFeedbackRepository userFeedbackRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveUserFeedback(@RequestBody @Valid final UserFeedback userFeedback)
			throws Exception, Throwable {
		return ResponseEntity.ok(userFeedbackRepository.save(userFeedback));
	}

}
