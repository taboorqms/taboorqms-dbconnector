package com.taboor.qms.db.connector.service;

import org.springframework.stereotype.Service;

import com.taboor.qms.core.exception.TaboorQMSServiceException;
import com.taboor.qms.core.utils.RSocketPayload;

@Service
public interface NotificationService {

	void decodePayload(RSocketPayload payload) throws TaboorQMSServiceException, Exception;

}
