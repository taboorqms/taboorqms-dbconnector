package com.taboor.qms.db.connector.repository;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.BranchCounter;
import com.taboor.qms.core.model.BranchCounterAgentMapper;

@Repository
@Transactional
public interface BranchCounterAgentMapperRepository extends JpaRepository<BranchCounterAgentMapper, Long> {

	@Query("Select bca from BranchCounterAgentMapper bca where branch_counter_id=:counterId")
	BranchCounterAgentMapper findByCounterId(@Param("counterId") Long counterId);

//	@Query("Select bc from BranchCounterAgentMapper bca inner join BranchCounter bc on bca.branchCounter = bc.counterId "
//			+ "where agent_id = :agentId")
//	BranchCounter findBranchCounterByAgentId(@Param("agentId") Long agentId);

	@Query("Select bca.branchCounter from BranchCounterAgentMapper bca where agent_id = :agentId")
	BranchCounter findBranchCounterByAgentId(@Param("agentId") Long agentId);

	@Query("Select count(b) from BranchCounterAgentMapper b where branch_counter_id = :counterId")
	int countByBranchCounter(@Param("counterId") long counterId);

	@Query("Select bca from BranchCounterAgentMapper bca where agent_id = :agentId")
	Optional<BranchCounterAgentMapper> findByAgentId(@Valid Long agentId);

	@Modifying
	@Query("DELETE from BranchCounterAgentMapper where agent_id = :agentId")
	int deleteByAgentId(@Param("agentId") Long agentId);

}
