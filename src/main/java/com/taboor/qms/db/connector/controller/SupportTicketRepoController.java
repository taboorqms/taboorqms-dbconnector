package com.taboor.qms.db.connector.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.ServiceCenter;
import com.taboor.qms.core.model.SupportTicket;
import com.taboor.qms.core.utils.FileDataService;
import com.taboor.qms.core.utils.GenericMapper;
import com.taboor.qms.core.utils.StatusHistoryObject;
import com.taboor.qms.db.connector.repository.SupportTicketRepository;

@RestController
@RequestMapping("/tab/supporttickets")
@CrossOrigin(origins = "*")
public class SupportTicketRepoController {

	@Autowired
	SupportTicketRepository supportTicketRepository;

	@Autowired
	private FileDataService fileDataService;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveSupportTicket(@RequestBody @Valid final SupportTicket supportTicket)
			throws Exception, Throwable {
		if (supportTicket.getStatusHistoryList() == null)
			supportTicket.setStatusHistoryList(new ArrayList<StatusHistoryObject>());
		supportTicket.setStatusHistory(GenericMapper.objectToJSONMapper(supportTicket.getStatusHistoryList()));
		return ResponseEntity.ok(supportTicketRepository.save(supportTicket));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long supportTicketId)
			throws Exception, Throwable {

		Optional<SupportTicket> entity = supportTicketRepository.findById(supportTicketId);
		if (entity.isPresent()) {
			if (entity.get().getStatusHistory() != null)
				entity.get().setStatusHistoryList(GenericMapper.jsonToListObjectMapper(entity.get().getStatusHistory(),
						StatusHistoryObject.class));
			else
				entity.get().setStatusHistoryList(new ArrayList<StatusHistoryObject>());
			return ResponseEntity.ok(entity.get());
		}
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<SupportTicket> entity = supportTicketRepository.findAll();
		if (!entity.isEmpty())
			for (SupportTicket item : entity)
				if (item.getStatusHistory() != null)
					item.setStatusHistoryList(
							GenericMapper.jsonToListObjectMapper(item.getStatusHistory(), StatusHistoryObject.class));
				else
					item.setStatusHistoryList(new ArrayList<StatusHistoryObject>());
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/serviceCenter", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByServiceCenter(@RequestParam @Valid final Long serviceCentreId)
			throws Exception, Throwable {

		ServiceCenter serviceCenter = new ServiceCenter();
		serviceCenter.setServiceCenterId(serviceCentreId);
		List<SupportTicket> entity = supportTicketRepository.findByServiceCenter(serviceCenter);
		if (!entity.isEmpty())
			for (SupportTicket item : entity)
				if (item.getStatusHistory() != null)
					item.setStatusHistoryList(
							GenericMapper.jsonToListObjectMapper(item.getStatusHistory(), StatusHistoryObject.class));
				else
					item.setStatusHistoryList(new ArrayList<StatusHistoryObject>());
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/delete/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteCounterById(@RequestParam @Valid final long supportTicketId)
			throws Exception, Throwable {
		SupportTicket supportTicket = supportTicketRepository.findById(supportTicketId).get();
		supportTicketRepository.delete(supportTicket);
		fileDataService.remove(supportTicket.getFilePathList().stream()
				.map(path -> new String(Base64.getDecoder().decode(path.getBytes()))).collect(Collectors.toList()));
		return ResponseEntity.ok(true);
	}

}
