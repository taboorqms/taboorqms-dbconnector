package com.taboor.qms.db.connector.repository;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.Agent;
import com.taboor.qms.core.model.Branch;

@Repository
@Transactional
public interface AgentRepository extends JpaRepository<Agent, Long> {

	@Query("Select count(a) from Agent a inner join ServiceCenterEmployee sce on a.serviceCenterEmployee = sce.employeeId inner join ServiceCenter sc on sce.serviceCenter = sc.serviceCenterId where sc.serviceCenterId = :serviceCentreId")
	int countByServiceCenter(@Param("serviceCentreId") Long serviceCenterId);

//	@Query("Select max(employeeNumber) from Agent where em")
//	List<String> findNextAgentEmpNumber(@Param("namePrefix") @Valid String namePrefix,
//			Pageable getLastestAndSortedByAgentIdDesc);

//	List<String> findEmployeeNumberStartingWith(String namePrefix, Pageable getLastestAndSortedByAgentIdDesc);

	@Query("Select a from Agent a inner join ServiceCenterEmployee sce on a.serviceCenterEmployee = sce.employeeId inner join ServiceCenter sc on sce.serviceCenter = sc.serviceCenterId where sc.serviceCenterId = :serviceCentreId")
	List<Agent> findByServiceCenter(@Param("serviceCentreId") Long serviceCenterId);

	List<Agent> findByBranch(Branch branch);

	int countByBranch(Branch branch);

	@Modifying
	@Query("delete from Agent a where a.agentId =:agentId")
	int deleteByBranchId(@Param("agentId") Long agentId);

	@Query("select count(a) from Agent a where branch_id IN :branchIdList")
	int countByBranchList(@Param("branchIdList") List<Long> branchIdList);

	@Query("select a from Agent a where branch_id IN :branchIdList")
	List<Agent> findByBranchList(@Valid List<Long> branchIdList);

	@Query("select ag from Agent ag inner join ServiceCenterEmployee sce on sce.employeeId = ag.serviceCenterEmployee"
			+ "  where sce.employeeNumber=:empNo")
	Optional<Agent> findByEmpNo(String empNo);

	@Query("select a from Agent a where agentId IN :agentIdList")
	List<Agent> findByIdList(@Valid List<Long> agentIdList);

}
