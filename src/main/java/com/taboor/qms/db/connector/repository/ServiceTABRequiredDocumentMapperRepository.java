//package com.taboor.qms.db.connector.repository;
//
//import java.util.List;
//
//import javax.validation.Valid;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.taboor.qms.core.model.ServiceTABRequiredDocumentMapper;
//
//@Repository
//@Transactional
//public interface ServiceTABRequiredDocumentMapperRepository
//		extends JpaRepository<ServiceTABRequiredDocumentMapper, Long> {
//
//	@Query("SELECT srdm FROM ServiceTABRequiredDocumentMapper srdm inner join BranchServiceTABMapper bsm on srdm.branchServiceTABMapper=bsm.branchServiceId inner join ServiceTAB s on bsm.serviceTAB=s.serviceId inner join Branch b on bsm.branch=b.branchId where b.branchId = :branchId and s.serviceId = :serviceId")
//	List<ServiceTABRequiredDocumentMapper> findByBranchIdAndServiceId(@Valid Long branchId, @Valid Long serviceId);
//
//}
