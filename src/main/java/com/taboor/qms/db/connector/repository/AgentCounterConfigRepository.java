package com.taboor.qms.db.connector.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.AgentCounterConfig;

@Repository
@Transactional
public interface AgentCounterConfigRepository extends JpaRepository<AgentCounterConfig, Long> {
	
	@Query("select acc from AgentCounterConfig acc where branch_counter_id = :branchCounterId")
	Optional<AgentCounterConfig> getByCounter(@Param("branchCounterId") long branchCounterId);
	
	@Query("select acc from AgentCounterConfig acc where acc.deviceToken = :token")
	Optional<AgentCounterConfig> findByDeviceToken(@Param("token") String token);
	
	@Query("select acc from AgentCounterConfig acc where acc.deviceToken = :token and branch_counter_id = :branchCounterId")
	Optional<AgentCounterConfig> findByDeviceTokenAndCounter(@Param("branchCounterId") Long branchCounterId,
		@Param("token") String token);
	
	@Modifying
	@Query("delete from AgentCounterConfig acc where acc.deviceToken = :token")
	int deleteByDeviceToken(@Param("token") String token);
}
