package com.taboor.qms.db.connector.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.TicketStepOut;
import com.taboor.qms.db.connector.repository.TicketStepOutRepository;

@RestController
@RequestMapping("/tab/ticketstepouts")
@CrossOrigin(origins = "*")
public class TicketStepOutRepoController {

	@Autowired
	TicketStepOutRepository ticketStepOutRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveTicketStepOut(@RequestBody @Valid final TicketStepOut ticketStepOut)
			throws Exception, Throwable {
		return ResponseEntity.ok(ticketStepOutRepository.save(ticketStepOut));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long ticketStepOutId)
			throws Exception, Throwable {

		Optional<TicketStepOut> entity = ticketStepOutRepository.findById(ticketStepOutId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<TicketStepOut> entity = ticketStepOutRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/ticket", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByTicketId(@RequestParam @Valid final Long ticketId)
			throws Exception, Throwable {

		Optional<TicketStepOut> entity = ticketStepOutRepository.findByTicket(ticketId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

//	@RequestMapping(value = "/check/ticket", method = RequestMethod.POST, consumes = "application/json; charset=UTF-8", produces = "application/json")
//	public @ResponseBody ResponseEntity<?> getByTicketId(@RequestBody @Valid final Ticket ticket)
//			throws Exception, Throwable {
//
//		Boolean entity = ticketStepOutRepository.existsByTicket(ticket);
//		return ResponseEntity.ok(entity);
//	}

}
