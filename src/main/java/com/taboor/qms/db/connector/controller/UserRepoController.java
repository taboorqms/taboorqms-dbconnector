package com.taboor.qms.db.connector.controller;

import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.User;
import com.taboor.qms.core.response.UserAndPrivilegesResponse;
import com.taboor.qms.core.utils.FileDataService;
import com.taboor.qms.db.connector.repository.UserPrivilegeMapperRepository;
import com.taboor.qms.db.connector.repository.UserRepository;

@RestController
@RequestMapping("/tab/users")
@CrossOrigin(origins = "*")
public class UserRepoController {

	@Autowired
	UserRepository userRepository;

	@Autowired
	private FileDataService fileDataService;

	@Autowired
	UserPrivilegeMapperRepository userPrivilegeMapperRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveUser(@RequestBody @Valid final User user) throws Exception, Throwable {
		return ResponseEntity.ok(userRepository.save(user));
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<User> entity = userRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByUserId(@RequestParam @Valid final Long userId)
			throws Exception, Throwable {

		Optional<User> entity = userRepository.findById(userId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteUser(@RequestParam @Valid final long userId)
			throws Exception, Throwable {
		User user = userRepository.findById(userId).get();
		userRepository.delete(user);
		// TODO Remove User Picture on Deletion
//		fileDataService
//				.remove(Arrays.asList(new String(Base64.getDecoder().decode(user.getProfileImageUrl().getBytes()))));
		return ResponseEntity.ok(1);
	}

	@RequestMapping(value = "/get/email", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByEmail(@RequestParam @Valid final String email)
			throws Exception, Throwable {

		String decodeEmail = java.net.URLDecoder.decode(email, "UTF-8");
		Optional<User> entity = userRepository.findByEmail(decodeEmail);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}
	@RequestMapping(value = "/get/phone_number", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByPhoneNumber(@RequestParam @Valid final String phone_number)
			throws Exception, Throwable {

		String decodeEmail = java.net.URLDecoder.decode(phone_number, "UTF-8");
		Optional<User> entity = userRepository.findByPhoneNumber(decodeEmail);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/getUserAndPrivileges/email", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getUserAndPrivilegesByEmail(@RequestParam @Valid final String email)
			throws Exception, Throwable {

		String decodeEmail = java.net.URLDecoder.decode(email, "UTF-8");
		Optional<User> entity = userRepository.findByEmail(decodeEmail);
		if (entity.isPresent())
			return ResponseEntity.ok(new UserAndPrivilegesResponse(entity.get(),
					userPrivilegeMapperRepository.findPrivilegeNameByUserId(entity.get().getUserId())));
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/checkUserByEmail", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> checkByEmail(@RequestParam @Valid final String email)
			throws Exception, Throwable {
		return ResponseEntity.ok(userRepository.existsByEmail(email));
	}

	@RequestMapping(value = "/checkUserByPhone", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> Phone(@RequestParam @Valid final String phoneNumber)
			throws Exception, Throwable {
		return ResponseEntity.ok(userRepository.existsByPhoneNumber(phoneNumber));
	}
}
