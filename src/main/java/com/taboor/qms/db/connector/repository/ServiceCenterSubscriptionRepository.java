package com.taboor.qms.db.connector.repository;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.PaymentPlan;
import com.taboor.qms.core.model.ServiceCenter;
import com.taboor.qms.core.model.ServiceCenterSubscription;
import com.taboor.qms.core.utils.SubscriptionStatus;

@Repository
@Transactional
public interface ServiceCenterSubscriptionRepository extends JpaRepository<ServiceCenterSubscription, Long> {

	@Query("select scs from ServiceCenterSubscription scs where service_center_id=:serviceCenterId and subscriptionStatus = :subscriptionStatus")
	Optional<ServiceCenterSubscription> findActiveSubscriptionByServiceCenterId(
			@Param("serviceCenterId") Long serviceCenterId,
			@Param("subscriptionStatus") SubscriptionStatus subscriptionStatus);

	@Query("select (scs.chargesPaid/scs.chargesTotal)*100 from ServiceCenterSubscription scs where service_center_id=:serviceCenterId and scs.subscriptionStatus = :subscriptionStatus")
	Double findPaidPercentageByServiceCenterId(@Param("serviceCenterId") Long serviceCenterId,
			@Param("subscriptionStatus") SubscriptionStatus subscriptionStatus);

	Optional<ServiceCenterSubscription> findByServiceCenter(@Valid ServiceCenter serviceCenter);

	@Query("select count(subscriptionId) from ServiceCenterSubscription where payment_plan_id=:planId and subscriptionStatus = :subscriptionStatus")
	int countSubscribersByPlanId(@Param("planId") Long paymentPlanId,
			@Param("subscriptionStatus") SubscriptionStatus subscriptionStatus);

	List<ServiceCenterSubscription> findBySubscriptionStatus(SubscriptionStatus active);

	@Query("select scs from ServiceCenterSubscription scs where service_center_id = :serviceCenterId")
	Optional<ServiceCenterSubscription> findByServiceCenterId(@Param("serviceCenterId") long serviceCenterId);

	@Query("select scs.noOfUsersPerBranch from ServiceCenterSubscription scs where service_center_id = :serviceCenterId and subscriptionStatus = :subscriptionStatus")
	int findNoOfUsersPerBranchByServiceCenterActiveSubscription(@Param("serviceCenterId") long serviceCenterId,
			@Param("subscriptionStatus") SubscriptionStatus subscriptionStatus);

	List<ServiceCenterSubscription> findByPaymentPlan(@Valid PaymentPlan paymentPlan);
}
