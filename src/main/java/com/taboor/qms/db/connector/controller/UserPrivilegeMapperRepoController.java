package com.taboor.qms.db.connector.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.UserPrivilege;
import com.taboor.qms.core.model.UserPrivilegeMapper;
import com.taboor.qms.core.utils.PrivilegeName;
import com.taboor.qms.db.connector.repository.UserPrivilegeMapperRepository;

@RestController
@RequestMapping("/tab/userprivilegemapper")
@CrossOrigin(origins = "*")
public class UserPrivilegeMapperRepoController {

	@Autowired
	UserPrivilegeMapperRepository userPrivilegeMapperRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveUserPrivilegeMapper(
			@RequestBody @Valid final UserPrivilegeMapper userPrivilegeMapper) throws Exception, Throwable {
		return ResponseEntity.ok(userPrivilegeMapperRepository.save(userPrivilegeMapper));
	}

	@RequestMapping(value = "/saveAll", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveAll(
			@RequestBody @Valid final List<UserPrivilegeMapper> userPrivilegeMappers) throws Exception, Throwable {
		userPrivilegeMapperRepository.saveAll(userPrivilegeMappers);
		return ResponseEntity.ok(true);
	}

	@RequestMapping(value = "/updateAll", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> updateAll(
			@RequestBody @Valid final List<UserPrivilegeMapper> userPrivilegeMappers) throws Exception, Throwable {
		userPrivilegeMapperRepository.deleteByUserId(userPrivilegeMappers.get(0).getUser().getUserId());
		userPrivilegeMapperRepository.saveAll(userPrivilegeMappers);
		return ResponseEntity.ok(true);
	}

	@RequestMapping(value = "/getPrivilegeName/user", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getPrivilegeNameByUserId(@RequestParam @Valid final Long userId)
			throws Exception, Throwable {

		List<PrivilegeName> entity = userPrivilegeMapperRepository.findPrivilegeNameByUserId(userId);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/user", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByUserId(@RequestParam @Valid final Long userId)
			throws Exception, Throwable {

		List<UserPrivilege> entity = userPrivilegeMapperRepository.findPrivilgesByUserId(userId);
		return ResponseEntity.ok(entity);
	}
}
