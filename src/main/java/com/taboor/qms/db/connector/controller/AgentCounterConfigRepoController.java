package com.taboor.qms.db.connector.controller;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.AgentCounterConfig;
import com.taboor.qms.db.connector.repository.AgentCounterConfigRepository;

@RestController
@RequestMapping("/tab/agentcounterconfigs")
@CrossOrigin(origins = "*")
public class AgentCounterConfigRepoController {

	@Autowired
	AgentCounterConfigRepository agentCounterConfigRepository;

	@PersistenceContext
	EntityManager em;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveAgentCounter(
			@RequestBody @Valid final AgentCounterConfig agentCounterConfig) throws Exception, Throwable {
		return ResponseEntity.ok(agentCounterConfigRepository.save(agentCounterConfig));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long agentCounterConfigId)
			throws Exception, Throwable {

		Optional<AgentCounterConfig> entity = agentCounterConfigRepository.findById(agentCounterConfigId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<AgentCounterConfig> entity = agentCounterConfigRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/counter", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getCounterById(@RequestParam @Valid final Long branchCounterId)
			throws Exception, Throwable {
		Optional<AgentCounterConfig> entity = agentCounterConfigRepository.getByCounter(branchCounterId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/token", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getConfigByToken(@RequestParam @Valid final String deviceToken)
			throws Exception, Throwable {
		Optional<AgentCounterConfig> agentCounterConfigEntity = agentCounterConfigRepository
				.findByDeviceToken(deviceToken);
		if (agentCounterConfigEntity.isPresent())
			return ResponseEntity.ok(agentCounterConfigEntity.get());
		return ResponseEntity.ok(null);
	}
	
	@RequestMapping(value = "/get/counterAndToken", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getCounterById(@RequestParam @Valid final Long branchCounterId,
			@RequestParam @Valid final String deviceToken)	throws Exception, Throwable {
		Optional<AgentCounterConfig> entity = agentCounterConfigRepository.findByDeviceTokenAndCounter(branchCounterId,
				deviceToken);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/delete/deviceToken", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteByDeviceToken(@RequestParam @Valid final String deviceToken)
			throws Exception, Throwable {
		int entity = agentCounterConfigRepository.deleteByDeviceToken(deviceToken);
		return ResponseEntity.ok(true);
	}
}
