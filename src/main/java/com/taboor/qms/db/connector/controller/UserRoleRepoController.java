package com.taboor.qms.db.connector.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.User;
import com.taboor.qms.core.model.UserPrivilege;
import com.taboor.qms.core.model.UserPrivilegeMapper;
import com.taboor.qms.core.model.UserRole;
import com.taboor.qms.core.response.GetUserRoleListResponse.RoleObject;
import com.taboor.qms.core.utils.RoleName;
import com.taboor.qms.db.connector.repository.UserPrivilegeMapperRepository;
import com.taboor.qms.db.connector.repository.UserRoleMapperRepository;
import com.taboor.qms.db.connector.repository.UserRoleRepository;

@RestController
@RequestMapping("/tab/userroles")
@CrossOrigin(origins = "*")
public class UserRoleRepoController {

	@Autowired
	UserRoleRepository userRoleRepository;

	@Autowired
	UserRoleMapperRepository userRoleMapperRepository;

	@Autowired
	UserPrivilegeMapperRepository userPrivilegeMapperRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveUserRole(@RequestBody @Valid final UserRole userRole)
			throws Exception, Throwable {

		return ResponseEntity.ok(userRoleRepository.save(userRole));
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST, produces = "application/json")
	@Transactional
	public @ResponseBody ResponseEntity<?> updateUserRole(@RequestBody @Valid UserRole userRole)
			throws Exception, Throwable {
		userRole = userRoleRepository.save(userRole);

		List<User> userList = userRoleMapperRepository.findUserByUserRole(userRole.getRoleId());
		List<Long> temp = new ArrayList<Long>();
		for (User user : userList) {
			temp.add(user.getUserId());
			List<UserPrivilegeMapper> userPrivilegeMappers = new ArrayList<UserPrivilegeMapper>();
			for (UserPrivilege privilege : userRole.getUserPrivileges()) {
				UserPrivilegeMapper userPrivilegeMapper = new UserPrivilegeMapper();
				userPrivilegeMapper.setUser(user);
				userPrivilegeMapper.setUserPrivilege(privilege);
				userPrivilegeMappers.add(userPrivilegeMapper);
			}
			userPrivilegeMapperRepository.deleteByUserId(user.getUserId());
			userPrivilegeMapperRepository.saveAll(userPrivilegeMappers);
		}
		return ResponseEntity.ok(userRole);
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long roleId) throws Exception, Throwable {

		Optional<UserRole> entity = userRoleRepository.findById(roleId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/roleName", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByRoleName(@RequestParam @Valid final String roleName)
			throws Exception, Throwable {

		Optional<UserRole> entity = userRoleRepository.findByRoleName(roleName);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {
		List<UserRole> entity = userRoleRepository.findAll();
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/all/2", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll2(@RequestParam String roleName) throws Exception, Throwable {
		List<RoleObject> reposne = new ArrayList<RoleObject>();

		List<UserRole> entity;
		if (roleName.startsWith("Taboor"))
			entity = userRoleRepository.findAll();
		else
			entity = userRoleRepository.findByRoleNameNotStartsWith(RoleName.Customer.name());
		entity.forEach(role -> reposne.add(new RoleObject(role, userRoleMapperRepository.countByUserRole(role))));
		return ResponseEntity.ok(reposne);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteUserRole(@RequestParam @Valid final Long roleId)
			throws Exception, Throwable {

		UserRole userRole = userRoleRepository.findById(roleId).get();
		for (RoleName item : RoleName.values())
			if (item.name().equals(userRole.getRoleName()))
				return ResponseEntity.ok(-1);
		Long count = userRoleMapperRepository.countByUserRole(userRole);
		if (count > 0)
			return ResponseEntity.ok(count.intValue());
		else
			userRoleRepository.deleteById(roleId);
		return ResponseEntity.ok(0);
	}
}
