package com.taboor.qms.db.connector.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.Customer;
import com.taboor.qms.core.model.CustomerSettings;

@Repository
@Transactional
public interface CustomerSettingsRepository extends JpaRepository<CustomerSettings, Long> {

	Optional<CustomerSettings> findByCustomer(Customer customerID);

}
