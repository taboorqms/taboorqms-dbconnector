package com.taboor.qms.db.connector.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.Agent;
import com.taboor.qms.core.model.TicketFeedback;

@Repository
@Transactional
public interface TicketFeedbackRepository extends JpaRepository<TicketFeedback, Long> {

	@Query("SELECT avg(tf.rating) FROM TicketFeedback tf inner join TicketServed ts on tf.ticket = ts.ticket where ts.servedAgent = :agent")
	Double avgRatingByAgent(Agent agent);

	@Query("SELECT tf FROM TicketFeedback tf inner join Ticket tk on tf.ticket = tk.ticketId where branch_id = :branchId")
	List<TicketFeedback> findByTicket(Long branchId);
}
