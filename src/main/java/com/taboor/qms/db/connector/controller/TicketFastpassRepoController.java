package com.taboor.qms.db.connector.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.TicketFastpass;
import com.taboor.qms.core.model.UserPaymentTransaction;
import com.taboor.qms.core.utils.TicketFastpassStatus;
import com.taboor.qms.db.connector.repository.TicketFastpassRepository;

@RestController
@RequestMapping("/tab/ticketfastpass")
@CrossOrigin(origins = "*")
public class TicketFastpassRepoController {

	@Autowired
	TicketFastpassRepository ticketFastpassRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveTicketFastpass(@RequestBody @Valid final TicketFastpass ticketFastpass)
			throws Exception, Throwable {
		return ResponseEntity.ok(ticketFastpassRepository.save(ticketFastpass));
	}

	@RequestMapping(value = "/get/ticketId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByTicketId(@RequestParam @Valid final Long ticketId)
			throws Exception, Throwable {

		Optional<TicketFastpass> entity = ticketFastpassRepository.findByTicketId(ticketId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long ticketFastpassId)
			throws Exception, Throwable {

		Optional<TicketFastpass> entity = ticketFastpassRepository.findById(ticketFastpassId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll() throws Exception, Throwable {

		List<TicketFastpass> entity = ticketFastpassRepository.findAll();
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/get/user", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByUserId(@RequestParam @Valid final Long userId)
			throws Exception, Throwable {

		List<TicketFastpass> entity = ticketFastpassRepository.findByUserId(userId);
		for (int i = 0; i < entity.size(); i++) {
			entity.get(i).getTicket().getBranch().setServiceCenter(null);
			entity.get(i).getUserPaymentTransaction().getUserBankCard().setUser(null);
		}
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteTicketFastpass(@RequestParam @Valid final long ticketFastpassId)
			throws Exception, Throwable {
		return ResponseEntity.ok(ticketFastpassRepository.deleteByTicketFastpassId(ticketFastpassId));
	}

	@RequestMapping(value = "/get/transactionId/ticket/latest", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getIdByTicketLatest(@RequestParam @Valid final Long ticketId)
			throws Exception, Throwable {

		Pageable getLastestAndSortedByTransactionDesc = PageRequest.of(0, 1,
				Sort.by("userPaymentTransaction").descending());
		List<UserPaymentTransaction> entity = ticketFastpassRepository.findTransactionIdByTicketLatest(ticketId,
				TicketFastpassStatus.CONFIRMED.getStatus(), getLastestAndSortedByTransactionDesc);
		if (!entity.isEmpty())
			return ResponseEntity.ok(entity.get(0).getTransactionId());
		return ResponseEntity.ok((long) 0);
	}
}
