package com.taboor.qms.db.connector.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.UserNotificationType;
import com.taboor.qms.db.connector.repository.UserNotificationTypeRepository;

@RestController
@RequestMapping("/tab/usernotificationtypes")
@CrossOrigin(origins = "*")
public class UserNotificationTypeRepoController {

	@Autowired
	UserNotificationTypeRepository userNotificationTypeRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveUserNotificationType(
			@RequestBody @Valid final UserNotificationType userNotificationType) throws Exception, Throwable {
		return ResponseEntity.ok(userNotificationTypeRepository.save(userNotificationType));
	}

	@RequestMapping(value = "/get/userId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getByUserId(@RequestParam @Valid final Long userId)
			throws Exception, Throwable {

		Optional<UserNotificationType> entity = userNotificationTypeRepository.findByUserId(userId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}
}
