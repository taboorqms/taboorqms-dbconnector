package com.taboor.qms.db.connector.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.taboor.qms.core.model.Branch;
import com.taboor.qms.core.model.BranchBookmark;
import com.taboor.qms.db.connector.repository.BranchBookmarkRepository;

@RestController
@RequestMapping("/tab/branchbookmarks")
@CrossOrigin(origins = "*")
public class BranchBookmarkRepoController {

	@Autowired
	BranchBookmarkRepository branchBookmarkRepository;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<?> saveBranchBookmark(@RequestBody @Valid final BranchBookmark branchBookmark)
			throws Exception, Throwable {
		return ResponseEntity.ok(branchBookmarkRepository.save(branchBookmark));
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> deleteBranchBookmark(@RequestParam @Valid final Long branchBookmarkId)
			throws Exception, Throwable {
		return ResponseEntity.ok(branchBookmarkRepository.deleteByBranchBookmarkId(branchBookmarkId));
	}

	@RequestMapping(value = "/get/id", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getById(@RequestParam @Valid final Long branchBookmarkId)
			throws Exception, Throwable {

		Optional<BranchBookmark> entity = branchBookmarkRepository.findById(branchBookmarkId);
		if (entity.isPresent())
			return ResponseEntity.ok(entity.get());
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value = "/get/user", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getAll(@RequestParam @Valid final Long userId) throws Exception, Throwable {

		List<Branch> entity = branchBookmarkRepository.findBranchByUserId(userId);
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		entity.forEach((branch) -> branch.setServiceCenter(null));
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/getBranchIds/userId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getBranchIdsByUserId(@RequestParam @Valid final Long userId)
			throws Exception, Throwable {

		List<Long> entity = branchBookmarkRepository.findBranchIdsByUserId(userId);
		if (entity.isEmpty())
			return ResponseEntity.ok(null);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/getCount/userId", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> countByUserId(@RequestParam @Valid final Long userId)
			throws Exception, Throwable {
		int entity = branchBookmarkRepository.findByUserId(userId);
		return ResponseEntity.ok(entity);
	}

	@RequestMapping(value = "/check/userAndBranch", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> checkByUserId(@RequestParam final Long userId,
			@RequestParam final Long branchId) throws Exception, Throwable {
		int entity = branchBookmarkRepository.checkByUserId(userId, branchId);
		if (entity == 0)
			return ResponseEntity.ok(false);
		return ResponseEntity.ok(true);
	}

	@RequestMapping(value = "/getId/userAndBranch", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<?> getIdsByUserAndBranch(@RequestParam @Valid final Long userId,
			@RequestParam @Valid final Long branchId) throws Exception, Throwable {
		Long entity = branchBookmarkRepository.findIdsByUserAndBranch(userId, branchId);
		return ResponseEntity.ok(entity);
	}
}
