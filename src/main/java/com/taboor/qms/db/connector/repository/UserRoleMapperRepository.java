package com.taboor.qms.db.connector.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.taboor.qms.core.model.User;
import com.taboor.qms.core.model.UserRole;
import com.taboor.qms.core.model.UserRoleMapper;

@Repository
@Transactional
public interface UserRoleMapperRepository extends JpaRepository<UserRoleMapper, Long> {

	Long countByUserRole(UserRole role);

	@Query("select urm.user from UserRoleMapper urm where role_id = :roleId")
	List<User> findUserByUserRole(@Param("roleId") Long roleId);

	UserRoleMapper findByUser(User user);
}
