
CREATE TABLE IF NOT EXISTS ticket_timeline(ticketID bigint, createdTime timestamp, stepOut bigint,
  stepIn bigint, fastpassTime bigint, agentCall bigint, completionTime timestamp, totalTime bigint);
  
INSERT INTO public.tab_payment_method(payment_method_name,payment_method_description)
	VALUES ('Taboor Credit','Taboor Official Currency') on conflict (payment_method_name) do nothing;
INSERT INTO public.tab_payment_method(payment_method_name,payment_method_description)
	VALUES ('Paypal','Paypal International') on conflict (payment_method_name) do nothing;
INSERT INTO public.tab_payment_method(payment_method_name,payment_method_description)
	VALUES ('MasterCard','MasterCard') on conflict (payment_method_name) do nothing;
INSERT INTO public.tab_payment_method(payment_method_name,payment_method_description)
	VALUES ('VISA','VISA') on conflict (payment_method_name) do nothing;
	
INSERT INTO public.tab_user_faq_type(user_faq_type_name)
	VALUES ('UserManual') on conflict (user_faq_type_name) do nothing;
	
INSERT INTO public.tab_payment_plan(
	payment_plan_id, advance_reporting_enabled, branch_limit, created_time, dedicated_account_manager,
	discount_percentage, easy_payment_enabled, free_demo_days, full_day_support, kiosk_enabled,
	no_ofsms, no_of_services, no_of_user_per_branch, plan_name, plan_name_arabic,
	prices_collection, priority_support, remote_dedicated_support, response_time_in_office, response_time_out_office,
	sms_limit, support_type, user_per_branch_limit)
	VALUES 
	(0, false, 1, CURRENT_TIMESTAMP, false,
	0, false, 30, true, false,
	10, 2, 2, 'Demo Plan', 'خطة تجريبية',
	'[{"perUserPrice":0.0,"perSMSPrice":0.0,"priceMonthly":0.0,"priceAnnually":0.0,"currency":"AED"}]', false, false, '00:30:00', '01:00:00',
	10, 'Basic', 2) on conflict (payment_plan_id) do nothing;

--INSERT INTO public.tab_taboor_configurations(config_key,config_value,config_description)
--	VALUES ('FastpassCharges','20','Price is in Dirham') on conflict (config_key) do nothing;
--INSERT INTO public.tab_taboor_configurations(config_key,config_value,config_description)
--	VALUES ('StepOutTimeFastpass','20','Fastpass ticket step out time limit') on conflict (config_key) do nothing;
--INSERT INTO public.tab_taboor_configurations(config_key,config_value,config_description)
--	VALUES ('StepOutTimeStandard','10','Standard ticket step out time limit') on conflict (config_key) do nothing;

INSERT INTO public.tab_service_center_configuration(configuration_id,currency,fastpass_amount,
fastpass_stepout_allowed_count,fastpass_stepout_time,normal_stepout_allowed_count,normal_stepout_time,taboor_share_amount)
VALUES (0,'AED',20,
	2,'00:20:00',1,'00:10:00',2) on conflict (configuration_id) do nothing;
	
-- CREATE SEQUENCE IF NOT EXISTS service_center_seq_pk START 10;
-- CREATE SEQUENCE IF NOT EXISTS service_sector_seq_pk START 10;
-- CREATE SEQUENCE IF NOT EXISTS user_seq_pk START 10;
-- CREATE SEQUENCE IF NOT EXISTS user_role_seq_pk START 10;

-- CREATE SEQUENCE IF NOT EXISTS service_seq_pk START 10;
-- CREATE SEQUENCE IF NOT EXISTS branch_seq_pk START 10;
-- CREATE SEQUENCE IF NOT EXISTS service_center_emp_seq_pk START 10;
-- CREATE SEQUENCE IF NOT EXISTS agent_seq_pk START 10;

-- CREATE SEQUENCE IF NOT EXISTS branch_counter_seq_pk START 10;
-- CREATE SEQUENCE IF NOT EXISTS branch_working_hours_seq_pk START 10;
-- CREATE SEQUENCE IF NOT EXISTS branch_service_map_seq_pk START 10;
-- CREATE SEQUENCE IF NOT EXISTS service_document_seq_pk START 10;

-- CREATE SEQUENCE IF NOT EXISTS ticket_seq_pk START 10;
-- CREATE SEQUENCE IF NOT EXISTS queue_seq_pk START 10;
-- CREATE SEQUENCE IF NOT EXISTS queue_ticket_map_seq_pk START 10;

-- CREATE SEQUENCE IF NOT EXISTS branch_counter_service_seq_pk START 10;
-- CREATE SEQUENCE IF NOT EXISTS branch_counter_agent_seq_pk START 10;
-- CREATE SEQUENCE IF NOT EXISTS transaction_seq_pk START 10;
CREATE SEQUENCE IF NOT EXISTS ticket_number_seq_pk START 1000;

--Constraints
ALTER TABLE tab_agent DROP CONSTRAINT fk3xqj8idefhjdq99yokqwjaqw3, ADD CONSTRAINT fk3xqj8idefhjdq99yokqwjaqw3 FOREIGN KEY (branch_id) REFERENCES tab_branch(branch_id) ON DELETE set null;
ALTER TABLE tab_invoice DROP CONSTRAINT fk29cxphnho2bil42bs3vh9ianu, ADD CONSTRAINT fk29cxphnho2bil42bs3vh9ianu FOREIGN KEY (bank_account_id) REFERENCES tab_bank_account(bank_account_id) ON DELETE set null;
ALTER TABLE tab_service_center_subscription DROP CONSTRAINT fknd5vr81o8weak2nrvr4mk9fst, ADD CONSTRAINT fknd5vr81o8weak2nrvr4mk9fst FOREIGN KEY (bank_card_id) REFERENCES tab_user_bank_card(bank_card_id) ON DELETE set null;
ALTER TABLE tab_ticket DROP CONSTRAINT fknjrhsgpyghl8806u7k40d3v3o, ADD CONSTRAINT fknjrhsgpyghl8806u7k40d3v3o FOREIGN KEY (branch_counter_id) REFERENCES tab_branch_counter(branch_counter_id) ON DELETE set null;
ALTER TABLE tab_ticket_fastpass DROP CONSTRAINT fkabqf1mwtpqq4tbg8di4k03mbx, ADD CONSTRAINT fkabqf1mwtpqq4tbg8di4k03mbx FOREIGN KEY (payment_transaction_id) REFERENCES tab_user_payment_transaction(transaction_id) ON DELETE set null;
ALTER TABLE tab_ticket_served DROP CONSTRAINT fkrwk7dh65nfc74yyegtqqcmp11, ADD CONSTRAINT fkrwk7dh65nfc74yyegtqqcmp11 FOREIGN KEY (served_agent_id) REFERENCES tab_agent(agent_id) ON DELETE set null;
ALTER TABLE tab_ticket_served DROP CONSTRAINT fkqfrwgirbt51nugwwxrpmxk4iv, ADD CONSTRAINT fkqfrwgirbt51nugwwxrpmxk4iv FOREIGN KEY (branch_counter_id) REFERENCES tab_branch_counter(branch_counter_id) ON DELETE set null;
ALTER TABLE tab_notifications DROP CONSTRAINT fk2kc7k2nvdu5w53dyeydj2hmco, ADD CONSTRAINT fk2kc7k2nvdu5w53dyeydj2hmco FOREIGN KEY (sent_by_user_id) REFERENCES tab_user(user_id) ON DELETE set null;
ALTER TABLE tab_support_ticket DROP CONSTRAINT fk6g6jqd2lu3chimt6d15m8olby, ADD CONSTRAINT fk6g6jqd2lu3chimt6d15m8olby FOREIGN KEY (emp_assigned_to) REFERENCES tab_taboor_employee(taboor_emp_id) ON DELETE set null;
